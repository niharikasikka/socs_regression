﻿using SOCS_SmokeTest.BrowserFactory;
using SOCS_SmokeTest;
using SOCS_SmokeTest.Pages;
using SOCS_SmokeTest.UtilityClasses;
using OpenQA.Selenium;
using System;
using System.Threading;
using TechTalk.SpecFlow;

namespace SOCS_SmokeTest.FeatureSteps
{

    [Binding]
    class SmokeSOCS_Steps
    {
        string userName;
        string strCaseNumber;
        string strProgramCaseNumber;
        string strSSSRequestCaseNumber;

        public IWebDriver Driver { get; private set; }

        [Given(@"I navigate to SOCS application homepage")]
        public void GivenINavigateToSOCSApplicationHomepage()
        {
            //Initialising the browser
            Driver = GetBrowser.InitBrowser(CommonFunctions.GetResourceValue("Browser"));
            
            //Navigating to SOCS homepage
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            homePage.GetUrl();
        }
        [When(@"I login using internal username and password")]
        public void GivenILoginUsingInternalUsernameAndPassword()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            homePage.EnterUsername();
            homePage.EnterPassword();
        }

        [When(@"I login using SystemAdmin username and password")]
        public void GivenILoginUsingSystemAdminUsernameAndPassword()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            userName = homePage.EnterSysAdminUsername();
            homePage.EnterSysAdminPassword();
        }        
        [When(@"I login using VisitingTeacherTeamLeader username and password")]
        public void GivenILoginUsingVisitingTeacherTeamLeaderUsernameAndPassword()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            userName = homePage.EnterVisitingTeacherTeamLeaderUsername();
            homePage.EnterVisitingTeacherTeamLeaderpassword();
        }
        [When(@"I login using VisitingTeacher username and password")]
        public void GivenILoginUsingVisitingTeacherUsernameAndPassword()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            userName = homePage.EnterVisitingTeacherUsername();
            homePage.EnterVisitingTeacherpassword();
        }
        [When(@"I login using SSSTeamLeader username and password")]
        public void GivenILoginUsingSSSTeamLeaderUsernameAndPassword()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            userName = homePage.EnterSSSTeamLeaderUsername();
            homePage.EnterSSSTeamLeaderpassword();
        }

        [When(@"I login using AuthorisedSchoolOfficer username and password")]
        public void GivenILoginUsingAuthorisedSchoolOfficerUsernameAndPassword()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            userName = homePage.EnterAuthorisedSchoolOfficerUsername();
            homePage.EnterAuthorisedSchoolOfficerpassword();
        }

        [When(@"I login using SchoolPrincipal username and password")]
        public void GivenILoginUsingSchoolPrincipalUsernameAndPassword()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            userName = homePage.EnterSchoolPrincipalUsername();
            homePage.EnterSchoolPrincipalpassword();
        }

        [When(@"I login using SSSAdminSupport username and password")]
        public void GivenILoginUsingSSSAdminSupportUsernameAndPassword()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            userName = homePage.EnterSSSAdminSupportUsername();
            homePage.EnterSSSAdminSupportpassword();
        }

        [When(@"I login using BranchManager username and password")]
        public void GivenILoginUsingBranchManagerUsernameAndPassword()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            userName = homePage.EnterBranchManagerUsername();
            homePage.EnterBranchManagerpassword();
        }

        [When(@"I login using HelpDesk username and password")]
        public void GivenILoginUsingHelpDeskUsernameAndPassword()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            userName = homePage.EnterHelpDeskUsername();
            homePage.EnterHelpDeskpassword();
        }

        [When(@"I login using SSSStaffMember username and password")]
        public void GivenILoginUsingSSSStaffMemberUsernameAndPassword()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            userName = homePage.EnterSSSStaffMemberUsername();
            homePage.EnterSSSStaffMemberpassword();
        }


        [When(@"I press login button")]
        public void WhenIPressLoginButton()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            homePage.ClickButton();
        }
        [Then(@"I logout of SOCS application")]
        public void ThenILogoutOfSOCSApplication()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            homePage.SignOut();
        }

        [Then(@"I close the browser successfully")]
        public void ThenICloseTheBrowserSuccessfully()
        {
            GetBrowser.Cleanup();
        }
        [Then(@"I click School Cases submenu")]
        public void ThenIClickSchoolCasesSubmenu()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            homePage.HoverCasesMenu();

            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.ClickSchoolCase();
        }
        [Then(@"I click Cases menu")]
        public void ThenIClickCasesMenu()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            homePage.ClickCasesMenu();
        }

        [Then(@"I select student")]
        public void ThenISelectStudent(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.SelectStudent(table);
        }
        [Then(@"I fill in case details")]
        public void ThenIFillInCaseDetails(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.FillInitialDetails(table);
        }

        [Then(@"I fill in case details - Attendance as Primary Presenting Issue")]
        public void ThenIFillInCaseDetails_AttendanceAsPrimaryPresentingIssue(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.FillInIndividualStudentCase_Attendance(table);
        }

        [Then(@"I fill in case details - Behaviour as Primary Presenting Issue")]
        public void ThenIFillInCaseDetails_BehaviourAsPrimaryPresentingIssue(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.FillInIndividualStudentCase_Behaviour(table);
        }
        [Then(@"I filter and select by case number")]
        public void ThenIFilterAndSelectByCaseNumber(Table table)
        {
            CasesProgramListPage casesProgramListPage = new CasesProgramListPage(Driver);
            casesProgramListPage.FilterandSelectbyCaseNumber(table);
        }
        [Then(@"I attach consent form")]
        public void ThenIAttachConsentForm()
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.AttachConsentForm();
        }

        [Then(@"I submit existing student case")]
        public void ThenISubmitExistingStudentCase()
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.SubmitExistingStudentCase();
        }
        [Then(@"I verify the user currently logged in")]
        public void ThenIVerifyTheUserCurrentlyLoggedIn()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            homePage.VerifyUserLoggedIn(userName);
        }
        [Then(@"I fill in case details - Curriculum/learning as Primary Presenting Issue")]
        public void ThenIFillInCaseDetails_CurriculumLearningAsPrimaryPresentingIssue(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.FillInIndividualStudentCase_CurriculumLearning(table);
        }

        [Then(@"I will get the case number")]
        public void ThenIWillGetTheCaseNumber()
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            strCaseNumber = schoolCasePage.GetCaseNumber();
        }
        [Then(@"I fill in program request details - Mental Health as Primary Presenting Issue")]
        public void ThenIFillInProgramRequestDetails_MentalHealthAsPrimaryPresentingIssue(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.FillInProgramRequest_MentalHealth(table);
        }
        [Then(@"I fill in program request details - Social/emotional as Primary Presenting Issue")]
        public void ThenIFillInProgramRequestDetails_SocialEmotionalAsPrimaryPresentingIssue(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.FillInProgramRequest_SocialEmotional(table);
        }
        [Then(@"I fill in program request details - Medical Health Physical as Primary Presenting Issue")]
        public void ThenIFillInProgramRequestDetails_MedicalHealthPhysicalAsPrimaryPresentingIssue(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.FillInProgramRequest_MedicalHealth(table);
        }

        [Then(@"I filter and select by newly created case number")]
        public void ThenIFilterAndSelectByNewlyCreatedCaseNumber()
        {
            CasesProgramListPage casesProgramListPage = new CasesProgramListPage(Driver);
            casesProgramListPage.FilterandSelectbyNewlyCreatedCaseNumber(strCaseNumber);
        }
        [Then(@"I select newly created case number for the student")]
        public void ThenISelectNewlyCreatedCaseNumberForTheStudent(Table table)
        {
            CasesProgramListPage casesProgramListPage = new CasesProgramListPage(Driver);
            casesProgramListPage.SelectNewlyCreatedCaseNumber(table);
        }

        [Then(@"I fill in case details - Primary Presenting Issue is other than below")]
        public void ThenIFillInCaseDetails_PrimaryPresentingIssueIsOtherThanBelow(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.FillInIndividualStudentCase(table);
        }
        [Then(@"I update by selecting Communication or Speech")]
        public void ThenIUpdatebySelectingCommunicationOrSpeech()
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.UpdateOthersCommunicationorSpeech();
        }
        [Then(@"I fill in case details - Social/Emotional as Primary Presenting Issue")]
        public void ThenIFillInCaseDetails_SocialEmotionalAsPrimaryPresentingIssue(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.FillInIndividualStudentCase_Socialemotional(table);
        }

        [Then(@"I made some action")]
        public void ThenIMadeSomeAction(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.Action(table);
        }
        [Then(@"I made some action for program request")]
        public void ThenIMadeSomeActionForProgramRequest(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.Action_ProgramRequest(table);
        }

        [Then(@"I select Close case action")]
        public void ThenISelectCloseCaseAction(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.CloseCaseAction(table);
        }

        [Then(@"I assign a case leader")]
        public void ThenIAssignACaseLeader(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.AssignCaseLeader(table);
        }
        [Then(@"I re-assign a case leader")]
        public void ThenIRe_AssignACaseLeader(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.ReAssignCaseLeader(table);
        }

        [Then(@"I select Service")]
        public void ThenISelectService(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.SelectService(table);
        }
        [Then(@"I select Service for Program Request")]
        public void ThenISelectServiceForProgramRequest(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.SelectService_Program(table);
        }

        [Then(@"I assign a case worker")]
        public void ThenIAssignACaseWorker(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.AssignCaseWorker(table);
        }

        [Then(@"I create a service note")]
        public void ThenICreateAServiceNote(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.CreateServiceNote(table);
        }

        [Then(@"I add/save case note and save school case")]
        public void ThenIAddSaveCaseNoteAndSaveSchoolCase(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.AddNoteandSave(table);
        }

        [Then(@"I close the service")]
        public void ThenICloseTheService(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.CloseService(table);
        }
        [Then(@"I close the case")]
        public void ThenICloseTheCase()
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.CloseCase();
        }
        [Then(@"I close the case for program request")]
        public void ThenICloseTheCaseForProgramRequest()
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.CloseCase_Program();
        }

        [Then(@"I return the case")]
        public void ThenIReturnTheCase()
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.ReturnCase();
        }

        [Then(@"I select Return for amendment action")]
        public void ThenISelectReturnForAmendmentAction(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.ReturnCaseAction(table);
        }

        [Then(@"I reopen the case")]
        public void ThenIReopenTheCase()
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.ReopenCase();
        }
        [Then(@"I click Create Program Request submenu")]
        public void ThenIClickCreateProgramRequestSubmenu()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            homePage.HoverCasesMenu();

            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.ClickCreateProgramRequest();
        }
        [Then(@"I fill in program request details - Curriculum/learning as Primary Presenting Issue")]
        public void ThenIFillInProgramRequestDetails_CurriculumLearningAsPrimaryPresentingIssue(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.FillInProgramRequest_CurriculumLearning(table);
        }

        [Then(@"I fill in program request details - Attendance as Primary Presenting Issue")]
        public void ThenIFillInProgramRequestDetails_AttendanceAsPrimaryPresentingIssue(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.FillInProgramRequest_Attendance(table);
        }
        [Then(@"I fill in program request details - Behaviour as Primary Presenting Issue")]
        public void ThenIFillInProgramRequestDetails_BehaviourAsPrimaryPresentingIssue(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.FillInProgramRequest_Behaviour(table);
        }
        [Then(@"I fill in program request details - Communication or Speech as Primary Presenting Issue")]
        public void ThenIFillInProgramRequestDetails_CommunicationOrSpeechAsPrimaryPresentingIssue(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.FillInProgramRequest_Communication(table);
        }
        [Then(@"I reopen the  for program request")]
        public void ThenIReopenTheForProgramRequest()
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.ReopenCase_Program();
        }

        [Then(@"I return the case for program request")]
        public void ThenIReturnTheCaseForProgramRequest()
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.ReturnCase_Program();
        }


        [Then(@"I select students for the program request")]
        public void ThenISelectStudentsForTheProgramRequest(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.SelectStudentfortheProgram(table);
        }

        [Then(@"I select multiple students for the program request")]
        public void ThenISelectMultipleStudentsForTheProgramRequest()
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.SelectStudentsfortheProgram();
        }
        [Then(@"I select no student for the program request")]
        public void ThenISelectNoStudentForTheProgramRequest()
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.NoStudentfortheProgram();
        }
        [Then(@"I will get the program case number")]
        public void ThenIWillGetTheProgramCaseNumber()
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            strProgramCaseNumber = programRequestPage.GetProgramCaseNumber();
        }
        [Then(@"I fill in program request details - Support following critical incident")]
        public void ThenIFillInProgramRequestDetails_SupportFollowingCriticalIncident(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.FillInProgramRequest_Support(table);
        }
        [Then(@"I fill in program request details - Educational Needs Assessment")]
        public void ThenIFillInProgramRequestDetails_EducationalNeedsAssessment(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.FillInProgramRequest_EducationalNeedsAssessment(table);
        }

        [Then(@"I filter and select by newly created Program Request case number")]
        public void ThenIFilterAndSelectByNewlyCreatedProgramRequestCaseNumber()
        {
            CasesProgramListPage casesProgramListPage = new CasesProgramListPage(Driver);
            casesProgramListPage.FilterandSelectbyNewlyCreatedCaseNumber(strProgramCaseNumber);
        }
        
        [Then(@"I upload supporting documents for Program Request")]
        public void ThenIUploadSupportingDocumentsForProgramRequest()
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.UploadSupportingDocuments();
        }

        [Then(@"I add/save case note for Program Request")]
        public void ThenIAddSaveCaseNoteForProgramRequest(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.AddNoteandSave(table);
        }
        [Then(@"I save the changes in Program Request")]
        public void ThenISaveTheChangesInProgramRequest()
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.SaveCase();
        }

        [Then(@"I attach consent form for Program Request")]
        public void ThenIAttachConsentFormForProgramRequest()
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.AttachConsentForm();
        }

        [Then(@"I save and submit existing Program Request")]
        public void ThenISaveandSubmitExistingProgramRequest()
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.SubmitCase();
        }
        [Then(@"I select newly Program Request case number for the student")]
        public void ThenISelectNewlyProgramRequestCaseNumberForTheStudent()
        {
            CasesProgramListPage casesProgramListPage = new CasesProgramListPage(Driver);
            casesProgramListPage.SelectNewlyCreatedProgramRequestCaseNumber();
        }
        [Then(@"I request new specialist")]
        public void ThenIRequestNewSpecialist()
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.AmendSpecialist();
        }
        [Then(@"I fill in program request details - Whole School Issue")]
        public void ThenIFillInProgramRequestDetails_WholeSchoolIssue(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.FillInProgramRequest_WholeSchoolIssue(table);
        }
        [Then(@"I close the case for program request by action")]
        public void ThenICloseTheCaseForProgramRequestByAction(Table table)
        {
            ProgramRequestPage programRequestPage = new ProgramRequestPage(Driver);
            programRequestPage.CloseCase_Action(table);
        }


        [Then(@"I select specific issue")]
        public void ThenISelectSpecificIssue(Table table)
        {
            CasesProgramListPage casesProgramListPage = new CasesProgramListPage(Driver);
            casesProgramListPage.SelectIssue(table);
        }
        [Then(@"I indicate that case activities have been completed")]
        public void ThenIIndicateThatCaseActivitiesHaveBeenCompleted()
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.CaseActivitiesCompleted();
        }
        [Then(@"I save changes in School Case")]
        public void ThenISaveChangesInSchoolCase()
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.SaveCase();
        }
        [Then(@"I update Priority")]
        public void ThenIUpdatePriority(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.UpdatePriority(table);
        }
        [Then(@"I update Case Plan")]
        public void ThenIUpdateCasePlan(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.UpdateCasePlan(table);
        }
        [Then(@"I bulk case assignment")]
        public void ThenIBulkCaseAssignment(Table table)
        {
            CasesProgramListPage casesProgramListPage = new CasesProgramListPage(Driver);
            casesProgramListPage.BulkAssignment(table);
            //casesProgramListPage.SelectCasefromList(table);
        }
        [Then(@"I press search button")]
        public void ThenIPressSearchButton()
        {
            ManageUsersPage manageUsersPage = new ManageUsersPage(Driver);
            manageUsersPage.ClickSearchButton();
        }
        [Then(@"I press Deactivate User access button")]
        public void ThenIPressDeactivateUserAccessButton()
        {
            ManageUsersPage manageUsersPage = new ManageUsersPage(Driver);
            manageUsersPage.ClickDeactivateUserAccessButton();
        }
        [Then(@"I enter First Name")]
        public void ThenIEnterFirstName(Table table)
        {
            ManageUsersPage manageUsersPage = new ManageUsersPage(Driver);
            manageUsersPage.EnterFirstName(table);
        }

        [Then(@"I hit select button")]
        public void ThenIHitSelectButton()
        {
            ManageUsersPage manageUsersPage = new ManageUsersPage(Driver);
            manageUsersPage.HitSelectButton();
        }
        [Then(@"I press Activate User access button")]
        public void ThenIPressActivateUserAccessButton()
        {
            ManageUsersPage manageUsersPage = new ManageUsersPage(Driver);
            manageUsersPage.ClickActivateUserAccessButton();
        }
        [Then(@"I select First Name")]
        public void ThenISelectFirstName(Table table)
        {
            ManageUsersPage manageUsersPage = new ManageUsersPage(Driver);
            manageUsersPage.ClickFirstName(table);
        }
        [Then(@"I select speciality")]
        public void ThenISelectSpeciality(Table table)
        {
            ManageUsersPage manageUsersPage = new ManageUsersPage(Driver);
            manageUsersPage.ClickSpeciality(table);
        }
        [Then(@"I press Update contact button")]
        public void ThenIPressUpdateContactButton()
        {
            ManageUsersPage manageUsersPage = new ManageUsersPage(Driver);
            manageUsersPage.ClickUpdateContact();
        }
        [Then(@"I click Add user menu")]
        public void ThenIClickAddUserMenu()
        {
            ManageUsersPage manageUsersPage = new ManageUsersPage(Driver);
            manageUsersPage.ClickAddUserMenu();
        }
        [Then(@"I click Manage Users submenu")]
        public void ThenIClickManageUsersSubmenu()
        {
            ManageUsersPage manageUsersPage = new ManageUsersPage(Driver);
            manageUsersPage.ClickManageUsers();
        }


        [Then(@"I press select button")]
        public void ThenIPressSelectButton()
        {
            ManageUsersPage manageUsersPage = new ManageUsersPage(Driver);
            manageUsersPage.ClickSelectButton();
        }
        [Then(@"I press Apply Filter button")]
        public void ThenIPressApplyFilterButton()
        {
            ManageUsersPage manageUsersPage = new ManageUsersPage(Driver);
            manageUsersPage.ClickApplyFilterButton();
        }
        [Then(@"I select Services and assign Case Worker for SSS Request")]
        public void ThenISelectServicesAndAssignCaseWorkerForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.SelectServicesAndAssign(table);
        }
        [Then(@"I select and close services for SSS Request")]
        public void ThenISelectAndCloseServicesForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.SelectServicesClose(table);
        }
        [Then(@"I select Services and assign Case Worker")]
        public void ThenISelectServicesAndAssignCaseWorker(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.SelectServicesAndAssign(table);
        }

        [Then(@"I select and close services")]
        public void ThenISelectAndCloseServices(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.SelectServicesClose(table);
        }
        [Then(@"I select student for SSS Request")]
        public void ThenISelectStudentForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.SelectStudent(table);
        }

        [Then(@"I select Create Student Support Services Request from the Case menu")]
        public void ThenISelectCreateStudentSupportServicesRequestFromTheCaseMenu()
        {
            SOCSHomepage homePage = new SOCSHomepage(Driver);
            homePage.HoverCasesMenu();

            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.ClickSSSRequest();
        }

        [Then(@"I fill in Student Support Services request details")]
        public void ThenIFillInStudentSupportServicesRequestDetails(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.FillSSSRequestDetails(table);
        }
        [Then(@"I update SSS Request by selecting Communication or Speech")]
        public void ThenIUpdateSSSRequestBySelectingCommunicationOrSpeech()
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.UpdateOthersCommunicationorSpeech();
        }
        [Then(@"I will get the SSS Request case number")]
        public void ThenIWillGetTheSSSRequestCaseNumber()
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            strSSSRequestCaseNumber = SSSPage.GetSSSRequestCaseNumber();
        }
        [Then(@"I filter and select by newly created SSS Request case number")]
        public void ThenIFilterAndSelectByNewlyCreatedSSSRequestCaseNumber()
        {
            CasesProgramListPage casesProgramListPage = new CasesProgramListPage(Driver);
            casesProgramListPage.FilterandSelectbyNewlyCreatedCaseNumber(strSSSRequestCaseNumber);
        }
        [Then(@"I set Pre-Intervention Survey Questions for SSS Request")]
        public void ThenISetPre_InterventionSurveyQuestionsForSSSRequest()
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.PreInterventionSurveyQuestion();
        }
        [Then(@"I upload supporting documents for SSS Request")]
        public void ThenIUploadSupportingDocumentsForSSSRequest()
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.UploadSupportingDocuments();
        }
        [Then(@"I add/save Student Case Notes for SSS Request")]
        public void ThenIAddSaveStudentCaseNotesForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.AddNoteandSave(table);
        }
        [Then(@"I attach Consent Form for SSS Request")]
        public void ThenIAttachConsentFormForSSSRequest()
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.AttachConsentForm();
        }
        [Then(@"I save and submit existing SSS Request")]
        public void ThenISaveAndSubmitExistingSSSRequest()
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.SubmitCase();
        }
        [Then(@"I request amend specialist for SSS Request")]
        public void ThenIRequestAmendSpecialistForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.AmendSpecialist(table);
        }

        [Then(@"I save the changes in SSS Request")]
        public void ThenISaveTheChangesInSSSRequest()
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.SaveCase();
        }
        [Then(@"I close the SSS request case")]
        public void ThenICloseTheSSSRequestCase()
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.CloseCase();
        }
        [Then(@"I made some action for SSS Request")]
        public void ThenIMadeSomeActionForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.Action(table);
        }

        [Then(@"I assign a case leader for SSS Request")]
        public void ThenIAssignACaseLeaderForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.AssignCaseLeader(table);
        }



        [Then(@"I select Service for SSS Request")]
        public void ThenISelectServiceForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.SelectService(table);
        }

        [Then(@"I assign a case worker for SSS Request")]
        public void ThenIAssignACaseWorkerForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.AssignCaseWorker(table);
        }

        [Then(@"I close the service for SSS Request")]
        public void ThenICloseTheServiceForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.CloseService(table);
        }
        [Then(@"I return the SSS Request case")]
        public void ThenIReturnTheSSSRequestCase()
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.ReturnCase();
        }
        [Then(@"I reopen the case SSS request case")]
        public void ThenIReopenTheCaseSSSRequestCase()
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.ReopenCase();
        }
        [Then(@"I select Close case action for SSS Request")]
        public void ThenISelectCloseCaseActionForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.CloseCaseAction(table);
        }
        [Then(@"I select Return for amendment action for SSS Request")]
        public void ThenISelectReturnForAmendmentActionForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.ReturnCaseAction(table);
        }
        [Then(@"I re-assign a case leader for SSS Request")]
        public void ThenIRe_AssignACaseLeaderForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.ReAssignCaseLeader(table);
        }
        [Then(@"I indicate that case activities have been completed for SSS Request")]
        public void ThenIIndicateThatCaseActivitiesHaveBeenCompletedForSSSRequest()
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.CaseActivitiesCompleted();
        }

        [Then(@"I update Priority for SSS Request")]
        public void ThenIUpdatePriorityForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.UpdatePriority(table);
        }
        [Then(@"I update Case Plan for SSS Request")]
        public void ThenIUpdateCasePlanForSSSRequest(Table table)
        {
            StudentSupportServicesPage SSSPage = new StudentSupportServicesPage(Driver);
            SSSPage.UpdateCasePlan(table);
        }
        [Then(@"I request amend specialist for School Case")]
        public void ThenIRequestAmendSpecialistForSchoolCase(Table table)
        {
            SchoolCasePage schoolCasePage = new SchoolCasePage(Driver);
            schoolCasePage.AmendSpecialist(table);
        }
    }
}
