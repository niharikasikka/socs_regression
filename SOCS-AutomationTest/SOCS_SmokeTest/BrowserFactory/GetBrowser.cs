﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;

namespace SOCS_SmokeTest.BrowserFactory
{
    public static class GetBrowser
    {
        private static IWebDriver Driver;
        public static IWebDriver InitBrowser(string browser)
        {
            switch (browser.ToLower())
            {
                case "chrome":
                    //var options = new ChromeOptions();
                    //options.PageLoadStrategy = PageLoadStrategy.Normal;
                    Driver = new ChromeDriver();
                    //Driver = new ChromeDriver();
                    //Driver.Manage().Timeouts().PageLoad = TimeSpan.FromMinutes(3);
                    break;
                case "ie":
                    Driver = new InternetExplorerDriver();
                    break;
                case "firefox":
                    Driver = new FirefoxDriver();
                    Driver.Manage().Timeouts().PageLoad = TimeSpan.FromMinutes(3);
                    break;
            }
            Driver.Manage().Window.Maximize();
            return Driver;
        }
        public static void Cleanup()
        {
            Driver.Close();
            Driver.Quit();
        }


    }
}
