﻿using SOCS_SmokeTest.BaseClass;
using SOCS_SmokeTest.UtilityClasses;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Data;
using TechTalk.SpecFlow;

namespace SOCS_SmokeTest.Pages
{
    class CasesProgramListPage : BaseApplicationPage
    {
        public CasesProgramListPage(IWebDriver driver) : base(driver)
        { }
        public By CaseProgramListForm_Element => By.XPath("//h2[contains(text(),'Cases/Programs List')]");

        //Case Details Section
        public By CaseNumberInput_Element => By.CssSelector("#FilterTicketNumber");
        public By StudentFirstNameInput_Element => By.CssSelector("#FilterStudentFirstName");
        public By AdvancedSearchLink_Element => By.CssSelector("#advancedSearchOption");

        //Dates Section
        public By SubmittedDateFromDate_Element => By.CssSelector("#FilterDateReferralSubmittedFrom");

        //Additional Filters
        public By AdditionalFiltersPanel_Element => By.XPath("//div[@id='searchAdditionalFilters']//div[@class='panel-body']");
        public By AdditionalFiltersSection_Element => By.CssSelector("#searchAdditionalFilters");
        public By ProgramRequestsCheckbox_Element => By.CssSelector("#FilterProgramsOnly");

        //Bulk Case Assignment Section
        public By EnableBulkAssignmentCheckbox_Element => By.CssSelector("#BatchAssign");
        public By PriorityDropdown_Element => By.CssSelector("#Priority");
        public By SelectUserDropdown_Element => By.CssSelector("#Priority");
        public By GuardingConsentCheckbox_Element => By.CssSelector("#GuardianConsentReceived");
        public By StartProcessingButton_Element => By.CssSelector("#cmdBatchProcess");

        //Buttons
        public By ApplyFilterButton_Element => By.CssSelector("#Submit1");


        //Filter results
        public By CaseNumberLink_Element => By.XPath("//div[@id='CaseListPartialView']//tbody//tr[1]//td[1]");
        public By CaseNumberLink2_Element => By.XPath("//div[@id='CaseListPartialView']//tbody//tr[1]//td[1]");
        
        

        

        internal void FilterandSelectbyCaseNumber(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
                wait.Until(x => x.FindElement(CaseProgramListForm_Element));

                //filter by case number
                wait.Until(x => x.FindElement(CaseNumberInput_Element));
                Driver.FindElement(CaseNumberInput_Element).SendKeys(row.ItemArray[0].ToString());

                //removed submitted date
                wait.Until(x => x.FindElement(SubmittedDateFromDate_Element));
                Driver.FindElement(SubmittedDateFromDate_Element).Clear();

                //Click Apply Filter
                wait.Until(x => x.FindElement(ApplyFilterButton_Element));
                Driver.FindElement(ApplyFilterButton_Element).Click();

                wait.Until(x => x.FindElement(By.XPath("//*[@id='CaseGrid']/table/tbody/tr/td")));
                //check whether input text exists
                if (Driver.FindElement(By.XPath("//*[@id='CaseGrid']/table/tbody/tr/td")).Text.Equals("No records to display."))
                {
                    throw new Exception("Case number may not be valid. No records to display.");
                }
                else
                {
                    //Click Case Number link
                    wait.Until(x => x.FindElement(CaseNumberLink_Element));
                    Driver.FindElement(CaseNumberLink_Element).Click();
                }  
            }            
        }
        internal void FilterandSelectbyNewlyCreatedCaseNumber(string strCaseNumber)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(CaseProgramListForm_Element));

            //filter by case number
            wait.Until(x => x.FindElement(CaseNumberInput_Element));
            Driver.FindElement(CaseNumberInput_Element).SendKeys(strCaseNumber);

            //removed submitted date
            wait.Until(x => x.FindElement(SubmittedDateFromDate_Element));
            Driver.FindElement(SubmittedDateFromDate_Element).Clear();

            //Click Apply Filter
            wait.Until(x => x.FindElement(ApplyFilterButton_Element));
            Driver.FindElement(ApplyFilterButton_Element).Click();

            
            //Click Case Number link
            wait.Until(x => x.FindElement(CaseNumberLink_Element));
            Driver.FindElement(CaseNumberLink_Element).Click();
        }
        internal void FilterandSelectbyNewlyCreatedProgramCaseNumber(string strProgramCaseNumber)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(CaseProgramListForm_Element));

            //filter by case number
            wait.Until(x => x.FindElement(CaseNumberInput_Element));
            Driver.FindElement(CaseNumberInput_Element).SendKeys(strProgramCaseNumber);

            //removed submitted date
            wait.Until(x => x.FindElement(SubmittedDateFromDate_Element));
            Driver.FindElement(SubmittedDateFromDate_Element).Clear();

            //Click Apply Filter
            wait.Until(x => x.FindElement(ApplyFilterButton_Element));
            Driver.FindElement(ApplyFilterButton_Element).Click();

            //Click Case Number link
            wait.Until(x => x.FindElement(CaseNumberLink_Element));
            Driver.FindElement(CaseNumberLink_Element).Click();
        }
        internal void SelectNewlyCreatedCaseNumber(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(CaseProgramListForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //filter by student first name
                wait.Until(x => x.FindElement(StudentFirstNameInput_Element));
                Driver.FindElement(StudentFirstNameInput_Element).SendKeys(row.ItemArray[0].ToString());

                /*//removed submitted date
                wait.Until(x => x.FindElement(SubmittedDateFromDate_Element));
                Driver.FindElement(SubmittedDateFromDate_Element).Clear();*/

                //Click Apply Filter
                wait.Until(x => x.FindElement(ApplyFilterButton_Element));
                Driver.FindElement(ApplyFilterButton_Element).Click();

                //Click Case Number link
                wait.Until(x => x.FindElement(CaseNumberLink2_Element));
                Driver.FindElement(CaseNumberLink2_Element).Click();
            }            
        }
        internal void SelectNewlyCreatedProgramRequestCaseNumber()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(CaseProgramListForm_Element));

            //filter by Programs Only
            wait.Until(x => x.FindElement(AdditionalFiltersSection_Element));
            Driver.FindElement(AdditionalFiltersSection_Element).Click();
            wait.Until(x => x.FindElement(AdditionalFiltersPanel_Element));
            wait.Until(x => x.FindElement(ProgramRequestsCheckbox_Element));
            Driver.FindElement(ProgramRequestsCheckbox_Element).Click();

            /*//removed submitted date
            wait.Until(x => x.FindElement(SubmittedDateFromDate_Element));
            Driver.FindElement(SubmittedDateFromDate_Element).Clear();*/

            //Click Apply Filter
            wait.Until(x => x.FindElement(ApplyFilterButton_Element));
            Driver.FindElement(ApplyFilterButton_Element).Click();

            //Click Case Number link
            wait.Until(x => x.FindElement(CaseNumberLink2_Element));
            Driver.FindElement(CaseNumberLink2_Element).Click();
        }
        internal void SelectIssue(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(CaseProgramListForm_Element));

            //Click Advanced Search
            wait.Until(x => x.FindElement(AdvancedSearchLink_Element));
            Driver.FindElement(AdvancedSearchLink_Element).Click();

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                
                //filter by student first name
                wait.Until(x => x.FindElement(StudentFirstNameInput_Element));
                Driver.FindElement(StudentFirstNameInput_Element).SendKeys(row.ItemArray[0].ToString());

                /*//removed submitted date
                wait.Until(x => x.FindElement(SubmittedDateFromDate_Element));
                Driver.FindElement(SubmittedDateFromDate_Element).Clear();*/

                //Click Apply Filter
                wait.Until(x => x.FindElement(ApplyFilterButton_Element));
                Driver.FindElement(ApplyFilterButton_Element).Click();

                //Click Case Number link
                wait.Until(x => x.FindElement(CaseNumberLink2_Element));
                Driver.FindElement(CaseNumberLink2_Element).Click();
            }
        }
        internal void BulkAssignment(Table table)
        {
            //NOTE: Bulk Assingment isn't working as per Margarita
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(CaseProgramListForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {

                //Click the Enable Bulk Assignment checkbox
                wait.Until(x => x.FindElement(EnableBulkAssignmentCheckbox_Element));
                Driver.FindElement(EnableBulkAssignmentCheckbox_Element).Click();

                //select from Priority dropdown
                wait.Until(x => x.FindElement(PriorityDropdown_Element));
                SelectElement priority = new SelectElement(Driver.FindElement(PriorityDropdown_Element));
                priority.SelectByText(row.ItemArray[0].ToString());

                //select from Select User dropdown
                wait.Until(x => x.FindElement(SelectUserDropdown_Element));
                Driver.FindElement(EnableBulkAssignmentCheckbox_Element).SendKeys(row.ItemArray[1].ToString());
                
                //Click Guarding Consent Received and Accepted checkbox
                wait.Until(x => x.FindElement(GuardingConsentCheckbox_Element));
                Driver.FindElement(GuardingConsentCheckbox_Element).Click();


                //NOTE: still need to add selection script
                //Click Start Processing button
                wait.Until(x => x.FindElement(StartProcessingButton_Element));
                Driver.FindElement(StartProcessingButton_Element).Click();
            }
        }
    }
}
