﻿using SOCS_SmokeTest.BaseClass;
using SOCS_SmokeTest.UtilityClasses;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;
using NUnit.Framework;
using System.Linq;
using System.Windows.Forms;

namespace SOCS_SmokeTest.Pages
{
    class SOCSHomepage : BaseApplicationPage
    {
        
        public SOCSHomepage(IWebDriver driver) : base(driver)
        { }
        
        public IWebElement UserId_Element => Driver.FindElement(By.CssSelector("#username"));
        public IWebElement Password_Element => Driver.FindElement(By.CssSelector("#password"));
        public IWebElement Submit_Element => Driver.FindElement(By.XPath("//div[@id='main']//form//div//fieldset//table//tbody//tr//td//p//input"));
        public By Cases_Element => By.XPath("//body[@id='main-body']/div[@class='page_margins']/div[@class='page']/div[@id='MenuPlaceHolder']/div[@id='pad']/ul[@id='menu']/li[2]/a[1]");
        public By SignOut_Link => By.XPath("//a[contains(text(),'Sign Out')]");
        public By UserName_Element => By.XPath("//span[@class='breadCrumb module']");
        
        internal void GetUrl()
        {
            string url = CommonFunctions.GetResourceValue("SOCSUrl_dev");
            Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(80);
            //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(80);
            Driver.Navigate().GoToUrl(url);
        }
        public string EnterUsername()
        {
            string userName = CommonFunctions.GetResourceValue("SOCS00_1UID_Internal");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            return userName;
        }
        internal void EnterPassword()
        {

            string password = CommonFunctions.GetResourceValue("SOCS00_2Pwd_Internal");
            Password_Element.SendKeys(password);
        }
        public string EnterSysAdminUsername()
        {
            string userName = CommonFunctions.GetResourceValue("SOCS01_1UID_SystemAdmin");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            return userName;
        }
        internal void EnterSysAdminPassword()
        {

            string password = CommonFunctions.GetResourceValue("SOCS01_2Pwd_SystemAdmin");
            Password_Element.SendKeys(password);
        }

        public string EnterVisitingTeacherTeamLeaderUsername()
        {
            string userName = CommonFunctions.GetResourceValue("SOCS02_1UID_VisitingTeacherTeamLeader");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            return userName;
        }
        internal void EnterVisitingTeacherTeamLeaderpassword()
        {

            string password = CommonFunctions.GetResourceValue("SOCS02_2Pwd_VisitingTeacherTeamLeader");
            Password_Element.SendKeys(password);
        }
        public string EnterVisitingTeacherUsername()
        {
            string userName = CommonFunctions.GetResourceValue("SOCS03_1UID_VisitingTeacher");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            return userName;
        }
        internal void EnterVisitingTeacherpassword()
        {

            string password = CommonFunctions.GetResourceValue("SOCS03_2Pwd_VisitingTeacher");
            Password_Element.SendKeys(password);
        }
        public string EnterSSSTeamLeaderUsername()
        {
            string userName = CommonFunctions.GetResourceValue("SOCS04_1UID_SSSTeamLeader");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            return userName;
        }
        internal void EnterSSSTeamLeaderpassword()
        {

            string password = CommonFunctions.GetResourceValue("SOCS04_2Pwd_SSSTeamLeader");
            Password_Element.SendKeys(password);
        }
        public string EnterAuthorisedSchoolOfficerUsername()
        {
            string userName = CommonFunctions.GetResourceValue("SOCS05_1UID_AuthorisedSchoolOfficer");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            return userName;
        }
        internal void EnterAuthorisedSchoolOfficerpassword()
        {

            string password = CommonFunctions.GetResourceValue("SOCS05_2Pwd_AuthorisedSchoolOfficer");
            Password_Element.SendKeys(password);
        }
        public string EnterSchoolPrincipalUsername()
        {
            string userName = CommonFunctions.GetResourceValue("SOCS06_1UID_SchoolPrincipal");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            return userName;
        }
        internal void EnterSchoolPrincipalpassword()
        {

            string password = CommonFunctions.GetResourceValue("SOCS06_2Pwd_SchoolPrincipal");
            Password_Element.SendKeys(password);
        }
        public string EnterSSSAdminSupportUsername()
        {
            string userName = CommonFunctions.GetResourceValue("SOCS07_1UID_SSSAdminSupport");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            return userName;
        }
        internal void EnterSSSAdminSupportpassword()
        {

            string password = CommonFunctions.GetResourceValue("SOCS07_2Pwd_SSSAdminSupport");
            Password_Element.SendKeys(password);
        }
        public string EnterBranchManagerUsername()
        {
            string userName = CommonFunctions.GetResourceValue("SOCS08_1UID_BranchManager");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            return userName;
        }
        internal void EnterBranchManagerpassword()
        {

            string password = CommonFunctions.GetResourceValue("SOCS08_2Pwd_BranchManager");
            Password_Element.SendKeys(password);
        }
        public string EnterHelpDeskUsername()
        {
            string userName = CommonFunctions.GetResourceValue("SOCS09_1UID_HelpDesk");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            return userName;
        }
        internal void EnterHelpDeskpassword()
        {

            string password = CommonFunctions.GetResourceValue("SOCS09_2Pwd_HelpDesk");
            Password_Element.SendKeys(password);
        }
        public string EnterSSSStaffMemberUsername()
        {
            string userName = CommonFunctions.GetResourceValue("SOCS10_1UID_SSSStaffMember");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            return userName;
        }
        internal void EnterSSSStaffMemberpassword()
        {

            string password = CommonFunctions.GetResourceValue("SOCS10_2Pwd_SSSStaffMember");
            Password_Element.SendKeys(password);
        }
        internal void ClickButton()
        {
            Submit_Element.Click();
        }
        internal void SignOut()
        {
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            waitAgency.Until(c => c.FindElement(SignOut_Link));
            Driver.FindElement(SignOut_Link).Click();
        }
        internal void HoverCasesMenu()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(Cases_Element));

            Actions action = new Actions(Driver);
            action.MoveToElement(Driver.FindElement(Cases_Element)).Perform();
        }
        internal void ClickCasesMenu()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(Cases_Element));
            Driver.FindElement(Cases_Element).Click();
        }
        
        public void VerifyUserLoggedIn(string userName)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(UserName_Element));
            if (userName.Equals("99999156"))
            {
                if (Driver.FindElement(UserName_Element).Text.Contains("SOCS SystemAdmin"))
                { }
                else
                {
                    throw new Exception("Incorrect user logged in");
                }
            }
            else
            {
                if (userName.Equals("99999155"))
                {
                    if (Driver.FindElement(UserName_Element).Text.Contains("SOCS VisitingTeacherTeamLeader"))
                    { }
                    else
                    {
                        throw new Exception("Incorrect user logged in");
                    }
                }
                else
                {
                    if (userName.Equals("99999154"))
                    {
                        if (Driver.FindElement(UserName_Element).Text.Contains("SOCS VisitingTeacher"))
                        { }
                        else
                        {
                            throw new Exception("Incorrect user logged in");
                        }
                    }
                    else
                    {
                        if (userName.Equals("99999152"))
                        {
                            if (Driver.FindElement(UserName_Element).Text.Contains("SOCS SSSTeamLeader"))
                            { }
                            else
                            {
                                throw new Exception("Incorrect user logged in");
                            }
                        }
                        else
                        {
                            if (userName.Equals("99999150"))
                            {
                                if (Driver.FindElement(UserName_Element).Text.Contains("SOCS AuthorisedSchoolOfficer"))
                                { }
                                else
                                {
                                    throw new Exception("Incorrect user logged in");
                                }
                            }
                            else
                            {
                                if (userName.Equals("99999151"))
                                {
                                    if (Driver.FindElement(UserName_Element).Text.Contains("SOCS SchoolPrincipal"))
                                    { }
                                    else
                                    {
                                        throw new Exception("Incorrect user logged in");
                                    }
                                }
                                else
                                {
                                    if (userName.Equals("99999158"))
                                    {
                                        if (Driver.FindElement(UserName_Element).Text.Contains("SOCS SSSAdminSupport"))
                                        { }
                                        else
                                        {
                                            throw new Exception("Incorrect user logged in");
                                        }
                                    }
                                    else
                                    {
                                        if (userName.Equals("99999157"))
                                        {
                                            if (Driver.FindElement(UserName_Element).Text.Contains("SOCS BranchManager"))
                                            { }
                                            else
                                            {
                                                throw new Exception("Incorrect user logged in");
                                            }
                                        }
                                        else
                                        {
                                            if (userName.Equals("99999159"))
                                            {
                                                if (Driver.FindElement(UserName_Element).Text.Contains("SOCS HelpDesk"))
                                                { }
                                                else
                                                {
                                                    throw new Exception("Incorrect user logged in");
                                                }
                                            }
                                            else
                                            {
                                                if (userName.Equals("99999153"))
                                                {
                                                    if (Driver.FindElement(UserName_Element).Text.Contains("SOCS SSSStaffMember"))
                                                    { }
                                                    else
                                                    {
                                                        throw new Exception("Incorrect user logged in");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Thread.Sleep(2000);
        }
    }
    
}
