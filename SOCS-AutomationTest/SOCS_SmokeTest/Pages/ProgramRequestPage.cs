﻿using SOCS_SmokeTest.BaseClass;
using SOCS_SmokeTest.UtilityClasses;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;
using NUnit.Framework;
using System.Linq;
using System.Windows.Forms;

namespace SOCS_SmokeTest.Pages
{
    class ProgramRequestPage : BaseApplicationPage
    {
        public ProgramRequestPage(IWebDriver driver) : base(driver)
        { }
        //Program Request submenu Element
        public By CreateProgramRequest_Element => By.XPath("//span[contains(text(),'Create Program Request')]");
        public By ConfirmationMessage_Element => By.CssSelector("#confirmationMessage");

        //Program Request Elements
        public By ProgramRequestForm_Element => By.XPath("//h2[contains(text(),'Program Request')]");
        public By PrimaryPresentingIssueDropdown_Element => By.CssSelector("#PrimaryPresentingIssue");


        //Program Request Sections Elements
        public By CurriculumLearningSection_Element => By.CssSelector("#CurriculumLearningPPI");
        public By CaseDetailsSection_Element => By.CssSelector("#cd");

        //Case Details Section Elements
        public By Casenumber_Element => By.CssSelector("#TicketNumber");
        public By CaseStatus_Element => By.CssSelector("#CaseStatus");
        public By ChooseFileButton_Element => By.CssSelector("#FileAttachment");
        public By UploadButton_Element => By.CssSelector("#cmdFileAttachment");
        public By AddConsentFormButton_Element => By.XPath("//a[contains(text(),'Add Consent Form')]");
        public By CaseCommentInput_Element => By.CssSelector("#CaseComments");
        public By AmendSpecialistRequestedButton_Element => By.CssSelector("#amendSpecialistId");
        public By SpeechPathologistCheckbox_Element => By.CssSelector("#SpecialistRequestedAmendedOptions-checkbox-item-1");

        //Curriculum/Learning Section Elements
        public By CurriculumLearningConcern_DifficultystayingontaskCheckbox_Element => By.CssSelector("#CurriculumLearningPPIMultiIDOptions-checkbox-item-0");

        //Whole School Issues Section Elements

        //Student Case Note Section Elements
        public By SCNChooseFileButton_Element => By.CssSelector("#scnFileAttachment");
        public By SCN_DurationHrsDropdown_Element => By.CssSelector("#sss_Date_Hour");
        public By SCN_DurationMinDropdown_Element => By.CssSelector("#sss_Date_Minute");
        public By SCN_SubjectInput_Element => By.CssSelector("#sss_name");
        public By SCN_SchoolActionDropdown_Element => By.CssSelector("#sss_schoolaction");
        public By SCN_NotesInput_Element => By.CssSelector("#sss_Notes");
        public By SCN_SaveCaseNoteButton_Element => By.CssSelector("#cmdCreateSchoolCaseNote");
        public By SCN_StudentCaseNoteSectionForm_Element => By.XPath("//div[@id='scn']//div[@class='panel-body']");
        public By SCN_StudentCaseNoteSectionGrid_Element => By.CssSelector("#SchoolCaseNotesGridDiv");
        //Consent Elements
        public By AddConsentForm_Element => By.XPath("//h2[contains(text(),'Add New Consent Form')]");
        public By ConsentChooseFileButton_Element => By.CssSelector("#FileAttachment");
        public By UploadFormButton_Element => By.CssSelector("#cmdUpload");
        public By UploadFormMessage_Element => By.XPath("//span[@class='message']");
        public By CurrentConsentForm_Element => By.XPath("//label[contains(text(),'Current Consent Form')]");
        public By ReturnToCaseButton_Element => By.CssSelector("#cmdReturnToCase");

        //Program Request button Elements
        public By SaveDraftButton_Element => By.CssSelector("#cmdCreateCase");
        public By SaveCaseButton_Element => By.CssSelector("#cmdUpdateCase");

        public By AddCaseNoteButton_Element => By.CssSelector("#cmdAddCaseNote");
        public By SaveandSubmitCaseButton_Element => By.CssSelector("#cmdSubmit");


        //***********************************************************************************
        //New Case element
        public By NewCaseForm_Element => By.XPath("//h2[contains(text(),'New Case')]");
        public By StudentFNInput_Element => By.CssSelector("#FilterFirstName");
        public By SelectStudentcheckbox_Element => By.XPath("//div[@id='StudentList']//tbody//tr[1]//td[1]");
        public By GuardianConsentCheckbox_Element => By.CssSelector("#GuardianConsentCheckBox");
        public By NextButton_Element => By.CssSelector("#cmdWizardNext");
        public By ApplyFilterButton_Element => By.CssSelector("#cmdSubmitSearch");

        public By SchoolCaseForm_Element => By.XPath("//h2[contains(text(),'School Case')]");
        public By SchoolNameNumInput_Element => By.CssSelector("#FilterSchoolNameOrNumber");


        public By StudentSelectLink_Element => By.XPath("//a[contains(text(),'Select')]");


        public By RequestTypeDropdown_Element => By.CssSelector("#RequestType");

        public By AttendanceSection_Element => By.CssSelector("#AttendancePPI");


        public By AttendanceConcern_ChronicIllnessCheckbox_Element => By.CssSelector("#AttendancePPIMultiIDOptions-checkbox-item-0");
        public By BehaviourConcern_AggressionangerCheckbox_Element => By.CssSelector("#BehaviourPPIMultiIDOptions-checkbox-item-0");
        public By SocialEmotionalConcern_AggressionangerCheckbox_Element => By.CssSelector("#SocialEmotionalPPIMultiIDOptions-checkbox-item-0");
        public By SocialEmotionalConcern_AngerCheckbox_Element => By.CssSelector("#SocialEmotionalPPIMultiIDOptions-checkbox-item-1");
        public By SocialEmotionalConcern_BulliedCheckbox_Element => By.CssSelector("#SocialEmotionalPPIMultiIDOptions-checkbox-item-2");

        public By CommunicationSpeechConcern_ArticulationCheckbox_Element => By.CssSelector("#CommunicationSpeechPPIMultiIDOptions-checkbox-item-0");

        public By ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element => By.CssSelector("#ServiceTypeRequestedMultiIDOptions-checkbox-item-0");
        public By Specialist_Psychologist_Checkbox_Element => By.CssSelector("#SpecialistRequestedMultiIDOptions-checkbox-item-0");
        public By SchoolInformationInput_Element => By.CssSelector("#InformationReStudent");
        public By AgenciesInformationInput_Element => By.CssSelector("#InformationFromSpecialists");
        public By FamilyInformationInput_Element => By.CssSelector("#InformationFromFamily");
        public By GuardianCheckbox_Element => By.CssSelector("#GuardianConsent");

        public By UpdateSchoolCaseButton_Element => By.CssSelector("#cmdUpdateCase");

        public By CloseCaseCommentInput_Element => By.CssSelector("#CompletionComment");
        public By CloseCaseCommitButton_Element => By.CssSelector("#cmdCloseCaseCommit");
        public By ReturnCaseCommentInput_Element => By.CssSelector("#ReturnForAmendmentComment");
        public By ReturnCaseCommitButton_Element => By.CssSelector("#cmdReturnCaseCommit");

        //School Case sections
        public By BehaviourSection_Element => By.CssSelector("#BehaviourPPI");

        public By CommunicationSpeechSection_Element => By.CssSelector("#CommunicationSpeechPPI");
        public By SocialEmotionalSection_Element => By.CssSelector("#SocialEmotionalPPI");
        public By PreInterventionSection_Element => By.CssSelector("#fq");

        public By StudentDetailsSection_Element => By.CssSelector("#sd");
        public By SCN_StudentCaseNoteSection_Element => By.CssSelector("#scn");
        public By SOCSection_Element => By.CssSelector("#soc");
        public By ServiceMilestonesSection_Element => By.CssSelector("#ad");
        public By ServicesSection_Element => By.CssSelector("#Services");
        public By ConsentFormsSection_Element => By.CssSelector("#ConsentForms");
        public By PreReferralFormsSection_Element => By.CssSelector("#PreReferralForms");
        public By PostInterventionSection_Element => By.CssSelector("#fqpost");
        public By CaseHistorySection_Element => By.CssSelector("#fsHistory");
        public By SchoolMovementHistorySection_Element => By.CssSelector("#smHistory");

        //Generic buttons
        public By UpdateButton_Element => By.CssSelector("#cmdSave");
        public By CancelButton_Element => By.CssSelector("#cmdCancel");
        public By SchoolCaseHeading_Element => By.XPath("//span[@class='heading']");

        public By CloseCasebutton_Element => By.CssSelector("#cmdCloseCase");
        public By ReturnCasebutton_Element => By.CssSelector("#cmdReturnCase");
        public By ReopenCasebutton_Element => By.CssSelector("#Submit1");
        public By PriorityDropdown_Element => By.CssSelector("#PriorityCode");
        public By ActionSection_Element => By.CssSelector("#action2");
        public By ActionDropdown_Element => By.CssSelector("#Action");
        public By RecommendationCategoryDropdown_Element => By.CssSelector("#RecommendationCategory");
        public By GuardianObtainedCheckbox_Element => By.CssSelector("#GuardianObtained");
        public By AssignCaseLeaderbutton_Element => By.CssSelector("#cmdAssignCaseManagerButton");
        public By ReAssignCaseLeaderbutton_Element => By.CssSelector("#ReassignCaseManagerButton");
        public By AssignCaseLeaderForm_Element => By.XPath("//h2[contains(text(),'Assign case leader - Select Contact.')]");
        public By AssignCaseWorkerForm_Element => By.XPath("//h2[contains(text(),'Assign case worker - Select Contact.')]");

        //filter elements
        public By FilterFirstNameInput_Element => By.CssSelector("#FilterFirstName");
        public By FilterLastNameInput_Element => By.CssSelector("#FilterLastName");
        public By FilterUserRoleDropdown_Element => By.CssSelector("#FilterUserRole");
        public By FilterSpecialtyDropdown_Element => By.CssSelector("#FilterSpeciality");
        public By FilterApplyFilterButton_Element => By.CssSelector("#cmdSearch");
        public By FilterSelectLink_Element => By.XPath("//a[contains(text(),'Select')]");

        public By CaseLeader_Element => By.CssSelector("#CaseManagerIDName");
        public By CaseWorker_Element => By.CssSelector("#ServiceResponsibleIDName");

        //Service Elements
        public By ServicesForm_Element => By.XPath("//div[@id='Services']//div[@class='panel-body']");
        public By ServiceDetailsForm_Element => By.CssSelector("#serviceDetailsForm");
        public By ServiceCloseCasePlan_Element => By.CssSelector("#cmdCloseService");
        public By ServiceDetailsAssignButton_Element => By.CssSelector("#AssignServiceResponsibleContactButton");
        public By ServiceDetailsClosedDates_Element => By.CssSelector("#ServiceClosedDate");

        //Case Notes Elements
        public By CaseNoteTitleInput_Element => By.CssSelector("#Subject");
        public By CaseNoteForm_Element => By.XPath("//h2[contains(text(),'Case Note')]");
        public By CaseNoteAdd_Element => By.XPath("//a[@class='t-grid-action t-button t-state-default']");
        public By CaseNoteLink_Element => By.XPath("//a[contains(text(),'Click to add a Case Note/Event')]");
        public By CaseNoteEventDate_Element => By.XPath("//img[@class='ui-datepicker-trigger']");
        public By CaseNoteEventDate_Today_Element => By.XPath("//td[contains(@class,'ui-datepicker-today')]");
        public By CaseNoteEventTypeDropdown_Element => By.CssSelector("#EventType");
        public By CaseNoteEventServiceDelHrsDropdown_Element => By.CssSelector("#TimeHours");
        public By CaseNoteEventServiceDelMinDropdown_Element => By.CssSelector("#TimeMins");
        public By CaseNoteEventTravelHrsDropdown_Element => By.CssSelector("#TravelTimeHours");
        public By CaseNoteEventTravelMinDropdown_Element => By.CssSelector("#TravelTimeMinutes");
        public By CaseNote_NoteInput_Element => By.CssSelector("#ActivityNotes");
        public By CaseNoteUploadButton_Element => By.CssSelector("#cmdFileAttachment");

        //Review Questions Form Elements
        public By ReviewQuestionsForm_Element => By.XPath("//h2[contains(text(),'Review questions')]");



        public By InterventionOutcomesDropdown_Element => By.CssSelector("#FeedbackOptionsQuestion8");
        public By InterventionAimsDropdown_Element => By.CssSelector("#FeedbackOptionsQuestion9");
        public By AttendanceConcern_Illness_Element => By.CssSelector("#AttendancePPIMultiIDOptions-checkbox-item-0");
        public By AttendanceConcern_Trunacy_Element => By.CssSelector("#AttendancePPIMultiIDOptions-checkbox-item-5");
        public By Specialist_SSSO_Checkbox_Element => By.CssSelector("#SpecialistRequestedMultiIDOptions-checkbox-item-5");
        public By Behaviour_Anger_Element => By.CssSelector("#BehaviourPPIMultiIDOptions-checkbox-item-0");
        public By CommunicationSpeechConcern_ExpressiveLanguage_Element => By.CssSelector("#CommunicationSpeechPPIMultiIDOptions-checkbox-item-1");
        public By CommunicationSpeechConcern_Feeding_Element => By.CssSelector("#CommunicationSpeechPPIMultiIDOptions-checkbox-item-2");
        public By CommunicationSpeechConcern_PragmaticLanguage_Element => By.CssSelector("#CommunicationSpeechPPIMultiIDOptions-checkbox-item-3");
        public By CommunicationSpeechConcern_SocialLanguage_Element => By.CssSelector("#CommunicationSpeechPPIMultiIDOptions-checkbox-item-5");
        public By CommunicationSpeechConcern_Stuttering_Element => By.CssSelector("#CommunicationSpeechPPIMultiIDOptions-checkbox-item-6");
        public By CommunicationSpeechConcern_Voice_Element => By.CssSelector("#CommunicationSpeechPPIMultiIDOptions-checkbox-item-7");

        public By CommunicationSpeechConcern_ReceptiveLanguage_Element => By.CssSelector("#CommunicationSpeechPPIMultiIDOptions-checkbox-item-4");
        //Assign to Case Leader
        public By StudentCaseDetailsForm_Element => By.XPath("//span[contains(text(),'Case Details')]");
        public By MedicalHealthSection_Element => By.XPath("//div[@id='MedicalHealthPPI']");

        public By MentalHealthSection_Element => By.XPath("//div[@id='MentalHealthPPI']");

        public By MentalHealthConcern_StressCheckbox_Element => By.CssSelector("#MentalHealthPPIMultiIDOptions-checkbox-item-6");
        public By WholeSchoolIssuesSection_Element => By.CssSelector("#WholeSchoolPPI");
        public By WholeSchoolIssuesCheckbox_Element => By.CssSelector("#WholeSchoolPPIMultiIDOptions-checkbox-item-0");
        public By SupportSection_Element => By.CssSelector("#SupportFollowingPPI");

        public By Support_FireCheckbox_Element => By.CssSelector("#SupportFollowingPPIMultiIDOptions-checkbox-item-0");
        public By EducationalNeedsAssessmentSection_Element => By.CssSelector("#ENAPPI");
        public By EducationalNeedsAssessment_Checkbox_Element => By.CssSelector("#ENAPPIMultiIDOptions-checkbox-item-0");


        internal void CloseCase_Program()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            //click Close Case
            wait.Until(x => x.FindElement(CloseCasebutton_Element));
            Driver.FindElement(CloseCasebutton_Element).Click();
            Thread.Sleep(1000);

            //random generator/input details for Reason for closure and details of alternative strategy where appropriate
            wait.Until(x => x.FindElement(CloseCaseCommentInput_Element));

            Random generatorReason = new Random();
            int iReason = generatorReason.Next(1, 10000000);
            string sReason = iReason.ToString().PadLeft(7, '0');
            string eReason = "Test Close Reason_" + DateTime.Now.ToString() + "_" + sReason;
            Driver.FindElement(CloseCaseCommentInput_Element).SendKeys(eReason);

            //click Close Case confirmation
            wait.Until(x => x.FindElement(CloseCaseCommitButton_Element));
            Driver.FindElement(CloseCaseCommitButton_Element).Click();
            Thread.Sleep(1000);

            //verify the message
            wait.Until(x => x.FindElement(ConfirmationMessage_Element));
            Assert.AreEqual("Case closed\r\nCase was closed successfully.", Driver.FindElement(ConfirmationMessage_Element).Text, "Case not closed");
            Thread.Sleep(2000);

            //click CaseDetailsSection
            wait.Until(x => x.FindElement(CaseDetailsSection_Element));
            Driver.FindElement(CaseDetailsSection_Element).Click();
            Thread.Sleep(1000);

            //verify the status
            //wait.Until(x => x.FindElement(CaseStatus_Element));
            //Assert.AreEqual("Completed", Driver.FindElement(CaseStatus_Element).Text, "Case not closed-incorrect status");
            //Thread.Sleep(2000);
        }

        internal void ReopenCase_Program()
        {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
                wait.Until(x => x.FindElement(ProgramRequestForm_Element));

                //click Reopen Case button
                wait.Until(x => x.FindElement(ReopenCasebutton_Element));
                Driver.FindElement(ReopenCasebutton_Element).Click();
                Thread.Sleep(1000);

                //verify the message
                wait.Until(x => x.FindElement(ConfirmationMessage_Element));
                Assert.AreEqual("Case re-opened\r\nCase was successfully re-opened.", Driver.FindElement(ConfirmationMessage_Element).Text, "Case not reopened");
                Thread.Sleep(2000);

                //verify the status
                wait.Until(x => x.FindElement(CaseStatus_Element));
                Assert.AreEqual("Submitted", Driver.FindElement(CaseStatus_Element).Text, "Case not reopened-incorrect status");
                Thread.Sleep(2000);

           }

        internal void FillInProgramRequest_MentalHealth(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Primary Presenting Issues
                wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                primaryPresentingIssues.SelectByText(row.ItemArray[0].ToString());

                //click Mental Health as primary issue
                wait.Until(x => x.FindElement(MentalHealthSection_Element));
                Driver.FindElement(MentalHealthSection_Element).Click();
                Thread.Sleep(1000);

                //Select one option as mental health Concern
                wait.Until(x => x.FindElement(MentalHealthConcern_StressCheckbox_Element));
                Driver.FindElement(MentalHealthConcern_StressCheckbox_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));
                Thread.Sleep(3000);
                //This will scroll the page till the element is found	
                IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
                js.ExecuteScript("arguments[0].scrollIntoView();", Driver.FindElement(CaseCommentInput_Element));
                Thread.Sleep(3000);
                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);
                Thread.Sleep(3000);
                //This will scroll the page till the element is found	
                //js.ExecuteScript("arguments[0].scrollIntoView();", Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Thread.Sleep(3000);
                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();
                Thread.Sleep(3000);
                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();
                Thread.Sleep(3000);
                //click Save button
                wait.Until(x => x.FindElement(SaveDraftButton_Element));
                Driver.FindElement(SaveDraftButton_Element).Click();
            }
        }

        internal void FillInProgramRequest_EducationalNeedsAssessment(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Primary Presenting Issues
                wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                primaryPresentingIssues.SelectByText(row.ItemArray[0].ToString());

                //click Support section
                wait.Until(x => x.FindElement(EducationalNeedsAssessmentSection_Element));
                Driver.FindElement(EducationalNeedsAssessmentSection_Element).Click();
                Thread.Sleep(1000);

                //Select one option as primary concern
                wait.Until(x => x.FindElement(EducationalNeedsAssessment_Checkbox_Element));
                Driver.FindElement(EducationalNeedsAssessment_Checkbox_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));
                Thread.Sleep(3000);
                //This will scroll the page till the element is found	
                IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
                js.ExecuteScript("arguments[0].scrollIntoView();", Driver.FindElement(CaseCommentInput_Element));
                Thread.Sleep(3000);
                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);
                //Thread.Sleep(3000);
                //This will scroll the page till the element is found	
                //js.ExecuteScript("arguments[0].scrollIntoView();", Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                //Thread.Sleep(3000);
                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();
                //Thread.Sleep(3000);
                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();
                Thread.Sleep(3000);
                //click Save button
                wait.Until(x => x.FindElement(SaveDraftButton_Element));
                Driver.FindElement(SaveDraftButton_Element).Click();
            }
        }

        internal void FillInProgramRequest_Support(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Primary Presenting Issues
                wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                primaryPresentingIssues.SelectByText(row.ItemArray[0].ToString());

                //click Support section
                wait.Until(x => x.FindElement(SupportSection_Element));
                Driver.FindElement(SupportSection_Element).Click();
                Thread.Sleep(1000);

                //Select one option as primary concern
                wait.Until(x => x.FindElement(Support_FireCheckbox_Element));
                Driver.FindElement(Support_FireCheckbox_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));
                Thread.Sleep(3000);
                //This will scroll the page till the element is found	
                IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
                js.ExecuteScript("arguments[0].scrollIntoView();", Driver.FindElement(CaseCommentInput_Element));
                Thread.Sleep(3000);
                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);
                Thread.Sleep(3000);
                //This will scroll the page till the element is found	
                //js.ExecuteScript("arguments[0].scrollIntoView();", Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Thread.Sleep(3000);
                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();
                Thread.Sleep(3000);
                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();
                Thread.Sleep(3000);
                //click Save button
                wait.Until(x => x.FindElement(SaveDraftButton_Element));
                Driver.FindElement(SaveDraftButton_Element).Click();
            }
        }

        internal void ReturnCase_Program()
        {

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            //click Return Case button
            wait.Until(x => x.FindElement(ReturnCasebutton_Element));
            Driver.FindElement(ReturnCasebutton_Element).Click();
            Thread.Sleep(1000);

            //random generator/input details for Return reason
            wait.Until(x => x.FindElement(ReturnCaseCommentInput_Element));

            Random generatorReason = new Random();
            int iReason = generatorReason.Next(1, 10000000);
            string sReason = iReason.ToString().PadLeft(7, '0');
            string eReason = "Test Return Reason_" + DateTime.Now.ToString() + "_" + sReason;
            Driver.FindElement(ReturnCaseCommentInput_Element).SendKeys(eReason);

            //click Return Case confirmation
            wait.Until(x => x.FindElement(ReturnCaseCommitButton_Element));
            Driver.FindElement(ReturnCaseCommitButton_Element).Click();
            Thread.Sleep(1000);

            //verify the message
            wait.Until(x => x.FindElement(ConfirmationMessage_Element));
            Assert.AreEqual("Case returned\r\nCase was successfully returned.", Driver.FindElement(ConfirmationMessage_Element).Text, "Case not returned");
            Thread.Sleep(2000);

            //verify the status
            wait.Until(x => x.FindElement(CaseStatus_Element));
            Assert.AreEqual("Returned", Driver.FindElement(CaseStatus_Element).Text, "Case not returned-incorrect status");
            Thread.Sleep(2000);


        }

        internal void SelectService_Program(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));

            //click Services Section
            wait.Until(x => x.FindElement(ServicesSection_Element));
            Driver.FindElement(ServicesSection_Element).Click();
            Thread.Sleep(5000);
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                Driver.FindElement(By.XPath("//a[contains(text(),'" + row.ItemArray[0].ToString() + "')]")).Click();
            }
        }

        internal void ClickCreateProgramRequest()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(CreateProgramRequest_Element));

            Driver.FindElement(CreateProgramRequest_Element).Click();
        }
        internal void FillInProgramRequest_CurriculumLearning(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Primary Presenting Issues
                wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                primaryPresentingIssues.SelectByText(row.ItemArray[0].ToString());

                //click Curriculum/Learning Section
                wait.Until(x => x.FindElement(CurriculumLearningSection_Element));
                Driver.FindElement(CurriculumLearningSection_Element).Click();
                Thread.Sleep(1000);

                //Select "Aggression/anger [fights with others, swears]" as Behaviour Concerns
                wait.Until(x => x.FindElement(CurriculumLearningConcern_DifficultystayingontaskCheckbox_Element));
                Driver.FindElement(CurriculumLearningConcern_DifficultystayingontaskCheckbox_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //click Save button
                wait.Until(x => x.FindElement(SaveDraftButton_Element));
                Driver.FindElement(SaveDraftButton_Element).Click();

            }
        }

        internal void FillInProgramRequest_Communication(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Primary Presenting Issues
                wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                primaryPresentingIssues.SelectByText(row.ItemArray[0].ToString());

                //click Behaviour as primary issue and few others as secondary issue
                wait.Until(x => x.FindElement(CommunicationSpeechSection_Element));
                Driver.FindElement(CommunicationSpeechSection_Element).Click();
                Thread.Sleep(1000);

                //Select all options as Communication or Speech Concerns
                wait.Until(x => x.FindElement(CommunicationSpeechConcern_ArticulationCheckbox_Element));
                Driver.FindElement(CommunicationSpeechConcern_ArticulationCheckbox_Element).Click();

                wait.Until(x => x.FindElement(CommunicationSpeechConcern_ExpressiveLanguage_Element));
                Driver.FindElement(CommunicationSpeechConcern_ExpressiveLanguage_Element).Click();

                wait.Until(x => x.FindElement(CommunicationSpeechConcern_Feeding_Element));
                Driver.FindElement(CommunicationSpeechConcern_Feeding_Element).Click();

                wait.Until(x => x.FindElement(CommunicationSpeechConcern_PragmaticLanguage_Element));
                Driver.FindElement(CommunicationSpeechConcern_PragmaticLanguage_Element).Click();

                wait.Until(x => x.FindElement(CommunicationSpeechConcern_ReceptiveLanguage_Element));
                Driver.FindElement(CommunicationSpeechConcern_ReceptiveLanguage_Element).Click();

                wait.Until(x => x.FindElement(CommunicationSpeechConcern_SocialLanguage_Element));
                Driver.FindElement(CommunicationSpeechConcern_SocialLanguage_Element).Click();

                wait.Until(x => x.FindElement(CommunicationSpeechConcern_Stuttering_Element));
                Driver.FindElement(CommunicationSpeechConcern_Stuttering_Element).Click();

                wait.Until(x => x.FindElement(CommunicationSpeechConcern_Voice_Element));
                Driver.FindElement(CommunicationSpeechConcern_Voice_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //click Save button
                wait.Until(x => x.FindElement(SaveDraftButton_Element));
                Driver.FindElement(SaveDraftButton_Element).Click();
            }
        }

        internal void CloseCase_Action(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Area Team case dropdown
                wait.Until(x => x.FindElement(PriorityDropdown_Element));
                SelectElement priority = new SelectElement(Driver.FindElement(PriorityDropdown_Element));
                priority.SelectByText(row.ItemArray[0].ToString());

                //click Action Section
                wait.Until(x => x.FindElement(ActionSection_Element));
                Driver.FindElement(ActionSection_Element).Click();
                Thread.Sleep(1000);

                //select from Action dropdown
                wait.Until(x => x.FindElement(ActionDropdown_Element));
                SelectElement action = new SelectElement(Driver.FindElement(ActionDropdown_Element));
                action.SelectByText(row.ItemArray[1].ToString());

                //click Save School Case button
                wait.Until(x => x.FindElement(UpdateSchoolCaseButton_Element));
                Driver.FindElement(UpdateSchoolCaseButton_Element).Click();

                //verify the message
                wait.Until(x => x.FindElement(ConfirmationMessage_Element));
                Assert.AreEqual("Case closed\r\nCase was closed successfully.", Driver.FindElement(ConfirmationMessage_Element).Text, "Student Case not closed");
                Thread.Sleep(2000);

                //click CaseDetailsSection
                wait.Until(x => x.FindElement(CaseDetailsSection_Element));
                Driver.FindElement(CaseDetailsSection_Element).Click();
                Thread.Sleep(1000);

                //verify the status
                wait.Until(x => x.FindElement(CaseStatus_Element));
                Assert.AreEqual("Completed", Driver.FindElement(CaseStatus_Element).Text, "Case not closed-incorrect status");
                Thread.Sleep(2000);
            }
        }

        internal void FillInProgramRequest_WholeSchoolIssue(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Primary Presenting Issues
                wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                primaryPresentingIssues.SelectByText(row.ItemArray[0].ToString());

                //click whole school issues section
                wait.Until(x => x.FindElement(WholeSchoolIssuesSection_Element));
                Driver.FindElement(WholeSchoolIssuesSection_Element).Click();
                Thread.Sleep(1000);

                //Select chekbox
                wait.Until(x => x.FindElement(WholeSchoolIssuesCheckbox_Element));
                Driver.FindElement(WholeSchoolIssuesCheckbox_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //click Save button
                wait.Until(x => x.FindElement(SaveDraftButton_Element));
                Driver.FindElement(SaveDraftButton_Element).Click();
            }
        }

        internal void FillInProgramRequest_MedicalHealth(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Primary Presenting Issues
                wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                primaryPresentingIssues.SelectByText(row.ItemArray[0].ToString());

                //click medical/health as primary issue and few others as secondary issue
                wait.Until(x => x.FindElement(MedicalHealthSection_Element));
                Driver.FindElement(MedicalHealthSection_Element).Click();
                Thread.Sleep(1000);

                for (int i = 0; i < 11; i++)
                {
                    //Select all social/emotional concerns
                    wait.Until(x => x.FindElement(By.CssSelector("#MedicalHealthPPIMultiIDOptions-checkbox-item-" + i + "")));
                    Driver.FindElement(By.CssSelector("#MedicalHealthPPIMultiIDOptions-checkbox-item-" + i + "")).Click();
                }

                //click Attendance Section as secondary concerns
                wait.Until(x => x.FindElement(AttendanceSection_Element));
                Driver.FindElement(AttendanceSection_Element).Click();
                Thread.Sleep(1000);

                wait.Until(x => x.FindElement(AttendanceConcern_Illness_Element));
                Driver.FindElement(AttendanceConcern_Illness_Element).Click();

                wait.Until(x => x.FindElement(AttendanceConcern_Trunacy_Element));
                Driver.FindElement(AttendanceConcern_Trunacy_Element).Click();

                //click Social/emotional as secondary issues
                wait.Until(x => x.FindElement(SocialEmotionalSection_Element));
                Driver.FindElement(SocialEmotionalSection_Element).Click();
                Thread.Sleep(1000);

                for (int i = 0; i < 23; i++)
                {
                    //Select all social/emotional concerns
                    wait.Until(x => x.FindElement(By.CssSelector("#SocialEmotionalPPIMultiIDOptions-checkbox-item-" + i + "")));
                    Driver.FindElement(By.CssSelector("#SocialEmotionalPPIMultiIDOptions-checkbox-item-" + i + "")).Click();
                }

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //click Save button
                wait.Until(x => x.FindElement(SaveDraftButton_Element));
                Driver.FindElement(SaveDraftButton_Element).Click();
            }
        }

        internal void FillInProgramRequest_SocialEmotional(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Primary Presenting Issues
                wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                primaryPresentingIssues.SelectByText(row.ItemArray[0].ToString());

                //click Social/emotional as primary issue and few others as secondary issue
                wait.Until(x => x.FindElement(SocialEmotionalSection_Element));
                Driver.FindElement(SocialEmotionalSection_Element).Click();
                Thread.Sleep(1000);

                for (int i = 0; i < 23; i++)
                {
                    //Select all social/emotional concerns
                    wait.Until(x => x.FindElement(By.CssSelector("#SocialEmotionalPPIMultiIDOptions-checkbox-item-" + i + "")));
                    Driver.FindElement(By.CssSelector("#SocialEmotionalPPIMultiIDOptions-checkbox-item-" + i + "")).Click();
                }

                //click Attendance Section as secondary concerns
                wait.Until(x => x.FindElement(AttendanceSection_Element));
                Driver.FindElement(AttendanceSection_Element).Click();
                Thread.Sleep(1000);

                wait.Until(x => x.FindElement(AttendanceConcern_Illness_Element));
                Driver.FindElement(AttendanceConcern_Illness_Element).Click();

                wait.Until(x => x.FindElement(AttendanceConcern_Trunacy_Element));
                Driver.FindElement(AttendanceConcern_Trunacy_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //click Save button
                wait.Until(x => x.FindElement(SaveDraftButton_Element));
                Driver.FindElement(SaveDraftButton_Element).Click();
            }
        }

        internal void FillInProgramRequest_Behaviour(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Primary Presenting Issues
                wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                primaryPresentingIssues.SelectByText(row.ItemArray[0].ToString());

                //click Behaviour as primary issue and few others as secondary issue
                wait.Until(x => x.FindElement(BehaviourSection_Element));
                Driver.FindElement(BehaviourSection_Element).Click();
                Thread.Sleep(1000);

                //Select "Aggression/anger [fights with others, swears]" as Behaviour Concerns
                wait.Until(x => x.FindElement(Behaviour_Anger_Element));
                Driver.FindElement(Behaviour_Anger_Element).Click();

                //Select secondary concerns
                //click Attendance Section
                wait.Until(x => x.FindElement(AttendanceSection_Element));
                Driver.FindElement(AttendanceSection_Element).Click();
                Thread.Sleep(1000);

                wait.Until(x => x.FindElement(AttendanceConcern_Illness_Element));
                Driver.FindElement(AttendanceConcern_Illness_Element).Click();

                wait.Until(x => x.FindElement(AttendanceConcern_Trunacy_Element));
                Driver.FindElement(AttendanceConcern_Trunacy_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //click Save button
                wait.Until(x => x.FindElement(SaveDraftButton_Element));
                Driver.FindElement(SaveDraftButton_Element).Click();
            }
        }

        internal void NoStudentfortheProgram()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(NewCaseForm_Element));

            //click GuardianConsentCheckbox
            wait.Until(x => x.FindElement(GuardianConsentCheckbox_Element));
            Driver.FindElement(GuardianConsentCheckbox_Element).Click();

            //click Next button
            wait.Until(x => x.FindElement(NextButton_Element));
            Driver.FindElement(NextButton_Element).Click();

            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            wait.Until(x => x.FindElement(Casenumber_Element));
            Assert.IsNotNull((Driver.FindElement(Casenumber_Element), "No Case number"));

            wait.Until(x => x.FindElement(CaseStatus_Element));
            Assert.AreEqual("Draft", Driver.FindElement(CaseStatus_Element).Text, "Status is incorrect");
        }

        internal void SelectStudentsfortheProgram()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(NewCaseForm_Element));

            //Select first 10 students from the list
            //click checkbox
            wait.Until(x => x.FindElement(SelectStudentcheckbox_Element));
            int i = 0;
            for (i = 1; i <= 10; i++)
            {
                Thread.Sleep(2000);
                Driver.FindElement(By.XPath("//div[@id='StudentList']//tbody//tr[" + i + "]//td[1]")).Click();
            }

            //click GuardianConsentCheckbox
            wait.Until(x => x.FindElement(GuardianConsentCheckbox_Element));
            Driver.FindElement(GuardianConsentCheckbox_Element).Click();

            //click Next button
            wait.Until(x => x.FindElement(NextButton_Element));
            Driver.FindElement(NextButton_Element).Click();

            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            wait.Until(x => x.FindElement(Casenumber_Element));
            Assert.IsNotNull((Driver.FindElement(Casenumber_Element), "No Case number"));

            wait.Until(x => x.FindElement(CaseStatus_Element));
            Assert.AreEqual("Draft", Driver.FindElement(CaseStatus_Element).Text, "Status is incorrect");

        }

        internal void FillInProgramRequest_Attendance(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Primary Presenting Issues
                wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                primaryPresentingIssues.SelectByText(row.ItemArray[0].ToString());

                //click Attendance Section
                wait.Until(x => x.FindElement(AttendanceSection_Element));
                Driver.FindElement(AttendanceSection_Element).Click();
                Thread.Sleep(1000);

                //Select multiple concerns
                //Select "Chronic Illness" and "Trunacy / chronic absenteeism" as Attendance Concerns
                wait.Until(x => x.FindElement(AttendanceConcern_Illness_Element));
                Driver.FindElement(AttendanceConcern_Illness_Element).Click();

                wait.Until(x => x.FindElement(AttendanceConcern_Trunacy_Element));
                Driver.FindElement(AttendanceConcern_Trunacy_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select "SSSO with attendance or behaviour expertise" as Specialist requested
                wait.Until(x => x.FindElement(Specialist_SSSO_Checkbox_Element));
                Driver.FindElement(Specialist_SSSO_Checkbox_Element).Click();

                //click Save button
                wait.Until(x => x.FindElement(SaveDraftButton_Element));
                Driver.FindElement(SaveDraftButton_Element).Click();

            }
        }
        internal void SelectStudentfortheProgram(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(NewCaseForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //input FirstName
                wait.Until(x => x.FindElement(StudentFNInput_Element));
                Driver.FindElement(StudentFNInput_Element).SendKeys(row.ItemArray[0].ToString());

                //click Apply Filter button
                wait.Until(x => x.FindElement(ApplyFilterButton_Element));
                Driver.FindElement(ApplyFilterButton_Element).Click();

                //click checkbox
                wait.Until(x => x.FindElement(SelectStudentcheckbox_Element));
                Driver.FindElement(SelectStudentcheckbox_Element).Click();
            }
            //click GuardianConsentCheckbox
            wait.Until(x => x.FindElement(GuardianConsentCheckbox_Element));
            Driver.FindElement(GuardianConsentCheckbox_Element).Click();

            //click Next button
            wait.Until(x => x.FindElement(NextButton_Element));
            Driver.FindElement(NextButton_Element).Click();

            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            wait.Until(x => x.FindElement(Casenumber_Element));
            Assert.IsNotNull((Driver.FindElement(Casenumber_Element), "No Case number"));

            wait.Until(x => x.FindElement(CaseStatus_Element));
            Assert.AreEqual("Draft", Driver.FindElement(CaseStatus_Element).Text, "Status is incorrect");
        }
        public string GetProgramCaseNumber()
        {
            //WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            string strProgramCaseNumber = (Driver.FindElement(Casenumber_Element).Text);
            return strProgramCaseNumber;
        }
        internal void UploadSupportingDocuments()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));

            //click Choose file button
            var winHandleBefore = Driver.CurrentWindowHandle;
            wait.Until(x => x.FindElement(ChooseFileButton_Element));
            Driver.FindElement(ChooseFileButton_Element).Click();

            //Switch to upload window
            Driver.SwitchTo().Window(Driver.WindowHandles.Last());
            //Uploading the file using keyboard actions
            Thread.Sleep(2000);

            var fileNamePath = CommonFunctions.GetResourceValue("FileUpload_Document");
            SendKeys.SendWait(fileNamePath);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(5000);
            //Driver.FindElement(UploadButton_Element).Click();

            //Brings the control back to original window
            Driver.SwitchTo().Window(winHandleBefore);
        }
        internal void AddNoteandSave(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            //click Add Case Note button
            wait.Until(x => x.FindElement(AddCaseNoteButton_Element));
            Driver.FindElement(AddCaseNoteButton_Element).Click();
            Thread.Sleep(1000);

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Duration Hours dropdown
                wait.Until(x => x.FindElement(SCN_DurationHrsDropdown_Element));
                SelectElement durationHours = new SelectElement(Driver.FindElement(SCN_DurationHrsDropdown_Element));
                durationHours.SelectByText(row.ItemArray[0].ToString());

                //select from Duration Minutes dropdown
                wait.Until(x => x.FindElement(SCN_DurationMinDropdown_Element));
                SelectElement durationMins = new SelectElement(Driver.FindElement(SCN_DurationMinDropdown_Element));
                durationMins.SelectByText(row.ItemArray[1].ToString());

                //random generator/input details for Student Case Note Subject
                wait.Until(x => x.FindElement(SCN_SubjectInput_Element));

                Random generatorSubject = new Random();
                int iSubject = generatorSubject.Next(1, 10000000);
                string sSubject = iSubject.ToString().PadLeft(7, '0');
                string eSubject = "Test Subject_" + DateTime.Now.ToString() + "_" + sSubject;
                Driver.FindElement(SCN_SubjectInput_Element).SendKeys(eSubject);

                //select from School Action dropdown
                wait.Until(x => x.FindElement(SCN_SchoolActionDropdown_Element));
                SelectElement schoolAction = new SelectElement(Driver.FindElement(SCN_SchoolActionDropdown_Element));
                schoolAction.SelectByText(row.ItemArray[2].ToString());

                //random generator/input details for Student Case Note Notes
                wait.Until(x => x.FindElement(SCN_NotesInput_Element));

                Random generatorNotes = new Random();
                int iNotes = generatorNotes.Next(1, 10000000);
                string sNotes = iNotes.ToString().PadLeft(7, '0');
                string eNotes = "Test Notes_" + DateTime.Now.ToString() + "_" + sNotes;
                Driver.FindElement(SCN_NotesInput_Element).SendKeys(eNotes);

                //click Save Case Note button
                wait.Until(x => x.FindElement(SCN_SaveCaseNoteButton_Element));
                Driver.FindElement(SCN_SaveCaseNoteButton_Element).Click();

                //This will scroll the page till the element is found	
                IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
                js.ExecuteScript("arguments[0].scrollIntoView();", Driver.FindElement(By.XPath("//h2[contains(text(),'Program Request')]")));

                //verify the message
                wait.Until(x => x.FindElement(ConfirmationMessage_Element));
                Assert.AreEqual("Student Case Note Created\r\nStudent case note was successfully created. View this and historical notes in the Student Case Note panel", Driver.FindElement(ConfirmationMessage_Element).Text, "Student Case Note not created");

                //click Student Case Note section
                //wait.Until(x => x.FindElement(SCN_StudentCaseNoteSection_Element));
                //Driver.FindElement(SCN_StudentCaseNoteSection_Element).Click();

                //wait.Until(x => x.FindElement(SCN_StudentCaseNoteSectionForm_Element));
                //Thread.Sleep(10000);

                /*
                //verify added student case note
                var TableRows = Driver.FindElements(By.XPath("//div[@id='SchoolCaseNotesGrid']//table//tbody//tr"));
                Thread.Sleep(2000);
                int matchedCaseNote = 0;
                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));

                    //look for the subject

                    if (td[2].Text.Equals(eSubject.ToString()))
                    {
                        matchedCaseNote = matchedCaseNote + 1;
                        break;
                    }

                }
                Thread.Sleep(5000);
                //verify the case note created
                Assert.AreEqual(1, matchedCaseNote, "No Student Case Note created");
                Thread.Sleep(2000);*/
            }

        }
        internal void Action_ProgramRequest(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Area Team case dropdown
                wait.Until(x => x.FindElement(PriorityDropdown_Element));
                SelectElement priority = new SelectElement(Driver.FindElement(PriorityDropdown_Element));
                priority.SelectByText(row.ItemArray[0].ToString());

                //click Action Section
                wait.Until(x => x.FindElement(ActionSection_Element));
                Driver.FindElement(ActionSection_Element).Click();
                Thread.Sleep(1000);

                //select from Action dropdown
                wait.Until(x => x.FindElement(ActionDropdown_Element));
                SelectElement action = new SelectElement(Driver.FindElement(ActionDropdown_Element));
                action.SelectByText(row.ItemArray[1].ToString());

                //select from Recommendations dropdown
                wait.Until(x => x.FindElement(RecommendationCategoryDropdown_Element));
                SelectElement recommendation = new SelectElement(Driver.FindElement(RecommendationCategoryDropdown_Element));
                recommendation.SelectByText(row.ItemArray[2].ToString());

                //click Guardian consent received and accepted? checkbox
                //wait.Until(x => x.FindElement(GuardianObtainedCheckbox_Element));
                //Driver.FindElement(GuardianObtainedCheckbox_Element).Click();

                //click Update School Case button
                wait.Until(x => x.FindElement(UpdateSchoolCaseButton_Element));
                Driver.FindElement(UpdateSchoolCaseButton_Element).Click();
            }
        }
        internal void AttachConsentForm()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            //click Consent Form
            wait.Until(x => x.FindElement(AddConsentFormButton_Element));
            Driver.FindElement(AddConsentFormButton_Element).Click();

            wait.Until(x => x.FindElement(AddConsentForm_Element));

            //click Choose file button
            var winHandleBefore = Driver.CurrentWindowHandle;
            wait.Until(x => x.FindElement(ConsentChooseFileButton_Element));
            Driver.FindElement(ConsentChooseFileButton_Element).Click();

            //Switch to upload window
            Driver.SwitchTo().Window(Driver.WindowHandles.Last());
            //Uploading the file using keyboard actions
            Thread.Sleep(2000);

            var fileNamePath = CommonFunctions.GetResourceValue("FileUpload_ConsentForm");
            SendKeys.SendWait(fileNamePath);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(5000);
            Driver.FindElement(UploadFormButton_Element).Click();

            //verify the message
            wait.Until(x => x.FindElement(UploadFormMessage_Element));
            Assert.AreEqual("Consent Form Created Succesfully", Driver.FindElement(UploadFormMessage_Element).Text, "File not uploaded");

            wait.Until(x => x.FindElement(CurrentConsentForm_Element));
            Assert.AreEqual("Current Consent Form", Driver.FindElement(CurrentConsentForm_Element).Text, "No file uploaded");

            wait.Until(x => x.FindElement(ReturnToCaseButton_Element));
            Driver.FindElement(ReturnToCaseButton_Element).Click();

            //Brings the control back to original window
            Driver.SwitchTo().Window(winHandleBefore);
        }
        internal void SaveCase()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            //click Save Case button
            wait.Until(x => x.FindElement(SaveCaseButton_Element));
            Driver.FindElement(SaveCaseButton_Element).Click();

            //verify the message
            wait.Until(x => x.FindElement(ConfirmationMessage_Element));
            Thread.Sleep(2000);
            Assert.AreEqual("Case updated\r\nCase was updated successfully.", Driver.FindElement(ConfirmationMessage_Element).Text, "Case not updated");
            Thread.Sleep(2000);

        }
        internal void SubmitCase()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            /*wait.Until(x => x.FindElement(GuardianCheckbox_Element));
            if (Driver.FindElement(GuardianCheckbox_Element).GetAttribute("checked").Equals("checked"))
            { }
            else
            {
                //click Guardian checkbox if not yet checked
                Driver.FindElement(GuardianCheckbox_Element).Click();
            }*/
            //This will scroll the page till the element is found	
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            js.ExecuteScript("arguments[0].scrollIntoView();", Driver.FindElement(SaveandSubmitCaseButton_Element));

            //click submit
            wait.Until(x => x.FindElement(SaveandSubmitCaseButton_Element));
            Driver.FindElement(SaveandSubmitCaseButton_Element).Click();

            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            //verify the message
            wait.Until(x => x.FindElement(ConfirmationMessage_Element));
            Assert.AreEqual("Case submitted\r\nCase sent to Student Support Services.", Driver.FindElement(ConfirmationMessage_Element).Text, "Case not submitted");

            //verify the status
            wait.Until(x => x.FindElement(CaseStatus_Element));
            Assert.AreEqual("Submitted", Driver.FindElement(CaseStatus_Element).Text, "Case not submitted-incorrect status");
            Thread.Sleep(2000);

        }
        internal void AmendSpecialist()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            wait.Until(x => x.FindElement(ProgramRequestForm_Element));

            //click Case Details Section
            //wait.Until(x => x.FindElement(CaseDetailsSection_Element));
            //Driver.FindElement(CaseDetailsSection_Element).Click();

            //click Amend Specialist Requested button
            wait.Until(x => x.FindElement(AmendSpecialistRequestedButton_Element));
            Driver.FindElement(AmendSpecialistRequestedButton_Element).Click();

            //click Speech Pathologist checkbox
            wait.Until(x => x.FindElement(SpeechPathologistCheckbox_Element));
            Driver.FindElement(SpeechPathologistCheckbox_Element).Click();
            //test
        }
    }

}