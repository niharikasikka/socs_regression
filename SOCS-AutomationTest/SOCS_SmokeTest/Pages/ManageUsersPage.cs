﻿using SOCS_SmokeTest.BaseClass;
using SOCS_SmokeTest.UtilityClasses;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading.Tasks;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;
using NUnit.Framework;
using System.Linq;
using System.Windows.Forms;


namespace SOCS_SmokeTest.Pages
{
    class ManageUsersPage : BaseApplicationPage
    {
        public ManageUsersPage(IWebDriver driver) : base(driver)
        { }
        //Manage Users Elements
        public By FirstNameInput_Element => By.CssSelector("#FilterFirstName");
        public By ApplyFilterButton_Element => By.CssSelector("#cmdSubmit");
        public By SelectButton_Element => By.XPath("//a[contains(text(),'Select')]");
        public By SpecialityDropdown_Element => By.CssSelector("#SpecialityId");
        public By ManageUsers_Element => By.XPath("//span[contains(text(),'Manage Users')]");
        public By UpdateContactButton_Element => By.CssSelector("#cmdSave");
        public By AddUserMenuButton_Element => By.XPath("//a[@class='t-grid-action t-button t-state-default']");
        public By SearchButton_Element => By.CssSelector("#cmdSubmit");
        public By DeactivateUserAccessButton_Element => By.CssSelector("#cmdUpdateStateCode");
        public By EnterFirstNameInput_Element => By.XPath("//input[@id='FilterGivenName']");
        public By HitSelectButton_Element => By.XPath("//a[contains(text(),'Select')]");
        public By ActivateUserAccessButton_Element => By.CssSelector("#cmdUpdateStateCode");

        internal void ClickManageUsers()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(ManageUsers_Element));
            Driver.FindElement(ManageUsers_Element).Click();
        }
        internal void ClickFirstName(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(FirstNameInput_Element));
            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //input FirstName
                wait.Until(x => x.FindElement(FirstNameInput_Element));
                Driver.FindElement(FirstNameInput_Element).SendKeys(row.ItemArray[0].ToString());
                
            }
        }
        internal void ClickApplyFilterButton()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(ApplyFilterButton_Element));
            Driver.FindElement(ApplyFilterButton_Element).Click();
        }
        internal void ClickSelectButton()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(SelectButton_Element));
            Driver.FindElement(SelectButton_Element).Click();
        }
        
        internal void ClickSpeciality(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(SpecialityDropdown_Element));
            Driver.FindElement(SpecialityDropdown_Element).Click();
            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //input FirstName
                wait.Until(x => x.FindElement(SpecialityDropdown_Element));
                Driver.FindElement(SpecialityDropdown_Element).SendKeys(row.ItemArray[0].ToString());

            }
        }
        internal void ClickUpdateContact()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(UpdateContactButton_Element));
            Driver.FindElement(UpdateContactButton_Element).Click(); 
        }
        internal void ClickAddUserMenu()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(AddUserMenuButton_Element));
            Driver.FindElement(AddUserMenuButton_Element).Click(); 
        }
        internal void ClickSearchButton()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(SearchButton_Element));
            Driver.FindElement(SearchButton_Element).Click(); 
        }
        internal void ClickDeactivateUserAccessButton()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(DeactivateUserAccessButton_Element));
            Driver.FindElement(DeactivateUserAccessButton_Element).Click(); 
        }
        internal void EnterFirstName(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(EnterFirstNameInput_Element));
            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //input FirstName
                wait.Until(x => x.FindElement(EnterFirstNameInput_Element));
                Driver.FindElement(EnterFirstNameInput_Element).SendKeys(row.ItemArray[0].ToString());

            }
            
        }
        internal void HitSelectButton()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(HitSelectButton_Element));
            Driver.FindElement(HitSelectButton_Element).Click();
        }
        internal void ClickActivateUserAccessButton()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(ActivateUserAccessButton_Element));
            Driver.FindElement(ActivateUserAccessButton_Element).Click();
        }
    }
}
