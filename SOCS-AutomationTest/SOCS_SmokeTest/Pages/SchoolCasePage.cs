﻿using SOCS_SmokeTest.BaseClass;
using SOCS_SmokeTest.UtilityClasses;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading.Tasks;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;
using NUnit.Framework;
using System.Linq;
using System.Windows.Forms;

namespace SOCS_SmokeTest.Pages
{
    class SchoolCasePage : BaseApplicationPage
    {
        public SchoolCasePage(IWebDriver driver) : base(driver)
        { }
        //School Case Elements
        public By SchoolCase_Element => By.XPath("//span[contains(text(),'Create School Case')]");
        public By SchoolCaseForm_Element => By.XPath("//h2[contains(text(),'School Case')]");
        public By SchoolNameNumInput_Element => By.CssSelector("#FilterSchoolNameOrNumber");
        public By StudentFNInput_Element => By.CssSelector("#FilterFirstName");
        public By ApplyFilterButton_Element => By.CssSelector("#cmdSubmitSearch");
        public By StudentSelectLink_Element => By.XPath("//a[contains(text(),'Select')]");
        public By NextButton_Element => By.CssSelector("#cmdWizardNext");
        public By StudentCaseDetailsForm_Element => By.XPath("//span[contains(text(),'Case Details')]");
        public By RequestTypeDropdown_Element => By.CssSelector("#RequestType");
        public By PrimaryPresentingIssueDropdown_Element => By.CssSelector("#PrimaryPresentingIssue");

        //Case Details
        public By SchoolCaseHeading_Element => By.XPath("//span[@class='heading']");
        public By Casenumber_Element => By.CssSelector("#TicketNumber");
        public By ConfirmationMessage_Element => By.CssSelector("#confirmationMessage");
        public By CaseStatus_Element => By.CssSelector("#CaseStatus");
        public By PriorityDropdown_Element => By.CssSelector("#PriorityCode");
        public By HaveCompletedCheckbox_Element => By.CssSelector("#IHaveCompletedActivities");
        public By CaseCommentInput_Element => By.CssSelector("#CaseComments");
        public By AmendSpecialistRequestedButton_Element => By.CssSelector("#amendSpecialistId");
        public By ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element => By.CssSelector("#ServiceTypeRequestedMultiIDOptions-checkbox-item-0");
        public By Specialist_Psychologist_Checkbox_Element => By.CssSelector("#SpecialistRequestedMultiIDOptions-checkbox-item-0");
        public By Specialist_AmendPsychologist_Checkbox_Element => By.CssSelector("#SpecialistRequestedAmendedOptions-checkbox-item-0");
        public By Specialist_SpeechPathologist_Checkbox_Element => By.CssSelector("#SpecialistRequestedMultiIDOptions-checkbox-item-1");
        public By Specialist_AmendedSpeechPathologist_Checkbox_Element => By.CssSelector("#SpecialistRequestedAmendedOptions-checkbox-item-1");
        public By Specialist_SocialWorker_Checkbox_Element => By.CssSelector("#SpecialistRequestedMultiIDOptions-checkbox-item-2");
        public By Specialist_AmendSocialWorker_Checkbox_Element => By.CssSelector("#SpecialistRequestedAmendedOptions-checkbox-item-2");
        public By Specialist_VisitingTeacher_Checkbox_Element => By.CssSelector("#SpecialistRequestedMultiIDOptions-checkbox-item-3");
        public By Specialist_AmendVisitingTeacher_Checkbox_Element => By.CssSelector("#SpecialistRequestedAmendedOptions-checkbox-item-3");
        public By Specialist_Other_Checkbox_Element => By.CssSelector("#SpecialistRequestedMultiIDOptions-checkbox-item-4");
        public By Specialist_AmendOther_Checkbox_Element => By.CssSelector("#SpecialistRequestedAmendedOptions-checkbox-item-4");
        public By Specialist_SSSOExpert_Checkbox_Element => By.CssSelector("#SpecialistRequestedMultiIDOptions-checkbox-item-5");
        public By Specialist_AmendSSSOExpert_Checkbox_Element => By.CssSelector("#SpecialistRequestedAmendedOptions-checkbox-item-5");
        public By SchoolInformationInput_Element => By.CssSelector("#InformationReStudent");
        public By AgenciesInformationInput_Element => By.CssSelector("#InformationFromSpecialists");
        public By FamilyInformationInput_Element => By.CssSelector("#InformationFromFamily");
        public By GuardianCheckbox_Element => By.CssSelector("#GuardianConsent");

        //Support following critical incident Section Elements
        public By SupportConcern_FireCheckbox_Element => By.CssSelector("#SupportFollowingPPIMultiIDOptions-checkbox-item-0");

        //Mental Health Section Elements
        public By MHConcern_BodyImageCheckbox_Element => By.CssSelector("#MentalHealthPPIMultiIDOptions-checkbox-item-0");

        //Medical Health Physical Section Elements
        public By MHPConcern_AlcoholDrugCheckbox_Element => By.CssSelector("#MedicalHealthPPIMultiIDOptions-checkbox-item-0");
        
        //Attendance Section Element
        public By AttendanceConcern_ChronicIllnessCheckbox_Element => By.CssSelector("#AttendancePPIMultiIDOptions-checkbox-item-0");
        
        //Behaviour Section Element
        public By BehaviourConcern_AggressionangerCheckbox_Element => By.CssSelector("#BehaviourPPIMultiIDOptions-checkbox-item-0");
        
        //SocialEmotional Section Element
        public By SocialEmotionalConcern_AggressionangerCheckbox_Element => By.CssSelector("#SocialEmotionalPPIMultiIDOptions-checkbox-item-0");
        
        //CurriculumLearning Section Element
        public By CurriculumLearningConcern_DifficultystayingontaskCheckbox_Element => By.CssSelector("#CurriculumLearningPPIMultiIDOptions-checkbox-item-0");

        //CommunicationSpeech Section Element
        public By CommunicationSpeechConcern_ArticulationCheckbox_Element => By.CssSelector("#CommunicationSpeechPPIMultiIDOptions-checkbox-item-0");

        //Generic buttons
        public By UpdateButton_Element => By.CssSelector("#cmdSave");
        public By CancelButton_Element => By.CssSelector("#cmdCancel");

        public By Submitbutton_Element => By.CssSelector("#cmdSubmit");
        public By CloseCasebutton_Element => By.CssSelector("#cmdCloseCase");
        public By ReturnCasebutton_Element => By.CssSelector("#cmdReturnCase");
        public By ReopenCasebutton_Element => By.CssSelector("#Submit1");

        public By SaveSchoolCaseButton_Element => By.CssSelector("#cmdCreateCase");
        public By UpdateSchoolCaseButton_Element => By.CssSelector("#cmdUpdateCase");
        public By AddCaseNoteButton_Element => By.CssSelector("#cmdAddCaseNote");
        public By CloseCaseCommentInput_Element => By.CssSelector("#CompletionComment");
        public By CloseCaseCommitButton_Element => By.CssSelector("#cmdCloseCaseCommit");
        public By ReturnCaseCommentInput_Element => By.CssSelector("#ReturnForAmendmentComment");
        public By ReturnCaseCommitButton_Element => By.CssSelector("#cmdReturnCaseCommit");
        public By ReturnToCaseButton_Element => By.CssSelector("#cmdReturnToCase");

        //School Case sections
        public By SupportFollowingSection_Element => By.CssSelector("#SupportFollowingPPI");
        public By MentalHealthSection_Element => By.CssSelector("#MentalHealthPPI");
        public By MedicalHealthPhysicalSection_Element => By.CssSelector("#MedicalHealthPPI");
        public By AttendanceSection_Element => By.CssSelector("#AttendancePPI");
        public By BehaviourSection_Element => By.CssSelector("#BehaviourPPI");
        public By CurriculumLearningSection_Element => By.CssSelector("#CurriculumLearningPPI");
        public By CommunicationSpeechSection_Element => By.CssSelector("#CommunicationSpeechPPI");
        public By SocialEmotionalSection_Element => By.CssSelector("#SocialEmotionalPPI");
        public By PreInterventionSection_Element => By.CssSelector("#fq");
        public By CaseDetailsSection_Element => By.CssSelector("#cd");
        public By StudentDetailsSection_Element => By.CssSelector("#sd");
        public By SCN_StudentCaseNoteSection_Element => By.CssSelector("#scn");
        public By SOCSection_Element => By.CssSelector("#soc");
        public By ServiceMilestonesSection_Element => By.CssSelector("#ad");
        public By ServicesSection_Element => By.CssSelector("#Services");
        public By ConsentFormsSection_Element => By.CssSelector("#ConsentForms");
        public By PreReferralFormsSection_Element => By.CssSelector("#PreReferralForms");
        public By PostInterventionSection_Element => By.CssSelector("#fqpost");
        public By CaseHistorySection_Element => By.CssSelector("#fsHistory");
        public By SchoolMovementHistorySection_Element => By.CssSelector("#smHistory");
        
        //School Case Note Elements
        public By SCN_DurationHrsDropdown_Element => By.CssSelector("#sss_Date_Hour");
        public By SCN_DurationMinDropdown_Element => By.CssSelector("#sss_Date_Minute");
        public By SCN_SubjectInput_Element => By.CssSelector("#sss_name");
        public By SCN_SchoolActionDropdown_Element => By.CssSelector("#sss_schoolaction");
        public By SCN_NotesInput_Element => By.CssSelector("#sss_Notes");
        public By SCN_SaveCaseNoteButton_Element => By.CssSelector("#cmdCreateSchoolCaseNote");
        
        public By SCN_StudentCaseNoteSectionGrid_Element => By.CssSelector("#SchoolCaseNotesGridDiv");
        
        //Consent Elements
        public By AddConsentFormButton_Element => By.XPath("//a[contains(text(),'Add Consent Form')]");
        public By AddConsentForm_Element => By.XPath("//h2[contains(text(),'Add New Consent Form')]");
        public By ChooseFileButton_Element => By.CssSelector("#FileAttachment");
        public By UploadFormButton_Element => By.CssSelector("#cmdUpload");
        public By UploadFormMessage_Element => By.XPath("//span[@class='message']");
        public By CurrentConsentForm_Element => By.XPath("//label[contains(text(),'Current Consent Form')]");

        
        //
        
        public By ActionSection_Element => By.CssSelector("#action2");
        public By ActionDropdown_Element => By.CssSelector("#Action");
        public By RecommendationCategoryDropdown_Element => By.CssSelector("#RecommendationCategory");
        public By GuardianObtainedCheckbox_Element => By.CssSelector("#GuardianObtained");
        public By AssignCaseLeaderbutton_Element => By.CssSelector("#cmdAssignCaseManagerButton");
        public By ReAssignCaseLeaderbutton_Element => By.CssSelector("#ReassignCaseManagerButton");
        public By AssignCaseLeaderForm_Element => By.XPath("//h2[contains(text(),'Assign case leader - Select Contact.')]");
        public By AssignCaseWorkerForm_Element => By.XPath("//h2[contains(text(),'Assign case worker - Select Contact.')]");
        
        //filter elements
        public By FilterFirstNameInput_Element => By.CssSelector("#FilterFirstName");
        public By FilterLastNameInput_Element => By.CssSelector("#FilterLastName");
        public By FilterUserRoleDropdown_Element => By.CssSelector("#FilterUserRole");
        public By FilterSpecialtyDropdown_Element => By.CssSelector("#FilterSpeciality");
        public By FilterApplyFilterButton_Element => By.CssSelector("#cmdSearch");
        public By FilterSelectLink_Element => By.XPath("//a[contains(text(),'Select')]");
        
        public By CaseLeader_Element => By.CssSelector("#CaseManagerIDName");
        public By CaseWorker_Element => By.CssSelector("#ServiceResponsibleIDName");

        //Service Elements
        public By ServicesForm_Element => By.XPath("//div[@id='Services']//div[@class='panel-body']");
        public By ServiceDetailsForm_Element => By.CssSelector("#serviceDetailsForm");
        public By ServiceCloseCasePlan_Element => By.CssSelector("#cmdCloseService");
        public By ServiceDetailsAssignButton_Element => By.CssSelector("#AssignServiceResponsibleContactButton");
        public By ServiceDetailsClosedDates_Element => By.CssSelector("#ServiceClosedDate");

        public By ServiceReviewDate_Element => By.XPath("//img[@class='ui-datepicker-trigger']");
        public By ServiceReviewDate_Today_Element => By.XPath("//td[contains(@class,'ui-datepicker-today')]");
        public By InterventionServiceTypeDropdown_Element => By.CssSelector("#InterventionsServiceType");

        //Case Notes Elements
        public By CaseNoteTitleInput_Element => By.CssSelector("#Subject");
        public By CaseNoteForm_Element => By.XPath("//h2[contains(text(),'Case Note')]");
        public By CaseNoteAdd_Element => By.XPath("//a[@class='t-grid-action t-button t-state-default']");
        public By CaseNoteLink_Element => By.XPath("//a[contains(text(),'Click to add a Case Note/Event')]");
        public By CaseNoteEventDate_Element => By.XPath("//img[@class='ui-datepicker-trigger']");
        public By CaseNoteEventDate_Today_Element => By.XPath("//td[contains(@class,'ui-datepicker-today')]");
        public By CaseNoteEventTypeDropdown_Element => By.CssSelector("#EventType");
        public By CaseNoteEventServiceDelHrsDropdown_Element => By.CssSelector("#TimeHours");
        public By CaseNoteEventServiceDelMinDropdown_Element => By.CssSelector("#TimeMins");
        public By CaseNoteEventTravelHrsDropdown_Element => By.CssSelector("#TravelTimeHours");
        public By CaseNoteEventTravelMinDropdown_Element => By.CssSelector("#TravelTimeMinutes");
        public By CaseNote_NoteInput_Element => By.CssSelector("#ActivityNotes");
        public By CaseNoteUploadButton_Element => By.CssSelector("#cmdFileAttachment");

        //Review Questions Form Elements
        public By ReviewQuestionsForm_Element => By.XPath("//h2[contains(text(),'Review questions')]");
        public By InterventionOutcomesDropdown_Element => By.CssSelector("#FeedbackOptionsQuestion8");
        public By InterventionAimsDropdown_Element => By.CssSelector("#FeedbackOptionsQuestion9");

        //Current active referral Elements
        public By CurrentActiveReferralForm_Element => By.XPath("//h2[contains(text(),'Current active referral')]");
        public By CurrentActiveReferralMessage_Element => By.XPath("//*[@id='main']/span");
        internal void ClickSchoolCase()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(SchoolCase_Element));

            Driver.FindElement(SchoolCase_Element).Click();
        }
        internal void SelectStudent(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(60));
            wait.Until(x => x.FindElement(SchoolCaseForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //input FirstName
                wait.Until(x => x.FindElement(StudentFNInput_Element));
                Driver.FindElement(StudentFNInput_Element).SendKeys(row.ItemArray[0].ToString());

                //click Apply Filter button
                wait.Until(x => x.FindElement(ApplyFilterButton_Element));
                Driver.FindElement(ApplyFilterButton_Element).Click();

                //click select link
                wait.Until(x => x.FindElement(StudentSelectLink_Element));
                Driver.FindElement(StudentSelectLink_Element).Click();

                //click Next button in confirmation screen
                wait.Until(x => x.FindElement(NextButton_Element));
                Driver.FindElement(NextButton_Element).Click();

            }

        }
        internal void FillInitialDetails(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Request type dropdown
                wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                SelectElement requestType = new SelectElement(Driver.FindElement(RequestTypeDropdown_Element));
                requestType.SelectByText(row.ItemArray[0].ToString());

                //select from Primary Presenting Issues
                wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                primaryPresentingIssues.SelectByText(row.ItemArray[1].ToString());
                if (row.ItemArray[1].ToString().Equals("Attendance"))
                {
                    FillInIndividualStudentCase_Attendance(table);
                }
                else
                {
                    if (row.ItemArray[1].ToString().Equals("Behaviour"))
                    {
                        FillInIndividualStudentCase_Behaviour(table);
                    }
                    else
                    {
                        if (row.ItemArray[1].ToString().Equals("Social/emotional"))
                        {
                            FillInIndividualStudentCase_Socialemotional(table);
                        }
                        else
                        {
                            if (row.ItemArray[1].ToString().Equals("Curriculum/learning"))
                            {
                                FillInIndividualStudentCase_CurriculumLearning(table);
                            }
                            else
                            {
                                if (row.ItemArray[1].ToString().Equals("Medical Health Physical"))
                                {
                                    FillInIndividualStudentCase_MedicalHealthPhysical(table);
                                }
                                else
                                {
                                    if (row.ItemArray[1].ToString().Equals("Hearing"))
                                    {
                                        FillInIndividualStudentCase(table);
                                    }
                                    else
                                    {
                                        if (row.ItemArray[1].ToString().Equals("Mental Health"))
                                        {
                                            FillInIndividualStudentCase_MentalHealth(table);
                                        }
                                        else
                                        {
                                            if (row.ItemArray[1].ToString().Equals("Support following critical incident"))
                                            {
                                                FillInIndividualStudentCase_Supportfollowingcriticalincident(table);
                                            }
                                            else
                                            {
                                                if (row.ItemArray[1].ToString().Equals("Vision"))
                                                {
                                                    FillInIndividualStudentCase(table);
                                                }
                                                else
                                                {
                                                    if (row.ItemArray[1].ToString().Equals("Physical Disability"))
                                                    {
                                                        FillInIndividualStudentCase(table);
                                                    }
                                                    else
                                                    {
                                                        if (row.ItemArray[1].ToString().Equals("Whole school issues"))
                                                        {
                                                            FillInIndividualStudentCase(table);
                                                        }
                                                        else
                                                        {
                                                            if (row.ItemArray[1].ToString().Equals("Educational Needs Assessment"))
                                                            {
                                                                FillInIndividualStudentCase(table);
                                                            }
                                                            else
                                                            {
                                                                throw new Exception("Primary Presenting Issue set in the table is incorrect.");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        internal void FillInIndividualStudentCase_Supportfollowingcriticalincident(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //click Supportfollowingcriticalincident Section
                wait.Until(x => x.FindElement(SupportFollowingSection_Element));
                Driver.FindElement(SupportFollowingSection_Element).Click();
                Thread.Sleep(1000);

                //Select "Fire" as Support Following Critical Incident Concern(s)
                wait.Until(x => x.FindElement(SupportConcern_FireCheckbox_Element));
                Driver.FindElement(SupportConcern_FireCheckbox_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //random generator/input details for School Information
                wait.Until(x => x.FindElement(SchoolInformationInput_Element));

                Random generatorSchoolInfo = new Random();
                int iSchoolInfo = generatorSchoolInfo.Next(1, 10000000);
                string sSchoolInfo = iSchoolInfo.ToString().PadLeft(7, '0');
                string eSchoolInfo = "Test School Information_" + DateTime.Now.ToString() + "_" + sSchoolInfo;
                Driver.FindElement(SchoolInformationInput_Element).SendKeys(eSchoolInfo);

                //random generator/input details for Agency Information
                wait.Until(x => x.FindElement(AgenciesInformationInput_Element));

                Random generatorAgency = new Random();
                int iAgencyInfo = generatorAgency.Next(1, 10000000);
                string sAgencyInfo = iAgencyInfo.ToString().PadLeft(7, '0');
                string eAgencyInfo = "Test Agency Information_" + DateTime.Now.ToString() + "_" + sAgencyInfo;
                Driver.FindElement(AgenciesInformationInput_Element).SendKeys(eAgencyInfo);

                //random generator/input details for Family Information
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));

                Random generatorFamily = new Random();
                int iFamilyInfo = generatorFamily.Next(1, 10000000);
                string sFamilyInfo = iFamilyInfo.ToString().PadLeft(7, '0');
                string eFamilyInfo = "Test Family Information_" + DateTime.Now.ToString() + "_" + sFamilyInfo;
                Driver.FindElement(FamilyInformationInput_Element).SendKeys(eFamilyInfo);

                //click Guardian checkbox
                wait.Until(x => x.FindElement(GuardianCheckbox_Element));
                Driver.FindElement(GuardianCheckbox_Element).Click();

                //click Save School Case button
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));
                Driver.FindElement(SaveSchoolCaseButton_Element).Click();

                //wait for the form
                wait.Until(x => x.FindElement(SchoolCaseHeading_Element));
                Assert.AreEqual("Case saved", Driver.FindElement(SchoolCaseHeading_Element).Text, "Case not saved");

                //verify if there's case #
                wait.Until(x => x.FindElement(Casenumber_Element));
                Assert.IsNotEmpty(Driver.FindElement(Casenumber_Element).Text, "No case number");
                Thread.Sleep(2000);
            }

        }
        internal void FillInIndividualStudentCase_MentalHealth(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //click MentalHealth Section
                wait.Until(x => x.FindElement(MentalHealthSection_Element));
                Driver.FindElement(MentalHealthSection_Element).Click();
                Thread.Sleep(1000);

                //Select "Body image or eating disorders" as MentalHealth Concerns
                wait.Until(x => x.FindElement(MHConcern_BodyImageCheckbox_Element));
                Driver.FindElement(MHConcern_BodyImageCheckbox_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //random generator/input details for School Information
                wait.Until(x => x.FindElement(SchoolInformationInput_Element));

                Random generatorSchoolInfo = new Random();
                int iSchoolInfo = generatorSchoolInfo.Next(1, 10000000);
                string sSchoolInfo = iSchoolInfo.ToString().PadLeft(7, '0');
                string eSchoolInfo = "Test School Information_" + DateTime.Now.ToString() + "_" + sSchoolInfo;
                Driver.FindElement(SchoolInformationInput_Element).SendKeys(eSchoolInfo);

                //random generator/input details for Agency Information
                wait.Until(x => x.FindElement(AgenciesInformationInput_Element));

                Random generatorAgency = new Random();
                int iAgencyInfo = generatorAgency.Next(1, 10000000);
                string sAgencyInfo = iAgencyInfo.ToString().PadLeft(7, '0');
                string eAgencyInfo = "Test Agency Information_" + DateTime.Now.ToString() + "_" + sAgencyInfo;
                Driver.FindElement(AgenciesInformationInput_Element).SendKeys(eAgencyInfo);

                //random generator/input details for Family Information
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));

                Random generatorFamily = new Random();
                int iFamilyInfo = generatorFamily.Next(1, 10000000);
                string sFamilyInfo = iFamilyInfo.ToString().PadLeft(7, '0');
                string eFamilyInfo = "Test Family Information_" + DateTime.Now.ToString() + "_" + sFamilyInfo;
                Driver.FindElement(FamilyInformationInput_Element).SendKeys(eFamilyInfo);

                //click Guardian checkbox
                wait.Until(x => x.FindElement(GuardianCheckbox_Element));
                Driver.FindElement(GuardianCheckbox_Element).Click();

                //click Save School Case button
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));
                Driver.FindElement(SaveSchoolCaseButton_Element).Click();

                //wait for the form
                wait.Until(x => x.FindElement(SchoolCaseHeading_Element));
                Assert.AreEqual("Case saved", Driver.FindElement(SchoolCaseHeading_Element).Text, "Case not saved");

                //verify if there's case #
                wait.Until(x => x.FindElement(Casenumber_Element));
                Assert.IsNotEmpty(Driver.FindElement(Casenumber_Element).Text, "No case number");
                Thread.Sleep(2000);
            }

        }
        internal void FillInIndividualStudentCase_MedicalHealthPhysical(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //click MedicalHealthPhysical Section
                wait.Until(x => x.FindElement(MedicalHealthPhysicalSection_Element));
                Driver.FindElement(MedicalHealthPhysicalSection_Element).Click();
                Thread.Sleep(1000);

                //Select "Alcohol or other drug concerns" as MedicalHealthPhysical Concerns
                wait.Until(x => x.FindElement(MHPConcern_AlcoholDrugCheckbox_Element));
                Driver.FindElement(MHPConcern_AlcoholDrugCheckbox_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //random generator/input details for School Information
                wait.Until(x => x.FindElement(SchoolInformationInput_Element));

                Random generatorSchoolInfo = new Random();
                int iSchoolInfo = generatorSchoolInfo.Next(1, 10000000);
                string sSchoolInfo = iSchoolInfo.ToString().PadLeft(7, '0');
                string eSchoolInfo = "Test School Information_" + DateTime.Now.ToString() + "_" + sSchoolInfo;
                Driver.FindElement(SchoolInformationInput_Element).SendKeys(eSchoolInfo);

                //random generator/input details for Agency Information
                wait.Until(x => x.FindElement(AgenciesInformationInput_Element));

                Random generatorAgency = new Random();
                int iAgencyInfo = generatorAgency.Next(1, 10000000);
                string sAgencyInfo = iAgencyInfo.ToString().PadLeft(7, '0');
                string eAgencyInfo = "Test Agency Information_" + DateTime.Now.ToString() + "_" + sAgencyInfo;
                Driver.FindElement(AgenciesInformationInput_Element).SendKeys(eAgencyInfo);

                //random generator/input details for Family Information
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));

                Random generatorFamily = new Random();
                int iFamilyInfo = generatorFamily.Next(1, 10000000);
                string sFamilyInfo = iFamilyInfo.ToString().PadLeft(7, '0');
                string eFamilyInfo = "Test Family Information_" + DateTime.Now.ToString() + "_" + sFamilyInfo;
                Driver.FindElement(FamilyInformationInput_Element).SendKeys(eFamilyInfo);

                //click Guardian checkbox
                wait.Until(x => x.FindElement(GuardianCheckbox_Element));
                Driver.FindElement(GuardianCheckbox_Element).Click();

                //click Save School Case button
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));
                Driver.FindElement(SaveSchoolCaseButton_Element).Click();

                //wait for the form
                wait.Until(x => x.FindElement(SchoolCaseHeading_Element));
                Assert.AreEqual("Case saved", Driver.FindElement(SchoolCaseHeading_Element).Text, "Case not saved");

                //verify if there's case #
                wait.Until(x => x.FindElement(Casenumber_Element));
                Assert.IsNotEmpty(Driver.FindElement(Casenumber_Element).Text, "No case number");
                Thread.Sleep(2000);
            }

        }
        internal void FillInIndividualStudentCase_Attendance(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Request type dropdown
                //wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                //SelectElement requestType = new SelectElement(Driver.FindElement(RequestTypeDropdown_Element));
                //requestType.SelectByText(row.ItemArray[0].ToString());

                //select from Primary Presenting Issues
                //wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                //SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                //primaryPresentingIssues.SelectByText(row.ItemArray[1].ToString());

                //click Attendance Section
                wait.Until(x => x.FindElement(AttendanceSection_Element));
                Driver.FindElement(AttendanceSection_Element).Click();
                Thread.Sleep(1000);

                //Select "Chronic illness" as Attendance Concerns
                wait.Until(x => x.FindElement(AttendanceConcern_ChronicIllnessCheckbox_Element));
                Driver.FindElement(AttendanceConcern_ChronicIllnessCheckbox_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //random generator/input details for School Information
                wait.Until(x => x.FindElement(SchoolInformationInput_Element));

                Random generatorSchoolInfo = new Random();
                int iSchoolInfo = generatorSchoolInfo.Next(1, 10000000);
                string sSchoolInfo = iSchoolInfo.ToString().PadLeft(7, '0');
                string eSchoolInfo = "Test School Information_" + DateTime.Now.ToString() + "_" + sSchoolInfo;
                Driver.FindElement(SchoolInformationInput_Element).SendKeys(eSchoolInfo);

                //random generator/input details for Agency Information
                wait.Until(x => x.FindElement(AgenciesInformationInput_Element));

                Random generatorAgency = new Random();
                int iAgencyInfo = generatorAgency.Next(1, 10000000);
                string sAgencyInfo = iAgencyInfo.ToString().PadLeft(7, '0');
                string eAgencyInfo = "Test Agency Information_" + DateTime.Now.ToString() + "_" + sAgencyInfo;
                Driver.FindElement(AgenciesInformationInput_Element).SendKeys(eAgencyInfo);

                //random generator/input details for Family Information
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));

                Random generatorFamily = new Random();
                int iFamilyInfo = generatorFamily.Next(1, 10000000);
                string sFamilyInfo = iFamilyInfo.ToString().PadLeft(7, '0');
                string eFamilyInfo = "Test Family Information_" + DateTime.Now.ToString() + "_" + sFamilyInfo;
                Driver.FindElement(FamilyInformationInput_Element).SendKeys(eFamilyInfo);

                //click Guardian checkbox
                wait.Until(x => x.FindElement(GuardianCheckbox_Element));
                Driver.FindElement(GuardianCheckbox_Element).Click();

                //click Save School Case button
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));
                Driver.FindElement(SaveSchoolCaseButton_Element).Click();

                //wait for the form
                wait.Until(x => x.FindElement(SchoolCaseHeading_Element));
                Assert.AreEqual("Case saved", Driver.FindElement(SchoolCaseHeading_Element).Text, "Case not saved");

                //verify if there's case #
                wait.Until(x => x.FindElement(Casenumber_Element));
                Assert.IsNotEmpty(Driver.FindElement(Casenumber_Element).Text, "No case number");
                Thread.Sleep(2000);
            }

        }
        internal void FillInIndividualStudentCase_Behaviour(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Request type dropdown
                //wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                //SelectElement requestType = new SelectElement(Driver.FindElement(RequestTypeDropdown_Element));
                //requestType.SelectByText(row.ItemArray[0].ToString());

                //select from Primary Presenting Issues
                //wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                //SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                //primaryPresentingIssues.SelectByText(row.ItemArray[1].ToString());

                //click Behaviour Section
                wait.Until(x => x.FindElement(BehaviourSection_Element));
                Driver.FindElement(BehaviourSection_Element).Click();
                Thread.Sleep(1000);

                //Select "Aggression/anger [fights with others, swears]" as Behaviour Concerns
                wait.Until(x => x.FindElement(BehaviourConcern_AggressionangerCheckbox_Element));
                Driver.FindElement(BehaviourConcern_AggressionangerCheckbox_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //random generator/input details for School Information
                wait.Until(x => x.FindElement(SchoolInformationInput_Element));

                Random generatorSchoolInfo = new Random();
                int iSchoolInfo = generatorSchoolInfo.Next(1, 10000000);
                string sSchoolInfo = iSchoolInfo.ToString().PadLeft(7, '0');
                string eSchoolInfo = "Test School Information_" + DateTime.Now.ToString() + "_" + sSchoolInfo;
                Driver.FindElement(SchoolInformationInput_Element).SendKeys(eSchoolInfo);

                //random generator/input details for Agency Information
                wait.Until(x => x.FindElement(AgenciesInformationInput_Element));

                Random generatorAgency = new Random();
                int iAgencyInfo = generatorAgency.Next(1, 10000000);
                string sAgencyInfo = iAgencyInfo.ToString().PadLeft(7, '0');
                string eAgencyInfo = "Test Agency Information_" + DateTime.Now.ToString() + "_" + sAgencyInfo;
                Driver.FindElement(AgenciesInformationInput_Element).SendKeys(eAgencyInfo);

                //random generator/input details for Family Information
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));

                Random generatorFamily = new Random();
                int iFamilyInfo = generatorFamily.Next(1, 10000000);
                string sFamilyInfo = iFamilyInfo.ToString().PadLeft(7, '0');
                string eFamilyInfo = "Test Family Information_" + DateTime.Now.ToString() + "_" + sFamilyInfo;
                Driver.FindElement(FamilyInformationInput_Element).SendKeys(eFamilyInfo);

                //click Guardian checkbox
                wait.Until(x => x.FindElement(GuardianCheckbox_Element));
                Driver.FindElement(GuardianCheckbox_Element).Click();

                //click Save School Case button
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));
                Driver.FindElement(SaveSchoolCaseButton_Element).Click();

                //wait for the form
                wait.Until(x => x.FindElement(SchoolCaseHeading_Element));
                Assert.AreEqual("Case saved", Driver.FindElement(SchoolCaseHeading_Element).Text, "Case not saved");

                //verify if there's case #
                wait.Until(x => x.FindElement(Casenumber_Element));
                Assert.IsNotEmpty(Driver.FindElement(Casenumber_Element).Text, "No case number");
                Thread.Sleep(2000);
            }
        }
        internal void FillInIndividualStudentCase_Socialemotional(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Request type dropdown
                //wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                //SelectElement requestType = new SelectElement(Driver.FindElement(RequestTypeDropdown_Element));
                //requestType.SelectByText(row.ItemArray[0].ToString());

                //select from Primary Presenting Issues
                //wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                //SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                //primaryPresentingIssues.SelectByText(row.ItemArray[1].ToString());

                //click Social/Emotional Section
                wait.Until(x => x.FindElement(SocialEmotionalSection_Element));
                Driver.FindElement(SocialEmotionalSection_Element).Click();
                Thread.Sleep(1000);

                //Select "Adjustment or transition" as Social/Emotional
                wait.Until(x => x.FindElement(SocialEmotionalConcern_AggressionangerCheckbox_Element));
                Driver.FindElement(SocialEmotionalConcern_AggressionangerCheckbox_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //random generator/input details for School Information
                wait.Until(x => x.FindElement(SchoolInformationInput_Element));

                Random generatorSchoolInfo = new Random();
                int iSchoolInfo = generatorSchoolInfo.Next(1, 10000000);
                string sSchoolInfo = iSchoolInfo.ToString().PadLeft(7, '0');
                string eSchoolInfo = "Test School Information_" + DateTime.Now.ToString() + "_" + sSchoolInfo;
                Driver.FindElement(SchoolInformationInput_Element).SendKeys(eSchoolInfo);

                //random generator/input details for Agency Information
                wait.Until(x => x.FindElement(AgenciesInformationInput_Element));

                Random generatorAgency = new Random();
                int iAgencyInfo = generatorAgency.Next(1, 10000000);
                string sAgencyInfo = iAgencyInfo.ToString().PadLeft(7, '0');
                string eAgencyInfo = "Test Agency Information_" + DateTime.Now.ToString() + "_" + sAgencyInfo;
                Driver.FindElement(AgenciesInformationInput_Element).SendKeys(eAgencyInfo);

                //random generator/input details for Family Information
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));

                Random generatorFamily = new Random();
                int iFamilyInfo = generatorFamily.Next(1, 10000000);
                string sFamilyInfo = iFamilyInfo.ToString().PadLeft(7, '0');
                string eFamilyInfo = "Test Family Information_" + DateTime.Now.ToString() + "_" + sFamilyInfo;
                Driver.FindElement(FamilyInformationInput_Element).SendKeys(eFamilyInfo);

                //click Guardian checkbox
                wait.Until(x => x.FindElement(GuardianCheckbox_Element));
                Driver.FindElement(GuardianCheckbox_Element).Click();

                //click Save School Case button
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));
                Driver.FindElement(SaveSchoolCaseButton_Element).Click();

                //wait for the form
                wait.Until(x => x.FindElement(SchoolCaseHeading_Element));
                Assert.AreEqual("Case saved", Driver.FindElement(SchoolCaseHeading_Element).Text, "Case not saved");

                //verify if there's case #
                wait.Until(x => x.FindElement(Casenumber_Element));
                Assert.IsNotEmpty(Driver.FindElement(Casenumber_Element).Text, "No case number");
                Thread.Sleep(2000);
            }
        }
        internal void FillInIndividualStudentCase_CurriculumLearning(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Request type dropdown
                //wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                //SelectElement requestType = new SelectElement(Driver.FindElement(RequestTypeDropdown_Element));
                //requestType.SelectByText(row.ItemArray[0].ToString());

                //select from Primary Presenting Issues
                //wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                //SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                //primaryPresentingIssues.SelectByText(row.ItemArray[1].ToString());

                //click Curriculum/Learning Section
                wait.Until(x => x.FindElement(CurriculumLearningSection_Element));
                Driver.FindElement(CurriculumLearningSection_Element).Click();
                Thread.Sleep(1000);

                //Select "Aggression/anger [fights with others, swears]" as Behaviour Concerns
                wait.Until(x => x.FindElement(CurriculumLearningConcern_DifficultystayingontaskCheckbox_Element));
                Driver.FindElement(CurriculumLearningConcern_DifficultystayingontaskCheckbox_Element).Click();

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //random generator/input details for School Information
                wait.Until(x => x.FindElement(SchoolInformationInput_Element));

                Random generatorSchoolInfo = new Random();
                int iSchoolInfo = generatorSchoolInfo.Next(1, 10000000);
                string sSchoolInfo = iSchoolInfo.ToString().PadLeft(7, '0');
                string eSchoolInfo = "Test School Information_" + DateTime.Now.ToString() + "_" + sSchoolInfo;
                Driver.FindElement(SchoolInformationInput_Element).SendKeys(eSchoolInfo);

                //random generator/input details for Agency Information
                wait.Until(x => x.FindElement(AgenciesInformationInput_Element));

                Random generatorAgency = new Random();
                int iAgencyInfo = generatorAgency.Next(1, 10000000);
                string sAgencyInfo = iAgencyInfo.ToString().PadLeft(7, '0');
                string eAgencyInfo = "Test Agency Information_" + DateTime.Now.ToString() + "_" + sAgencyInfo;
                Driver.FindElement(AgenciesInformationInput_Element).SendKeys(eAgencyInfo);

                //random generator/input details for Family Information
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));

                Random generatorFamily = new Random();
                int iFamilyInfo = generatorFamily.Next(1, 10000000);
                string sFamilyInfo = iFamilyInfo.ToString().PadLeft(7, '0');
                string eFamilyInfo = "Test Family Information_" + DateTime.Now.ToString() + "_" + sFamilyInfo;
                Driver.FindElement(FamilyInformationInput_Element).SendKeys(eFamilyInfo);

                //click Guardian checkbox
                wait.Until(x => x.FindElement(GuardianCheckbox_Element));
                Driver.FindElement(GuardianCheckbox_Element).Click();

                //click Save School Case button
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));
                Driver.FindElement(SaveSchoolCaseButton_Element).Click();

                //wait for the form
                wait.Until(x => x.FindElement(SchoolCaseHeading_Element));
                Assert.AreEqual("Case saved", Driver.FindElement(SchoolCaseHeading_Element).Text, "Case not saved");

                //verify if there's case #
                wait.Until(x => x.FindElement(Casenumber_Element));
                Assert.IsNotEmpty(Driver.FindElement(Casenumber_Element).Text, "No case number");
                Thread.Sleep(2000);

            }

        }

        internal void UpdateOthersCommunicationorSpeech()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            //click Communication or Speech Section
            wait.Until(x => x.FindElement(CommunicationSpeechSection_Element));
            Driver.FindElement(CommunicationSpeechSection_Element).Click();
            Thread.Sleep(1000);

            //Select "Articulation (Production of speech sounds)" as Communication or Speech Concerns
            wait.Until(x => x.FindElement(CommunicationSpeechConcern_ArticulationCheckbox_Element));
            Driver.FindElement(CommunicationSpeechConcern_ArticulationCheckbox_Element).Click();

            //click Update School Case button
            wait.Until(x => x.FindElement(UpdateSchoolCaseButton_Element));
            Driver.FindElement(UpdateSchoolCaseButton_Element).Click();

        }
        internal void FillInIndividualStudentCase(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Request type dropdown
                //wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                //SelectElement requestType = new SelectElement(Driver.FindElement(RequestTypeDropdown_Element));
                //requestType.SelectByText(row.ItemArray[0].ToString());

                //select from Primary Presenting Issues
                //wait.Until(x => x.FindElement(RequestTypeDropdown_Element));
                //SelectElement primaryPresentingIssues = new SelectElement(Driver.FindElement(PrimaryPresentingIssueDropdown_Element));
                //primaryPresentingIssues.SelectByText(row.ItemArray[1].ToString());

                //random generator/input details for Briefly describe the underlying issue
                wait.Until(x => x.FindElement(CaseCommentInput_Element));

                Random generatorComment = new Random();
                int iComment = generatorComment.Next(1, 10000000);
                string sComment = iComment.ToString().PadLeft(7, '0');
                string eComment = "Test Comment_" + DateTime.Now.ToString() + "_" + sComment;
                Driver.FindElement(CaseCommentInput_Element).SendKeys(eComment);

                //Select "Assessment including preliminary advice" as Service type requested
                wait.Until(x => x.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element));
                Driver.FindElement(ServiceType_AssessmentIncludingPreliminaryAdviceCheckbox_Element).Click();

                //Select Psychologist as Specialist requested
                wait.Until(x => x.FindElement(Specialist_Psychologist_Checkbox_Element));
                Driver.FindElement(Specialist_Psychologist_Checkbox_Element).Click();

                //random generator/input details for School Information
                wait.Until(x => x.FindElement(SchoolInformationInput_Element));

                Random generatorSchoolInfo = new Random();
                int iSchoolInfo = generatorSchoolInfo.Next(1, 10000000);
                string sSchoolInfo = iSchoolInfo.ToString().PadLeft(7, '0');
                string eSchoolInfo = "Test School Information_" + DateTime.Now.ToString() + "_" + sSchoolInfo;
                Driver.FindElement(SchoolInformationInput_Element).SendKeys(eSchoolInfo);

                //random generator/input details for Agency Information
                wait.Until(x => x.FindElement(AgenciesInformationInput_Element));

                Random generatorAgency = new Random();
                int iAgencyInfo = generatorAgency.Next(1, 10000000);
                string sAgencyInfo = iAgencyInfo.ToString().PadLeft(7, '0');
                string eAgencyInfo = "Test Agency Information_" + DateTime.Now.ToString() + "_" + sAgencyInfo;
                Driver.FindElement(AgenciesInformationInput_Element).SendKeys(eAgencyInfo);

                //random generator/input details for Family Information
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));

                Random generatorFamily = new Random();
                int iFamilyInfo = generatorFamily.Next(1, 10000000);
                string sFamilyInfo = iFamilyInfo.ToString().PadLeft(7, '0');
                string eFamilyInfo = "Test Family Information_" + DateTime.Now.ToString() + "_" + sFamilyInfo;
                Driver.FindElement(FamilyInformationInput_Element).SendKeys(eFamilyInfo);

                //click Guardian checkbox
                wait.Until(x => x.FindElement(GuardianCheckbox_Element));
                Driver.FindElement(GuardianCheckbox_Element).Click();

                //click Save School Case button
                wait.Until(x => x.FindElement(SaveSchoolCaseButton_Element));
                Driver.FindElement(SaveSchoolCaseButton_Element).Click();

                //wait for the form
                wait.Until(x => x.FindElement(SchoolCaseHeading_Element));
                Assert.AreEqual("Case saved", Driver.FindElement(SchoolCaseHeading_Element).Text, "Case not saved");

                //verify if there's case #
                wait.Until(x => x.FindElement(Casenumber_Element));
                Assert.IsNotEmpty(Driver.FindElement(Casenumber_Element).Text, "No case number");
                Thread.Sleep(2000);

            }
        }
        public string GetCaseNumber()
        {
            //WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            string strCaseNumber = (Driver.FindElement(Casenumber_Element).Text);
            return strCaseNumber;
        }
        internal void SubmitExistingStudentCase()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            /*wait.Until(x => x.FindElement(GuardianCheckbox_Element));
            if (Driver.FindElement(GuardianCheckbox_Element).GetAttribute("checked").Equals("checked"))
            { }
            else
            {
                //click Guardian checkbox if not yet checked
                Driver.FindElement(GuardianCheckbox_Element).Click();
            }*/

            //click submit
            wait.Until(x => x.FindElement(Submitbutton_Element));
            Driver.FindElement(Submitbutton_Element).Click();

            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            //verify the message
            wait.Until(x => x.FindElement(ConfirmationMessage_Element));
            Assert.AreEqual("Case submitted\r\nCase sent to Student Support Services.", Driver.FindElement(ConfirmationMessage_Element).Text, "Case not submitted");

            //verify the status
            wait.Until(x => x.FindElement(CaseStatus_Element));
            Assert.AreEqual("Submitted", Driver.FindElement(CaseStatus_Element).Text, "Case not submitted-incorrect status");
            Thread.Sleep(2000);

        }
        internal void AttachConsentForm()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            //click Consent Form
            wait.Until(x => x.FindElement(AddConsentFormButton_Element));
            Driver.FindElement(AddConsentFormButton_Element).Click();

            wait.Until(x => x.FindElement(AddConsentForm_Element));

            //click Choose file button
            var winHandleBefore = Driver.CurrentWindowHandle;
            wait.Until(x => x.FindElement(ChooseFileButton_Element));
            Driver.FindElement(ChooseFileButton_Element).Click();

            //Switch to upload window
            Driver.SwitchTo().Window(Driver.WindowHandles.Last());
            //Uploading the file using keyboard actions
            Thread.Sleep(2000);

            var fileNamePath = CommonFunctions.GetResourceValue("FileUpload_ConsentForm");
            SendKeys.SendWait(fileNamePath);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(5000);
            Driver.FindElement(UploadFormButton_Element).Click();

            //verify the message
            wait.Until(x => x.FindElement(UploadFormMessage_Element));
            Assert.AreEqual("Consent Form Created Succesfully", Driver.FindElement(UploadFormMessage_Element).Text, "File not uploaded");

            wait.Until(x => x.FindElement(CurrentConsentForm_Element));
            Assert.AreEqual("Current Consent Form", Driver.FindElement(CurrentConsentForm_Element).Text, "No file uploaded");

            wait.Until(x => x.FindElement(ReturnToCaseButton_Element));
            Driver.FindElement(ReturnToCaseButton_Element).Click();

            //Brings the control back to original window
            Driver.SwitchTo().Window(winHandleBefore);
        }
        internal void Action(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Area Team case dropdown
                wait.Until(x => x.FindElement(PriorityDropdown_Element));
                SelectElement priority = new SelectElement(Driver.FindElement(PriorityDropdown_Element));
                priority.SelectByText(row.ItemArray[0].ToString());

                //click Action Section
                wait.Until(x => x.FindElement(ActionSection_Element));
                Driver.FindElement(ActionSection_Element).Click();
                Thread.Sleep(1000);

                //select from Action dropdown
                wait.Until(x => x.FindElement(ActionDropdown_Element));
                SelectElement action = new SelectElement(Driver.FindElement(ActionDropdown_Element));
                action.SelectByText(row.ItemArray[1].ToString());

                //select from Recommendations dropdown
                wait.Until(x => x.FindElement(RecommendationCategoryDropdown_Element));
                SelectElement recommendation = new SelectElement(Driver.FindElement(RecommendationCategoryDropdown_Element));
                recommendation.SelectByText(row.ItemArray[2].ToString());

                //click Guardian consent received and accepted? checkbox
                wait.Until(x => x.FindElement(GuardianObtainedCheckbox_Element));
                Driver.FindElement(GuardianObtainedCheckbox_Element).Click();

                //click Update School Case button
                wait.Until(x => x.FindElement(UpdateSchoolCaseButton_Element));
                Driver.FindElement(UpdateSchoolCaseButton_Element).Click();
            }
        }
        internal void CloseCaseAction(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Area Team case dropdown
                wait.Until(x => x.FindElement(PriorityDropdown_Element));
                SelectElement priority = new SelectElement(Driver.FindElement(PriorityDropdown_Element));
                priority.SelectByText(row.ItemArray[0].ToString());

                //click Action Section
                wait.Until(x => x.FindElement(ActionSection_Element));
                Driver.FindElement(ActionSection_Element).Click();
                Thread.Sleep(1000);

                //select from Action dropdown
                wait.Until(x => x.FindElement(ActionDropdown_Element));
                SelectElement action = new SelectElement(Driver.FindElement(ActionDropdown_Element));
                action.SelectByText(row.ItemArray[1].ToString());

                //click Save School Case button
                wait.Until(x => x.FindElement(UpdateSchoolCaseButton_Element));
                Driver.FindElement(UpdateSchoolCaseButton_Element).Click();

                //verify the message
                wait.Until(x => x.FindElement(ConfirmationMessage_Element));
                Assert.AreEqual("Case closed\r\nCase was closed successfully.", Driver.FindElement(ConfirmationMessage_Element).Text, "Student Case not closed");
                Thread.Sleep(2000);

                //click CaseDetailsSection
                wait.Until(x => x.FindElement(CaseDetailsSection_Element));
                Driver.FindElement(CaseDetailsSection_Element).Click();
                Thread.Sleep(1000);

                //verify the status
                wait.Until(x => x.FindElement(CaseStatus_Element));
                Assert.AreEqual("Completed", Driver.FindElement(CaseStatus_Element).Text, "Case not closed-incorrect status");
                Thread.Sleep(2000);
            }
        }
        internal void AssignCaseLeader(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            //click Assign Case Leader button
            wait.Until(x => x.FindElement(AssignCaseLeaderbutton_Element));
            Driver.FindElement(AssignCaseLeaderbutton_Element).Click();
            Thread.Sleep(1000);

            wait.Until(x => x.FindElement(AssignCaseLeaderForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //enter First Name
                wait.Until(x => x.FindElement(FilterFirstNameInput_Element));
                Driver.FindElement(FilterFirstNameInput_Element).SendKeys(row.ItemArray[0].ToString());

                //enter Last Name
                wait.Until(x => x.FindElement(FilterLastNameInput_Element));
                Driver.FindElement(FilterLastNameInput_Element).SendKeys(row.ItemArray[1].ToString());

                //select from User Role dropdown
                wait.Until(x => x.FindElement(FilterUserRoleDropdown_Element));
                SelectElement userRole = new SelectElement(Driver.FindElement(FilterUserRoleDropdown_Element));
                userRole.SelectByText(row.ItemArray[2].ToString());

                //select from Specialty dropdown
                //wait.Until(x => x.FindElement(FilterSpecialtyDropdown_Element));
                //SelectElement specialty = new SelectElement(Driver.FindElement(FilterSpecialtyDropdown_Element));
                //specialty.SelectByText(row.ItemArray[3].ToString());

                //click Apply Filter
                wait.Until(x => x.FindElement(FilterApplyFilterButton_Element));
                Driver.FindElement(FilterApplyFilterButton_Element).Click();

                //click select
                wait.Until(x => x.FindElement(FilterSelectLink_Element));
                Driver.FindElement(FilterSelectLink_Element).Click();
                wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

                //Assert if case leader added successfully
                wait.Until(x => x.FindElement(ConfirmationMessage_Element));
                Thread.Sleep(2000);
                Assert.AreEqual("Case leader assigned\r\nCase leader was assigned successfully. Return to the Cases menu to view/edit Cases", Driver.FindElement(ConfirmationMessage_Element).Text, "Case leader not assigned");
                Thread.Sleep(2000);

                //click Update School Case button
                wait.Until(x => x.FindElement(UpdateSchoolCaseButton_Element));
                Driver.FindElement(UpdateSchoolCaseButton_Element).Click();

                //Click on the case leader section
                wait.Until(x => x.FindElement(CaseDetailsSection_Element));
                Driver.FindElement(CaseDetailsSection_Element).Click();

                //verify the case leader
                wait.Until(x => x.FindElement(CaseLeader_Element));
                Assert.AreEqual((row.ItemArray[0].ToString() + " " + row.ItemArray[1].ToString()), Driver.FindElement(CaseLeader_Element).GetAttribute("textContent"), "Incorrect Case Leader assigned");
                Thread.Sleep(2000);
            }
        }
        internal void ReAssignCaseLeader(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            //click CaseDetailsSection
            wait.Until(x => x.FindElement(CaseDetailsSection_Element));
            Driver.FindElement(CaseDetailsSection_Element).Click();
            Thread.Sleep(1000);

            
            //click ReAssign Case Leader button
            wait.Until(x => x.FindElement(ReAssignCaseLeaderbutton_Element));
            Driver.FindElement(ReAssignCaseLeaderbutton_Element).Click();
            Thread.Sleep(1000);

            wait.Until(x => x.FindElement(AssignCaseLeaderForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //enter First Name
                wait.Until(x => x.FindElement(FilterFirstNameInput_Element));
                Driver.FindElement(FilterFirstNameInput_Element).SendKeys(row.ItemArray[0].ToString());

                //enter Last Name
                wait.Until(x => x.FindElement(FilterLastNameInput_Element));
                Driver.FindElement(FilterLastNameInput_Element).SendKeys(row.ItemArray[1].ToString());

                //select from User Role dropdown
                wait.Until(x => x.FindElement(FilterUserRoleDropdown_Element));
                SelectElement userRole = new SelectElement(Driver.FindElement(FilterUserRoleDropdown_Element));
                userRole.SelectByText(row.ItemArray[2].ToString());

                //select from Specialty dropdown
                //wait.Until(x => x.FindElement(FilterSpecialtyDropdown_Element));
                //SelectElement specialty = new SelectElement(Driver.FindElement(FilterSpecialtyDropdown_Element));
                //specialty.SelectByText(row.ItemArray[3].ToString());

                //click Apply Filter
                wait.Until(x => x.FindElement(FilterApplyFilterButton_Element));
                Driver.FindElement(FilterApplyFilterButton_Element).Click();

                //click select
                wait.Until(x => x.FindElement(FilterSelectLink_Element));
                Driver.FindElement(FilterSelectLink_Element).Click();
                wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

                //click Update School Case button
                wait.Until(x => x.FindElement(UpdateSchoolCaseButton_Element));
                Driver.FindElement(UpdateSchoolCaseButton_Element).Click();

                //verify the case leader
                wait.Until(x => x.FindElement(CaseLeader_Element));
                Assert.AreEqual((row.ItemArray[0].ToString() + " " + row.ItemArray[1].ToString()), Driver.FindElement(CaseLeader_Element).GetAttribute("textContent"), "Incorrect Case Leader assigned");
                Thread.Sleep(2000);
            }
        }
        internal void SelectService(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            //click Services Section
            wait.Until(x => x.FindElement(ServicesSection_Element));
            Driver.FindElement(ServicesSection_Element).Click();
            Thread.Sleep(1000);

            wait.Until(x => x.FindElement(ServicesForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                var TableRows = Driver.FindElements(By.XPath("//div[@id='ServiceList']//table//tbody//tr"));

                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));
                    //look for the service type
                    if (td[1].Text.Contains(row.ItemArray[0].ToString()))
                    {
                        td[0].FindElement(By.TagName("a")).Click();
                        break;
                    }
                }
            }
        }
        internal void SelectServicesAndAssign(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            //click Services Section
            wait.Until(x => x.FindElement(ServicesSection_Element));
            Driver.FindElement(ServicesSection_Element).Click();
            Thread.Sleep(1000);

            wait.Until(x => x.FindElement(ServicesForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //SplitContainer servicesSection = new SplitContainer();
                //((ServicesSection_Element)servicesSection.Panel1);
                //((ServicesSection_Element)servicesSection.Panel1);
                //var serviceSection = SplitterPanel(Driver.FindElement(ServicesSection_Element));

                //SplitContainer servicesSection = new SplitContainer();
                //servicesSection.Panel1.VisibleChanged += SplitterPanel_Collapsed;
                //(Driver.FindElement(ServicesSection_Element))
                //((ServicesSection_Element)servicesSection.Panel1).VisibleChanged += SplitterPanel_Collapsed;

                if (Driver.FindElement(By.XPath("//div[@id='ServiceList']//table//tbody")).Displayed)
                {

                }
                else
                {
                    Driver.FindElement(ServicesSection_Element).Click();
                }

                Thread.Sleep(1000);
                var TableRows = Driver.FindElements(By.XPath("//div[@id='ServiceList']//table//tbody//tr"));

                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));
                    //look for the service type
                    if (td[1].Text.Contains(row.ItemArray[0].ToString()))
                    {
                        td[0].FindElement(By.TagName("a")).Click();
                        break;
                    }
                }

                wait.Until(x => x.FindElement(ServiceDetailsForm_Element));

                //click Assign button
                wait.Until(x => x.FindElement(ServiceDetailsAssignButton_Element));
                Driver.FindElement(ServiceDetailsAssignButton_Element).Click();
                Thread.Sleep(1000);

                wait.Until(x => x.FindElement(AssignCaseWorkerForm_Element));

                //enter First Name
                wait.Until(x => x.FindElement(FilterFirstNameInput_Element));
                Driver.FindElement(FilterFirstNameInput_Element).SendKeys(row.ItemArray[1].ToString());

                //enter Last Name
                wait.Until(x => x.FindElement(FilterLastNameInput_Element));
                Driver.FindElement(FilterLastNameInput_Element).SendKeys(row.ItemArray[2].ToString());

                //select from User Role dropdown
                wait.Until(x => x.FindElement(FilterUserRoleDropdown_Element));
                SelectElement userRole = new SelectElement(Driver.FindElement(FilterUserRoleDropdown_Element));
                userRole.SelectByText(row.ItemArray[3].ToString());

                //select from Specialty dropdown
                //wait.Until(x => x.FindElement(FilterSpecialtyDropdown_Element));
                //SelectElement specialty = new SelectElement(Driver.FindElement(FilterSpecialtyDropdown_Element));
                //specialty.SelectByText(row.ItemArray[4].ToString());

                //click Apply Filter
                wait.Until(x => x.FindElement(FilterApplyFilterButton_Element));
                Driver.FindElement(FilterApplyFilterButton_Element).Click();

                //click select
                wait.Until(x => x.FindElement(FilterSelectLink_Element));
                Driver.FindElement(FilterSelectLink_Element).Click();

                wait.Until(x => x.FindElement(ServiceDetailsForm_Element));

                //verify the message
                wait.Until(x => x.FindElement(ConfirmationMessage_Element));
                Assert.AreEqual("Case worker contact set\r\nCase worker contact was set successfully.", Driver.FindElement(ConfirmationMessage_Element).Text, "Case worker not set");

                //click Update Service button
                wait.Until(x => x.FindElement(UpdateButton_Element));
                Driver.FindElement(UpdateButton_Element).Click();

                //verify the message
                wait.Until(x => x.FindElement(ConfirmationMessage_Element));
                Assert.AreEqual("Service updated\r\nService was updated successfully.", Driver.FindElement(ConfirmationMessage_Element).Text, "Service not updated");

                //verify the case worker
                wait.Until(x => x.FindElement(CaseWorker_Element));
                Assert.AreEqual((row.ItemArray[1].ToString() + " " + row.ItemArray[2].ToString()), Driver.FindElement(CaseWorker_Element).GetAttribute("textContent"), "Incorrect Case Worker assigned");
                Thread.Sleep(2000);

                //click Return to case page button
                wait.Until(x => x.FindElement(CancelButton_Element));
                Driver.FindElement(CancelButton_Element).Click();

            }
        }
        internal void AssignCaseWorker(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ServiceDetailsForm_Element));

            //click Assign button
            wait.Until(x => x.FindElement(ServiceDetailsAssignButton_Element));
            Driver.FindElement(ServiceDetailsAssignButton_Element).Click();
            Thread.Sleep(1000);

            wait.Until(x => x.FindElement(AssignCaseWorkerForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //enter First Name
                wait.Until(x => x.FindElement(FilterFirstNameInput_Element));
                Driver.FindElement(FilterFirstNameInput_Element).SendKeys(row.ItemArray[0].ToString());

                //enter Last Name
                wait.Until(x => x.FindElement(FilterLastNameInput_Element));
                Driver.FindElement(FilterLastNameInput_Element).SendKeys(row.ItemArray[1].ToString());

                //select from User Role dropdown
                wait.Until(x => x.FindElement(FilterUserRoleDropdown_Element));
                SelectElement userRole = new SelectElement(Driver.FindElement(FilterUserRoleDropdown_Element));
                userRole.SelectByText(row.ItemArray[2].ToString());

                //select from Specialty dropdown
                wait.Until(x => x.FindElement(FilterSpecialtyDropdown_Element));
                SelectElement specialty = new SelectElement(Driver.FindElement(FilterSpecialtyDropdown_Element));
                specialty.SelectByText(row.ItemArray[3].ToString());

                //click Apply Filter
                wait.Until(x => x.FindElement(FilterApplyFilterButton_Element));
                Driver.FindElement(FilterApplyFilterButton_Element).Click();

                //click select
                wait.Until(x => x.FindElement(FilterSelectLink_Element));
                Driver.FindElement(FilterSelectLink_Element).Click();

                wait.Until(x => x.FindElement(ServiceDetailsForm_Element));

                //verify the message
                wait.Until(x => x.FindElement(ConfirmationMessage_Element));
                Assert.AreEqual("Case worker contact set\r\nCase worker contact was set successfully.", Driver.FindElement(ConfirmationMessage_Element).Text, "Case worker not set");

                //click Update Service button
                wait.Until(x => x.FindElement(UpdateButton_Element));
                Driver.FindElement(UpdateButton_Element).Click();

                //verify the message
                wait.Until(x => x.FindElement(ConfirmationMessage_Element));
                Assert.AreEqual("Service updated\r\nService was updated successfully.", Driver.FindElement(ConfirmationMessage_Element).Text, "Service not updated");

                //verify the case worker
                wait.Until(x => x.FindElement(CaseWorker_Element));
                Assert.AreEqual((row.ItemArray[0].ToString() + " " + row.ItemArray[1].ToString()), Driver.FindElement(CaseWorker_Element).GetAttribute("textContent"), "Incorrect Case Worker assigned");
                Thread.Sleep(2000);
            }
        }
        internal void CreateServiceNote(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ServiceDetailsForm_Element));

            //click Add Note
            bool bNoLink;
            try
            {
                if (Driver.FindElement(CaseNoteLink_Element).Displayed)
                {

                }
                bNoLink = true;
            }
            catch
            {
                bNoLink = false;
            }

            if (bNoLink == true)
            {
                wait.Until(x => x.FindElement(CaseNoteLink_Element));
                Driver.FindElement(CaseNoteLink_Element).Click();
                Thread.Sleep(1000);
            }
            else
            {
                wait.Until(x => x.FindElement(CaseNoteAdd_Element));
                Driver.FindElement(CaseNoteAdd_Element).Click();
                Thread.Sleep(1000);
            }


            wait.Until(x => x.FindElement(CaseNoteForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //random generator/input details for Case Note Title
                wait.Until(x => x.FindElement(CaseNoteTitleInput_Element));

                Random generatorTitle = new Random();
                int iTitle = generatorTitle.Next(1, 10000000);
                string sTitle = iTitle.ToString().PadLeft(7, '0');
                string eTitle = "Test Title_" + DateTime.Now.ToString() + "_" + sTitle;
                Driver.FindElement(CaseNoteTitleInput_Element).SendKeys(eTitle);

                //click Event Date
                wait.Until(x => x.FindElement(CaseNoteEventDate_Element));
                Driver.FindElement(CaseNoteEventDate_Element).Click();
                Thread.Sleep(1000);

                //Set today's date for Event Date
                wait.Until(x => x.FindElement(CaseNoteEventDate_Element));
                Driver.FindElement(CaseNoteEventDate_Today_Element).Click();
                Thread.Sleep(1000);

                //select from Event type dropdown
                wait.Until(x => x.FindElement(CaseNoteEventTypeDropdown_Element));
                SelectElement eventType = new SelectElement(Driver.FindElement(CaseNoteEventTypeDropdown_Element));
                eventType.SelectByText(row.ItemArray[0].ToString());

                //select from Service Deliver Time Hours dropdown
                wait.Until(x => x.FindElement(CaseNoteEventServiceDelHrsDropdown_Element));
                Driver.FindElement(CaseNoteEventTypeDropdown_Element).Click();
                SelectElement sdHours = new SelectElement(Driver.FindElement(CaseNoteEventServiceDelHrsDropdown_Element));
                sdHours.SelectByText(row.ItemArray[1].ToString());

                //select from Service Deliver Time Minutes dropdown
                wait.Until(x => x.FindElement(CaseNoteEventServiceDelMinDropdown_Element));
                SelectElement sdMins = new SelectElement(Driver.FindElement(CaseNoteEventServiceDelMinDropdown_Element));
                sdMins.SelectByText(row.ItemArray[2].ToString());

                //select from Travel Time Hours dropdown
                wait.Until(x => x.FindElement(CaseNoteEventTravelHrsDropdown_Element));
                SelectElement travelHours = new SelectElement(Driver.FindElement(CaseNoteEventTravelHrsDropdown_Element));
                travelHours.SelectByText(row.ItemArray[3].ToString());

                //select from Travel Time Minutes dropdown
                wait.Until(x => x.FindElement(CaseNoteEventTravelMinDropdown_Element));
                SelectElement travelMins = new SelectElement(Driver.FindElement(CaseNoteEventTravelMinDropdown_Element));
                travelMins.SelectByText(row.ItemArray[4].ToString());

                //random generator/input details for Notes
                wait.Until(x => x.FindElement(CaseNote_NoteInput_Element));

                Random generatorNotes = new Random();
                int iNotes = generatorNotes.Next(1, 10000000);
                string sNotes = iNotes.ToString().PadLeft(7, '0');
                string eNotes = "Test Notes_" + DateTime.Now.ToString() + "_" + sNotes;
                Driver.FindElement(CaseNote_NoteInput_Element).SendKeys(eNotes);

                /*//click Choose file button
                var winHandleBefore = Driver.CurrentWindowHandle;
                wait.Until(x => x.FindElement(ChooseFileButton_Element));
                Driver.FindElement(ChooseFileButton_Element).Click();

                //Switch to upload window
                Driver.SwitchTo().Window(Driver.WindowHandles.Last());
                //Uploading the file using keyboard actions
                Thread.Sleep(2000);

                var fileNamePath = CommonFunctions.GetResourceValue("FileUpload_ConsentForm");
                SendKeys.SendWait(fileNamePath);
                SendKeys.SendWait(@"{Enter}");
                Thread.Sleep(5000);

                Driver.FindElement(CaseNoteUploadButton_Element).Click();*/

                //click Add/Update Case Note button
                wait.Until(x => x.FindElement(UpdateButton_Element));
                Driver.FindElement(UpdateButton_Element).Click();

                //verify the message
                wait.Until(x => x.FindElement(ConfirmationMessage_Element));
                Assert.AreEqual("SSS Case Note added\r\nSSS Case Note was added successfully.", Driver.FindElement(ConfirmationMessage_Element).Text, "Case Note not updated");

                //click Cancel button to go back to Service
                wait.Until(x => x.FindElement(CancelButton_Element));
                Driver.FindElement(CancelButton_Element).Click();

                wait.Until(x => x.FindElement(ServiceDetailsForm_Element));

                //verify added service note
                var TableRows = Driver.FindElements(By.XPath("//div[@id='TaskList']//table//tbody//tr"));

                int matchedCaseNote = 0;
                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));

                    //look for the subject
                    if (td[1].Text.Equals(eTitle.ToString()))
                    {
                        matchedCaseNote = matchedCaseNote + 1;
                        break;
                    }

                }
                //verify the case note created
                Assert.AreEqual(1, matchedCaseNote, "No Service Note created");
                Thread.Sleep(2000);
            }
        }
        internal void AddNoteandSave(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            //click Add Case Note button
            wait.Until(x => x.FindElement(AddCaseNoteButton_Element));
            Driver.FindElement(AddCaseNoteButton_Element).Click();
            Thread.Sleep(1000);

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Duration Hours dropdown
                wait.Until(x => x.FindElement(SCN_DurationHrsDropdown_Element));
                SelectElement durationHours = new SelectElement(Driver.FindElement(SCN_DurationHrsDropdown_Element));
                durationHours.SelectByText(row.ItemArray[0].ToString());

                //select from Duration Minutes dropdown
                wait.Until(x => x.FindElement(SCN_DurationMinDropdown_Element));
                SelectElement durationMins = new SelectElement(Driver.FindElement(SCN_DurationMinDropdown_Element));
                durationMins.SelectByText(row.ItemArray[1].ToString());

                //random generator/input details for Student Case Note Subject
                wait.Until(x => x.FindElement(SCN_SubjectInput_Element));

                Random generatorSubject = new Random();
                int iSubject = generatorSubject.Next(1, 10000000);
                string sSubject = iSubject.ToString().PadLeft(7, '0');
                string eSubject = "Test Subject_" + DateTime.Now.ToString() + "_" + sSubject;
                Driver.FindElement(SCN_SubjectInput_Element).SendKeys(eSubject);

                //select from School Action dropdown
                wait.Until(x => x.FindElement(SCN_SchoolActionDropdown_Element));
                SelectElement schoolAction = new SelectElement(Driver.FindElement(SCN_SchoolActionDropdown_Element));
                schoolAction.SelectByText(row.ItemArray[2].ToString());

                //random generator/input details for Student Case Note Notes
                wait.Until(x => x.FindElement(SCN_NotesInput_Element));

                Random generatorNotes = new Random();
                int iNotes = generatorNotes.Next(1, 10000000);
                string sNotes = iNotes.ToString().PadLeft(7, '0');
                string eNotes = "Test Notes_" + DateTime.Now.ToString() + "_" + sNotes;
                Driver.FindElement(SCN_NotesInput_Element).SendKeys(eNotes);

                //click Save Case Note button
                wait.Until(x => x.FindElement(SCN_SaveCaseNoteButton_Element));
                Driver.FindElement(SCN_SaveCaseNoteButton_Element).Click();

                //verify the message
                wait.Until(x => x.FindElement(ConfirmationMessage_Element));
                Assert.AreEqual("Student Case Note Created\r\nStudent case note was successfully created. View this and historical notes in the Student Case Note panel", Driver.FindElement(ConfirmationMessage_Element).Text, "Student Case Note not created");

                //click Student Case Note section
                wait.Until(x => x.FindElement(SCN_StudentCaseNoteSection_Element));
                Driver.FindElement(SCN_StudentCaseNoteSection_Element).Click();

                //wait.Until(x => x.FindElement(SCN_StudentCaseNoteSectionGrid_Element));
                Thread.Sleep(5000);

                //verify added student case note
                var TableRows = Driver.FindElements(By.XPath("//div[@id='SchoolCaseNotesGrid']//table//tbody//tr"));

                int matchedCaseNote = 0;
                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));

                    //look for the subject
                    if (td[2].Text.Equals(eSubject.ToString()))
                    {
                        matchedCaseNote = matchedCaseNote + 1;
                        break;
                    }

                }
                Thread.Sleep(5000);
                //verify the case note created
                Assert.AreEqual(1, matchedCaseNote, "No Student Case Note created");
                Thread.Sleep(2000);
            }
            //click Update School Case button
            wait.Until(x => x.FindElement(UpdateSchoolCaseButton_Element));
            Driver.FindElement(UpdateSchoolCaseButton_Element).Click();

            //verify the message
            wait.Until(x => x.FindElement(ConfirmationMessage_Element));
            Assert.AreEqual("Case updated\r\nCase was updated successfully.", Driver.FindElement(ConfirmationMessage_Element).Text, "Student Case Note not updated");
            Thread.Sleep(2000);
        }

        internal void CloseService(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ServiceDetailsForm_Element));


            //click Close Case Plan button
            wait.Until(x => x.FindElement(ServiceCloseCasePlan_Element));
            Driver.FindElement(ServiceCloseCasePlan_Element).Click();
            Thread.Sleep(1000);

            wait.Until(x => x.FindElement(ReviewQuestionsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Intervention Outcomes dropdown
                wait.Until(x => x.FindElement(InterventionOutcomesDropdown_Element));
                SelectElement interventionOutcomes = new SelectElement(Driver.FindElement(InterventionOutcomesDropdown_Element));
                interventionOutcomes.SelectByText(row.ItemArray[0].ToString());

                //select from Intervention Aims dropdown
                wait.Until(x => x.FindElement(InterventionAimsDropdown_Element));
                SelectElement interventionAims = new SelectElement(Driver.FindElement(InterventionAimsDropdown_Element));
                interventionAims.SelectByText(row.ItemArray[1].ToString());

                //click Save button for Review Questions
                wait.Until(x => x.FindElement(UpdateButton_Element));
                Driver.FindElement(UpdateButton_Element).Click();

                wait.Until(x => x.FindElement(ServiceDetailsClosedDates_Element));
                Assert.IsNotNull(ServiceDetailsClosedDates_Element, "Service not yet closed");
                Thread.Sleep(2000);
            }
            //click Return to Case page button
            wait.Until(x => x.FindElement(CancelButton_Element));
            Driver.FindElement(CancelButton_Element).Click();
        }
        internal void SelectServicesClose(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            //click Services Section
            wait.Until(x => x.FindElement(ServicesSection_Element));
            Driver.FindElement(ServicesSection_Element).Click();
            Thread.Sleep(1000);

            wait.Until(x => x.FindElement(ServicesForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //SplitContainer servicesSection = new SplitContainer();
                //((ServicesSection_Element)servicesSection.Panel1);
                //((ServicesSection_Element)servicesSection.Panel1);
                //var serviceSection = SplitterPanel(Driver.FindElement(ServicesSection_Element));

                //SplitContainer servicesSection = new SplitContainer();
                //servicesSection.Panel1.VisibleChanged += SplitterPanel_Collapsed;
                //(Driver.FindElement(ServicesSection_Element))
                //((ServicesSection_Element)servicesSection.Panel1).VisibleChanged += SplitterPanel_Collapsed;

                if (Driver.FindElement(By.XPath("//div[@id='ServiceList']//table//tbody")).Displayed)
                {

                }
                else
                {
                    Driver.FindElement(ServicesSection_Element).Click();
                }
                Thread.Sleep(1000);
                var TableRows = Driver.FindElements(By.XPath("//div[@id='ServiceList']//table//tbody//tr"));

                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));
                    //look for the service type
                    if (td[1].Text.Contains(row.ItemArray[0].ToString()))
                    {
                        td[0].FindElement(By.TagName("a")).Click();
                        break;
                    }
                }

                wait.Until(x => x.FindElement(ServiceDetailsForm_Element));

                //click Close Case Plan button
                wait.Until(x => x.FindElement(ServiceCloseCasePlan_Element));
                Driver.FindElement(ServiceCloseCasePlan_Element).Click();
                Thread.Sleep(1000);

                wait.Until(x => x.FindElement(ReviewQuestionsForm_Element));

                //select from Intervention Outcomes dropdown
                wait.Until(x => x.FindElement(InterventionOutcomesDropdown_Element));
                SelectElement interventionOutcomes = new SelectElement(Driver.FindElement(InterventionOutcomesDropdown_Element));
                interventionOutcomes.SelectByText(row.ItemArray[1].ToString());

                //select from Intervention Aims dropdown
                wait.Until(x => x.FindElement(InterventionAimsDropdown_Element));
                SelectElement interventionAims = new SelectElement(Driver.FindElement(InterventionAimsDropdown_Element));
                interventionAims.SelectByText(row.ItemArray[2].ToString());

                //click Save button for Review Questions
                wait.Until(x => x.FindElement(UpdateButton_Element));
                Driver.FindElement(UpdateButton_Element).Click();

                wait.Until(x => x.FindElement(ServiceDetailsClosedDates_Element));
                Assert.IsNotNull(ServiceDetailsClosedDates_Element, "Service not yet closed");
                Thread.Sleep(2000);

                //click Return to case page button
                wait.Until(x => x.FindElement(CancelButton_Element));
                Driver.FindElement(CancelButton_Element).Click();

            }
        }
        internal void CloseCase()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(SchoolCaseForm_Element));
            
            //click Close Case
            wait.Until(x => x.FindElement(CloseCasebutton_Element));
            Driver.FindElement(CloseCasebutton_Element).Click();
            Thread.Sleep(1000);

            //random generator/input details for Reason for closure and details of alternative strategy where appropriate
            wait.Until(x => x.FindElement(CloseCaseCommentInput_Element));

            Random generatorReason = new Random();
            int iReason = generatorReason.Next(1, 10000000);
            string sReason = iReason.ToString().PadLeft(7, '0');
            string eReason = "Test Close Reason_" + DateTime.Now.ToString() + "_" + sReason;
            Driver.FindElement(CloseCaseCommentInput_Element).SendKeys(eReason);

            //click Close Case confirmation
            wait.Until(x => x.FindElement(CloseCaseCommitButton_Element));
            Driver.FindElement(CloseCaseCommitButton_Element).Click();
            Thread.Sleep(1000);

            //verify the message
            wait.Until(x => x.FindElement(ConfirmationMessage_Element));
            Assert.AreEqual("Case closed\r\nCase was closed successfully.", Driver.FindElement(ConfirmationMessage_Element).Text, "Case not closed");
            Thread.Sleep(2000);

            //click CaseDetailsSection
            wait.Until(x => x.FindElement(CaseDetailsSection_Element));
            Driver.FindElement(CaseDetailsSection_Element).Click();
            Thread.Sleep(1000);

            //verify the status
            wait.Until(x => x.FindElement(CaseStatus_Element));
            Assert.AreEqual("Completed", Driver.FindElement(CaseStatus_Element).Text, "Case not closed-incorrect status");
            Thread.Sleep(2000);
        }
        internal void ReturnCaseAction(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select from Area Team case dropdown
                wait.Until(x => x.FindElement(PriorityDropdown_Element));
                SelectElement priority = new SelectElement(Driver.FindElement(PriorityDropdown_Element));
                priority.SelectByText(row.ItemArray[0].ToString());

                //click Action Section
                wait.Until(x => x.FindElement(ActionSection_Element));
                Driver.FindElement(ActionSection_Element).Click();
                Thread.Sleep(1000);

                //select from Action dropdown
                wait.Until(x => x.FindElement(ActionDropdown_Element));
                SelectElement action = new SelectElement(Driver.FindElement(ActionDropdown_Element));
                action.SelectByText(row.ItemArray[1].ToString());

                //random generator/input details for Return reason
                wait.Until(x => x.FindElement(ReturnCaseCommentInput_Element));

                Random generatorReason = new Random();
                int iReason = generatorReason.Next(1, 10000000);
                string sReason = iReason.ToString().PadLeft(7, '0');
                string eReason = "Test Return Reason_" + DateTime.Now.ToString() + "_" + sReason;
                Driver.FindElement(ReturnCaseCommentInput_Element).SendKeys(eReason);

                //click Save School Case button
                wait.Until(x => x.FindElement(UpdateSchoolCaseButton_Element));
                Driver.FindElement(UpdateSchoolCaseButton_Element).Click();

                //Current Active Referral
                //verify if the case was returned or not
                wait.Until(x => x.FindElement(CurrentActiveReferralForm_Element));
                Assert.AreEqual("Current active referral", Driver.FindElement(CurrentActiveReferralForm_Element).Text);
                Thread.Sleep(2000);

                //verify the message
                wait.Until(x => x.FindElement(CurrentActiveReferralMessage_Element));
                Assert.AreEqual("This student already has a case in returned status. Please contact your Area Team/Sub-Region Coordinator to update/close this before submitting a new case.", Driver.FindElement(CurrentActiveReferralMessage_Element).Text);
                Thread.Sleep(2000);
            }
        }
        internal void ReturnCase()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(SchoolCaseForm_Element));

            //click Return Case button
            wait.Until(x => x.FindElement(ReturnCasebutton_Element));
            Driver.FindElement(ReturnCasebutton_Element).Click();
            Thread.Sleep(1000);

            //random generator/input details for Return reason
            wait.Until(x => x.FindElement(ReturnCaseCommentInput_Element));

            Random generatorReason = new Random();
            int iReason = generatorReason.Next(1, 10000000);
            string sReason = iReason.ToString().PadLeft(7, '0');
            string eReason = "Test Return Reason_" + DateTime.Now.ToString() + "_" + sReason;
            Driver.FindElement(ReturnCaseCommentInput_Element).SendKeys(eReason);

            //click Return Case confirmation
            wait.Until(x => x.FindElement(ReturnCaseCommitButton_Element));
            Driver.FindElement(ReturnCaseCommitButton_Element).Click();
            Thread.Sleep(1000);

            //verify the message
            wait.Until(x => x.FindElement(ConfirmationMessage_Element));
            Assert.AreEqual("Case returned\r\nCase was successfully returned.", Driver.FindElement(ConfirmationMessage_Element).Text, "Case not returned");
            Thread.Sleep(2000);

            //verify the status
            wait.Until(x => x.FindElement(CaseStatus_Element));
            Assert.AreEqual("Returned", Driver.FindElement(CaseStatus_Element).Text, "Case not returned-incorrect status");
            Thread.Sleep(2000);
        }

        internal void ReopenCase()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(SchoolCaseForm_Element));

            //click Reopen Case button
            wait.Until(x => x.FindElement(ReopenCasebutton_Element));
            Driver.FindElement(ReopenCasebutton_Element).Click();
            Thread.Sleep(1000);

            //verify the message
            wait.Until(x => x.FindElement(ConfirmationMessage_Element));
            Assert.AreEqual("Case re-opened\r\nCase was successfully re-opened.", Driver.FindElement(ConfirmationMessage_Element).Text, "Case not reopened");
            Thread.Sleep(2000);

            //verify the status
            wait.Until(x => x.FindElement(CaseStatus_Element));
            Assert.AreEqual("Submitted", Driver.FindElement(CaseStatus_Element).Text, "Case not reopened-incorrect status");
            Thread.Sleep(2000);

        }
        internal void CaseActivitiesCompleted()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(SchoolCaseForm_Element));

            //click Case Details Section
            wait.Until(x => x.FindElement(CaseDetailsSection_Element));
            Driver.FindElement(CaseDetailsSection_Element).Click();
            Thread.Sleep(1000);

            //click "I have completed activities" checkbox
            wait.Until(x => x.FindElement(HaveCompletedCheckbox_Element));
            Driver.FindElement(HaveCompletedCheckbox_Element).Click();
            Thread.Sleep(1000);
        }
        internal void SaveCase()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(SchoolCaseForm_Element));

            //click Save Case button
            wait.Until(x => x.FindElement(UpdateSchoolCaseButton_Element));
            Driver.FindElement(UpdateSchoolCaseButton_Element).Click();

            //verify the message
            wait.Until(x => x.FindElement(SchoolCaseHeading_Element));
            Assert.AreEqual("Case updated", Driver.FindElement(SchoolCaseHeading_Element).Text, "Case not updated");
            Thread.Sleep(2000);

            //click Case Details Section
            //wait.Until(x => x.FindElement(CaseDetailsSection_Element));
            //Driver.FindElement(CaseDetailsSection_Element).Click();
            //Thread.Sleep(1000);

            //verify the status
            //wait.Until(x => x.FindElement(CaseStatus_Element));
            //Assert.AreEqual("Assigned", Driver.FindElement(CaseStatus_Element).Text, "Case not updated-incorrect status");
            //Thread.Sleep(2000);
        }
        internal void UpdatePriority(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));
            
            //click Case Details Section
            wait.Until(x => x.FindElement(CaseDetailsSection_Element));
            Driver.FindElement(CaseDetailsSection_Element).Click();
            Thread.Sleep(1000);

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //Update Area Team case priority
                wait.Until(x => x.FindElement(PriorityDropdown_Element));
                SelectElement priority = new SelectElement(Driver.FindElement(PriorityDropdown_Element));
                priority.SelectByText(row.ItemArray[0].ToString());
            }
        }
        internal void UpdateCasePlan(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            wait.Until(x => x.FindElement(ServiceDetailsForm_Element));

            //click Service Review Date
            wait.Until(x => x.FindElement(ServiceReviewDate_Element));
            Driver.FindElement(ServiceReviewDate_Element).Click();
            Thread.Sleep(1000);

            //Set today's date for Event Date
            wait.Until(x => x.FindElement(ServiceReviewDate_Today_Element));
            Driver.FindElement(ServiceReviewDate_Today_Element).Click();
            Thread.Sleep(1000);

            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //Select value in InterventionsServiceType
                wait.Until(x => x.FindElement(InterventionServiceTypeDropdown_Element));
                SelectElement intervention = new SelectElement(Driver.FindElement(InterventionServiceTypeDropdown_Element));
                intervention.SelectByText(row.ItemArray[0].ToString());
            }
            //click Update Service button
            wait.Until(x => x.FindElement(UpdateButton_Element));
            Driver.FindElement(UpdateButton_Element).Click();

            //verify the message
            wait.Until(x => x.FindElement(ConfirmationMessage_Element));
            Assert.AreEqual("Service updated\r\nService was updated successfully.", Driver.FindElement(ConfirmationMessage_Element).Text, "Service not updated");
        }
        internal void AmendSpecialist(Table table)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            wait.Until(x => x.FindElement(StudentCaseDetailsForm_Element));

            //click Case Details Section
            //wait.Until(x => x.FindElement(CaseDetailsSection_Element));
            //Driver.FindElement(CaseDetailsSection_Element).Click();

            //click Amend Specialist Requested button
            wait.Until(x => x.FindElement(AmendSpecialistRequestedButton_Element));
            Driver.FindElement(AmendSpecialistRequestedButton_Element).Click();

            //select specialist
            var dataTable = TableExtensions.ToDataTable(table);
            string numRows = dataTable.Rows.ToString();
            foreach (DataRow row in dataTable.Rows)
            {
                //select/unselect for Psychologist
                wait.Until(x => x.FindElement(Specialist_AmendPsychologist_Checkbox_Element));
                if (Driver.FindElement(Specialist_AmendPsychologist_Checkbox_Element).Selected)
                {
                    if (row.ItemArray[0].ToString().Equals("checked"))
                    {

                    }
                    else
                    {
                        Driver.FindElement(Specialist_AmendPsychologist_Checkbox_Element).Click();
                    }
                }
                else
                {
                    if (row.ItemArray[0].ToString().Equals("checked"))
                    {
                        Driver.FindElement(Specialist_AmendPsychologist_Checkbox_Element).Click();
                    }
                    else
                    {
                        
                    }
                }

                //select/unselect for Speech Pathologist
                wait.Until(x => x.FindElement(Specialist_AmendedSpeechPathologist_Checkbox_Element));
                if (Driver.FindElement(Specialist_AmendedSpeechPathologist_Checkbox_Element).Selected)
                {
                    if (row.ItemArray[1].ToString().Equals("checked"))
                    {

                    }
                    else
                    {
                        Driver.FindElement(Specialist_AmendedSpeechPathologist_Checkbox_Element).Click();
                    }
                }
                else
                {
                    if (row.ItemArray[1].ToString().Equals("checked"))
                    {
                        Driver.FindElement(Specialist_AmendedSpeechPathologist_Checkbox_Element).Click();
                    }
                    else
                    {

                    }
                }

                //select/unselect for Social Worker
                wait.Until(x => x.FindElement(Specialist_AmendSocialWorker_Checkbox_Element));
                if (Driver.FindElement(Specialist_AmendSocialWorker_Checkbox_Element).Selected)
                {
                    if (row.ItemArray[2].ToString().Equals("checked"))
                    {

                    }
                    else
                    {
                        Driver.FindElement(Specialist_AmendSocialWorker_Checkbox_Element).Click();
                    }
                }
                else
                {
                    if (row.ItemArray[2].ToString().Equals("checked"))
                    {
                        Driver.FindElement(Specialist_AmendSocialWorker_Checkbox_Element).Click();
                    }
                    else
                    {

                    }
                }

                //select/unselect for Visiting Teacher
                wait.Until(x => x.FindElement(Specialist_AmendVisitingTeacher_Checkbox_Element));
                if (Driver.FindElement(Specialist_AmendVisitingTeacher_Checkbox_Element).Selected)
                {
                    if (row.ItemArray[3].ToString().Equals("checked"))
                    {

                    }
                    else
                    {
                        Driver.FindElement(Specialist_AmendVisitingTeacher_Checkbox_Element).Click();
                    }
                }
                else
                {
                    if (row.ItemArray[3].ToString().Equals("checked"))
                    {
                        Driver.FindElement(Specialist_AmendVisitingTeacher_Checkbox_Element).Click();
                    }
                    else
                    {

                    }
                }

                //select/unselect for Other
                wait.Until(x => x.FindElement(Specialist_AmendOther_Checkbox_Element));
                if (Driver.FindElement(Specialist_AmendOther_Checkbox_Element).Selected)
                {
                    if (row.ItemArray[4].ToString().Equals("checked"))
                    {

                    }
                    else
                    {
                        Driver.FindElement(Specialist_AmendOther_Checkbox_Element).Click();
                    }
                }
                else
                {
                    if (row.ItemArray[4].ToString().Equals("checked"))
                    {
                        Driver.FindElement(Specialist_AmendOther_Checkbox_Element).Click();
                    }
                    else
                    {

                    }
                }

                //select/unselect for SSSO with attendance or behaviour expertise
                wait.Until(x => x.FindElement(Specialist_AmendSSSOExpert_Checkbox_Element));
                if (Driver.FindElement(Specialist_AmendSSSOExpert_Checkbox_Element).Selected)
                {
                    if (row.ItemArray[5].ToString().Equals("checked"))
                    {

                    }
                    else
                    {
                        Driver.FindElement(Specialist_AmendSSSOExpert_Checkbox_Element).Click();
                    }
                }
                else
                {
                    if (row.ItemArray[5].ToString().Equals("checked"))
                    {
                        Driver.FindElement(Specialist_AmendSSSOExpert_Checkbox_Element).Click();
                    }
                    else
                    {

                    }
                }
            }

        }
    }
}
