﻿using SOCS_SmokeTest.Data;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace SOCS_SmokeTest.UtilityClasses
{
    class CommonFunctions
    {
        public static string GetResourceValue(string ResourceName)
        {
            //Using URL variable from resources file
            string value = Resources.ResourceManager.GetString(ResourceName);
            return value;
        }
        public static void GetUrl(IWebDriver Driver, string url)
        {
            //Thread.Sleep(5000);
            string urlValue = GetResourceValue(url);
            Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(80);
            Driver.Navigate().GoToUrl(urlValue);
        }

        /*This function will be used to enter text to any textfield*/
        public static void EnterText(IWebElement element, string text)
        {
            //string enterText = GetResourceValue(text);
            element.Click();
            element.Clear();
            element.SendKeys(text);

        }
        //Function to click any element or button
        internal static void GetClick(IWebElement element)
        {
            try
            {
                Thread.Sleep(2);
                if (element.Displayed)
                {
                    element.Click();
                    Thread.Sleep(5);
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        /* internal static void EnterText(object searchBox_Element, string addedNewService)
         {
             throw new NotImplementedException();
         }*/

        internal static void ClearCache(IWebDriver Driver)
        {
            //Driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
            //Thread.Sleep(5000);
            //Driver.SwitchTo().ActiveElement();
            // Thread.Sleep(5000);
            //Driver.FindElement(By.CssSelector("* /deep/ #clearBrowsingDataConfirm")).Click();
            //Thread.Sleep(5000);
            // begin identify clear data button via nested Shadow Dom elements
            // get 1st parent
            //ChromeOptions options = new ChromeOptions();
            //ChromeNetworkConditions.
            // options.pr

        }
        internal static void ExplicitWait(IWebDriver Driver, By element)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(element));
        }
        public static bool IsElementDisplayed(IWebDriver driver, By element)
        {
            if (driver.FindElements(element).Count > 0)
            {
                if (driver.FindElement(element).Displayed)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
    }
}
