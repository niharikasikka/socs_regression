﻿Feature: TestJourney27_E2E_SSSRequest_CreateSubmitThenClose(button)

##############################################################################################################
##
## This feature file covers the following:
## 01_SchoolPrincipal_Create_and_Submit_a_SSSRequest_Behavior
## 02_SSSTeamLeader_Closing_a_Case
##
## Things to do:
## Modify the Student First Name - ensure that no existing submitted/active case for this student
## Update the FileUpload_ConsentForm in SOCS_SmokeTest>Data>Resources.resx
## Build/Rebuild
##
## NOTE(s):
## below scenarios are created for continuity/dependency of each other
## integrated jobs may affect SOCS performance hence may affect automation script as well
##
##############################################################################################################

@SmokeTest
Scenario: Create>Submit>Close(usingbutton)
	##01_SchoolPrincipal_Create_and_Submit_a_SSSRequest_Behavior
	Given I navigate to SOCS application homepage
	When I login using SchoolPrincipal username and password
	And I press login button
	Then I select Create Student Support Services Request from the Case menu
	And I select student for SSS Request
	| Student First Name |
	| FSN010050190       |
	And I fill in Student Support Services request details
	| Request Type       | Primary Presenting Issue |
	| Individual student | Behaviour                |
	And I will get the SSS Request case number
	Then I click Cases menu
	And I filter and select by newly created SSS Request case number
	And I attach Consent Form for SSS Request
	And I save and submit existing SSS Request
	And I logout of SOCS application
	And I close the browser successfully
	##02_SSSTeamLeader_Closing_a_Case
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button
	Then I click Cases menu
	And I filter and select by newly created SSS Request case number
	And I close the SSS request case
	And I logout of SOCS application
	And I close the browser successfully