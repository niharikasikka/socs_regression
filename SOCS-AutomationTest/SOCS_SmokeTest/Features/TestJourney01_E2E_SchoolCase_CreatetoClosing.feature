﻿Feature: TestJourney01_E2E_SchoolCase_CreatetoClosing
##############################################################################################################
##
## This feature file covers the following:
## 01_SchoolPrincipal_Create_and_Submit_a_School_Case_Curriculum/learning
## 02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
## 03_CaseLeader-SSS_Team_Member_Assigning_a_CaseWorker-SSS_Team_Member_to_a_Service
## 04_CaseLeader-SSS_Team_Member_Closing_a_Case
##
## Things to do:
## Modify the Student First Name - ensure that no existing submitted/active case for this student
## Update the FileUpload_ConsentForm in SOCS_SmokeTest>Data>Resources.resx
## Build/Rebuild
##
## NOTE(s):
## below scenarios are created for continuity/dependency of each other
## integrated jobs may affect SOCS performance hence may affect automation script as well
##
##############################################################################################################

@SmokeTest
Scenario: Create>Submit>AssignCaseLeader>AssignCaseWorker>Close
	##01_SchoolPrincipal_Create_and_Submit_a_School_Case_Curriculum/learning
	Given I navigate to SOCS application homepage
	When I login using SchoolPrincipal username and password
	And I press login button
	Then I click School Cases submenu
	And I select student
	| Student First Name |
	| FSN010050155       |
	And I fill in case details
	| Request Type       | Primary Presenting Issue |
	| Individual student | Curriculum/learning      | 
	And I will get the case number
	Then I click Cases menu
	And I filter and select by newly created case number
	And I attach consent form
	And I submit existing student case
	And I logout of SOCS application
	And I close the browser successfully
	##02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created case number
	And I made some action
	| Priority   | Action               | Recommendations                                 |
	| Priority 1 | Assign a Case Leader | The school agrees on an internal school program |
	And I assign a case leader
	| FirstName           | LastName | UserRole         | Specialty    |
	| SOCS SSSStaffMember | Test     | SSS Staff Member | Psychologist |
	And I logout of SOCS application
	And I close the browser successfully
	##03_CaseLeader-SSS_Team_Member_Assigning_a_CaseWorker-SSS_Team_Member_to_a_Service
	Given I navigate to SOCS application homepage
	When I login using SSSStaffMember username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created case number
	And I select Service
	| Service type         |
	| Learning/Development |
	And I assign a case worker
	| FirstName           | LastName | UserRole         | Specialty    |
	| SOCS SSSStaffMember | Test     | SSS Staff Member | Psychologist |
	And I logout of SOCS application
	And I close the browser successfully
	##04_CaseLeader-SSS_Team_Member_Closing_a_Case
	Given I navigate to SOCS application homepage
	When I login using SSSStaffMember username and password
	And I press login button
	Then I click Cases menu
	And I filter and select by newly created case number
	And I select Service
	| Service type         |
	| Learning/Development |
	And I close the service
	| InterventionOutcomes | InterventionAims         |
	| Issue is resolved    | Been completely achieved |
	And I close the case
	And I logout of SOCS application
	And I close the browser successfully