﻿Feature: TestJourney32_E2E_ProgramRequest_CreatetoCompleteActivities_EducationalNeeds
##############################################################################################################
##
## This feature file covers the following:
## 01_SchoolPrincipal_Create_and_Submit_a_School_Case_EducationalNeedsAssessment
## 02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
## 03_CaseLeader-SSS_Team_Member_Assigning_a_CaseWorker-SSS_Team_Member_to_a_Service
## 04_CaseLeader-SSS_Team_Member_CompleteActivities
##
## Things to do:
## Modify the Student First Name - ensure that no existing submitted/active case for this student
## Update the FileUpload_ConsentForm in SOCS_SmokeTest>Data>Resources.resx
## Build/Rebuild
##
## NOTE(s):
## below scenarios are created for continuity/dependency of each other
## integrated jobs may affect SOCS performance hence may affect automation script as well
##
##############################################################################################################

@SmokeTest
Scenario: Create>Submit>AssignCaseLeader>AssignCaseWorker>CompleteActivities>EducationalNeedsAssessment
	##01_SchoolPrincipal_Create_and_Submit_a_Program_Request_EducationalNeedsAssessment
	Given I navigate to SOCS application homepage
	When I login using SchoolPrincipal username and password
	And I press login button
	Then I click Create Program Request submenu
	And I fill in program request details - Educational Needs Assessment
	| Primary Presenting Issue            |
	| Educational Needs Assessment |  
	And I select students for the program request
	| Student First Name |
	| FSN010050208       |
	And I will get the program case number
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	And I add/save case note for Program Request 
	| DurationHours | DurationMinutes | School Action                      |
	| 1             | 00              | Complete a school based assessment |
	And I save the changes in Program Request
	And I attach consent form for Program Request 
	And I save and submit existing Program Request
	And I logout of SOCS application
	And I close the browser successfully
	##02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	And I made some action for program request
	| Priority   | Action               | Recommendations                                 |
	| Priority 1 | Assign a Case Leader | The school agrees on an internal school program |
	And I assign a case leader
	| FirstName           | LastName | UserRole         | Specialty    |
	| SOCS SSSStaffMember | Test     | SSS Staff Member | Psychologist |
	And I logout of SOCS application
	And I close the browser successfully
	##03_CaseLeader-SSS_Team_Member_Assigning_a_CaseWorker-SSS_Team_Member_to_a_Service
	Given I navigate to SOCS application homepage
	When I login using SSSStaffMember username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	And I select Service for Program Request
	| Service type    |
	| Program Details |  
	And I assign a case worker
	| FirstName           | LastName | UserRole         | Specialty    |
	| SOCS SSSStaffMember | Test     | SSS Staff Member | Psychologist |
	And I logout of SOCS application
	And I close the browser successfully
	##04_CaseLeader-SSS_Team_Member_UpdatePriority
	Given I navigate to SOCS application homepage
	When I login using SSSStaffMember username and password
	And I press login button
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	And I select Service for Program Request
	| Service type    |
	| Program Details |  
	And I close the service
	| InterventionOutcomes | InterventionAims         |
	| Issue is resolved    | Been completely achieved |	
	And I close the case for program request
	And I logout of SOCS application
	And I close the browser successfully
	#############END OF SCRIPT########################################