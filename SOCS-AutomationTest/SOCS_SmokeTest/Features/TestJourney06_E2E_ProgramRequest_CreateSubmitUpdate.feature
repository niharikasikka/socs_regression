﻿Feature: TestJourney06_E2E_ProgramRequest_CreateSubmitUpdate_Curriculum
##############################################################################################################
##
## This feature file covers the following:
## 01_SchoolPrincipal_Create_and_Submit_a_ProgramRequest
## 02_AuthorisedSchoolOfficer_AddCaseNote
## 03_SchoolPrincipal_RequestNewSpecialist
##
## Things to do:
## Modify the Student First Name - ensure that no existing submitted/active case for this student
## Update the FileUpload_ConsentForm in SOCS_SmokeTest>Data>Resources.resx
## Update the FileUpload_Document in SOCS_SmokeTest>Data>Resources.resx
## Build/Rebuild
##
## NOTE(s):
## below scenarios are created for continuity/dependency of each other
## integrated jobs may affect SOCS performance hence may affect automation script as well
##
##############################################################################################################

@SmokeTest
Scenario: Create>Submit>AddCaseNote>RequestNewSpecialist
	##01_SchoolPrincipal_Create_and_Submit_a_ProgramRequest
	Given I navigate to SOCS application homepage
	When I login using SchoolPrincipal username and password
	And I press login button
	Then I click Create Program Request submenu	
	And I fill in program request details - Curriculum/learning as Primary Presenting Issue
	| Primary Presenting Issue |
	| Curriculum/learning      |
	And I select students for the program request
	| Student First Name |
	| FSN010050159       |
	And I will get the program case number
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	Then I upload supporting documents for Program Request
	And I save the changes in Program Request
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	And I add/save case note for Program Request 
	| DurationHours | DurationMinutes | School Action                      |
	| 1             | 00              | Complete a school based assessment |
	And I save the changes in Program Request
	And I attach consent form for Program Request 
	And I save and submit existing Program Request
	And I logout of SOCS application
	And I close the browser successfully
	##02_AuthorisedSchoolOfficer_AddCaseNote
	Given I navigate to SOCS application homepage
	When I login using AuthorisedSchoolOfficer username and password
	And I press login button
	Then I click Cases menu
	And I filter and select by newly created Program Request case number	
	And I add/save case note and save school case
	| DurationHours | DurationMinutes | School Action              |
	| 1             | 00              | Obtain external assessment |
	And I save the changes in Program Request
	And I logout of SOCS application
	And I close the browser successfully
	##03_SchoolPrincipal_RequestNewSpecialist
	Given I navigate to SOCS application homepage
	When I login using SchoolPrincipal username and password
	And I press login button
	Then I click Cases menu
	And I filter and select by newly created Program Request case number	
	And I request new specialist
	And I save the changes in Program Request
	And I logout of SOCS application
	And I close the browser successfully