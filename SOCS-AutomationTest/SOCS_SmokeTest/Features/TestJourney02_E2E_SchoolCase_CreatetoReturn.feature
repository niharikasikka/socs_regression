﻿Feature: TestJourney02_E2E_SchoolCase_CreatetoReturn
##############################################################################################################
##
## This feature file covers the following:
## 01_AuthorisedSchoolOfficer_Create_and_Submit_a_School_Case_Hearing+Communication_or_Speech
## 02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
## 03_CaseLeader-SSS_Team_Member_Assigning_a_CaseWorker-SSS_Team_Member_to_a_Service
## 04_SSSTeamLeader_Return_a_Case
##
## Things to do:
## Modify the Student First Name - ensure that no existing submitted/active/returned case for this student
## Update the FileUpload_ConsentForm in SOCS_SmokeTest>Data>Resources.resx
## Build/Rebuild
##
## NOTE(s):
## below scenarios are created for continuity/dependency of each other
## integrated jobs may affect SOCS performance hence may affect automation script as well
##
##############################################################################################################

@SmokeTest
Scenario: Create>Submit>AssignCaseLeader>AssignCaseWorker>Return
	##01_AuthorisedSchoolOfficer_Create_and_Submit_a_School_Case_Hearing+Communication_or_Speech
	Given I navigate to SOCS application homepage
	When I login using AuthorisedSchoolOfficer username and password
	And I press login button
	Then I click School Cases submenu
	And I select student
	| Student First Name |
	| FSN010050154       |
	And I fill in case details	
	| Request Type       | Primary Presenting Issue |
	| Individual student | Hearing                  |
	And I update by selecting Communication or Speech
	##only applicable when Primary Presenting Issue is other than 'Attendance', 'Behaviour', 'Communication or Speech', 'Social/Emotional', 'Curriculum or learning', 'Medical Health or Physical', 'Mental Health', 'Support following critical incident
	And I will get the case number
	Then I click Cases menu
	And I filter and select by newly created case number
	And I attach consent form
	And I submit existing student case
	And I logout of SOCS application
	And I close the browser successfully
	##02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created case number
	And I made some action
	| Priority   | Action               | Recommendations                                 |
	| Priority 1 | Assign a Case Leader | The school agrees on an internal school program |
	And I assign a case leader
	| FirstName           | LastName | UserRole         | Specialty    |
	| SOCS SSSStaffMember | Test     | SSS Staff Member | Psychologist |
	And I logout of SOCS application
	And I close the browser successfully
	##03_CaseLeader-SSS_Team_Member_Assigning_a_CaseWorker-SSS_Team_Member_to_a_Service
	Given I navigate to SOCS application homepage
	When I login using SSSStaffMember username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created case number
	And I select Service
	| Service type   |
	| Hearing/Vision |
	And I assign a case worker
	| FirstName           | LastName | UserRole         | Specialty    |
	| SOCS SSSStaffMember | Test     | SSS Staff Member | Psychologist |
	And I logout of SOCS application
	And I close the browser successfully
	##04_SSSTeamLeader_Return_a_Case
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button
	Then I click Cases menu
	And I filter and select by newly created case number	
	And I return the case
	And I logout of SOCS application
	And I close the browser successfully