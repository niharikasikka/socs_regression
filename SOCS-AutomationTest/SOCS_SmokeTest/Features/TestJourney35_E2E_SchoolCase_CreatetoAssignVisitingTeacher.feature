﻿Feature: TestJourney35_E2E_SchoolCase_CreatetoAssignVisitingTeacher
##############################################################################################################
##
## This feature file covers the following:
## 01_AuthorisedSchoolOfficer_Create_and_Submit_a_School_Case_WholeSchoolIssues+Communication_or_Speech
## 02_AuthorisedSchoolOfficer_UpdateAddCaseNote
## 03_SchoolPrincipal_UpdateRequestNewSpecialist
## 04_SSSTeamLeader_Assigning_a_CaseLeader_to_VisitingTeacherTeamLeader
## 05_CaseLeader-VisitingTeacherTeamLeader_Assigning_a_CaseWorker-VisitingTeacher_MULTIPLEService
##
## Things to do:
## Modify the Student First Name - ensure that no existing submitted/active case for this student
## Update the FileUpload_ConsentForm in SOCS_SmokeTest>Data>Resources.resx
## Build/Rebuild
##
## NOTE(s):
## below scenarios are created for continuity/dependency of each other
## integrated jobs may affect SOCS performance hence may affect automation script as well
##
##############################################################################################################


@SmokeTest
Scenario: Create>Submit>Update:AddCaseNote>Update:RequestNewSpecialist>AssignCaseLeader>AssignCaseWorker
	##01_AuthorisedSchoolOfficer_Create_and_Submit_a_School_Case_WholeSchoolIssues+Communication_or_Speech
	Given I navigate to SOCS application homepage
	When I login using AuthorisedSchoolOfficer username and password
	And I press login button
	Then I click School Cases submenu
	And I select student
	| Student First Name |
	| FSN010050202       |
	And I fill in case details
	| Request Type       | Primary Presenting Issue |
	| Individual student | Whole school issues      |
	And I update by selecting Communication or Speech
	##only applicable when Primary Presenting Issue is other than 'Attendance', 'Behaviour', 'Communication or Speech', 'Social/Emotional', 'Curriculum or learning', 'Medical Health or Physical', 'Mental Health', 'Support following critical incident
	And I will get the case number
	Then I click Cases menu
	And I filter and select by newly created case number
	And I attach consent form
	And I submit existing student case
	And I logout of SOCS application
	And I close the browser successfully
	##02_AuthorisedSchoolOfficer_AddCaseNote
	Given I navigate to SOCS application homepage
	When I login using AuthorisedSchoolOfficer username and password
	And I press login button
	Then I click Cases menu
	##And I filter and select by case number
	##| Case number |
	##| 00135171    |
	And I filter and select by newly created case number	
	And I add/save case note and save school case
	| DurationHours | DurationMinutes | School Action              |
	| 1             | 00              | Obtain external assessment |
	And I save changes in School Case
	And I logout of SOCS application
	And I close the browser successfully
	##03_SchoolPrincipal_UpdateRequestNewSpecialist
	Given I navigate to SOCS application homepage
	When I login using SchoolPrincipal username and password
	And I press login button
	Then I click Cases menu
	##And I filter and select by case number
	##| Case number |
	##| 00135171    |
	And I filter and select by newly created case number	
	And I request amend specialist for School Case
	| Psychologist | Speech Pathologist | Social Worker | Visiting Teacher | Other | SSSO with attendance or behaviour expertise |
	| checked      | checked            |               | checked          |       |                                             |
	And I save changes in School Case
	And I logout of SOCS application
	And I close the browser successfully
	##04_SSSTeamLeader_Assigning_a_CaseLeader_to_VisitingTeacherTeamLeader
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created case number
	And I made some action
	| Priority   | Action               | Recommendations                                 |
	| Priority 1 | Assign a Case Leader | The school agrees on an internal school program |
	And I assign a case leader
	| FirstName                      | LastName | UserRole                     | Specialty |
	| SOCS VisitingTeacherTeamLeader | Test     | Visiting Teacher Team Leader |           |
	And I logout of SOCS application
	And I close the browser successfully
	##05_CaseLeader-VisitingTeacherTeamLeader_Assigning_a_CaseWorker-VisitingTeacher_to_MULTIPLEService
	Given I navigate to SOCS application homepage
	When I login using VisitingTeacherTeamLeader username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created case number
	##And I select Service
	##| Service type    |
	##| Speech/Language |
	##And I assign a case worker
	##| FirstName            | LastName | UserRole         | Specialty |
	##| SOCS VisitingTeacher | Test     | Visiting Teacher |           |
	And I select Services and assign Case Worker
	| Service type     | FirstName            | LastName | UserRole         | Specialty |
	| Social/Emotional | SOCS VisitingTeacher | Test     | Visiting Teacher |           |
	| Speech/Language  | SOCS VisitingTeacher | Test     | Visiting Teacher |           |
	And I logout of SOCS application
	And I close the browser successfully
	##06_Cleanup_CloseCase_to_make_same_student_reusable
	Given I navigate to SOCS application homepage
	When I login using VisitingTeacherTeamLeader username and password
	And I press login button
	Then I click Cases menu
	And I filter and select by newly created case number
	##And I select Service
	##| Service type         |
	##| Learning/Development |
	##And I close the service
	##| InterventionOutcomes | InterventionAims         |
	##| Issue is resolved    | Been completely achieved |
	And I select and close services
	| Service type     | InterventionOutcomes | InterventionAims         |
	| Social/Emotional | Issue is resolved    | Been completely achieved |
	| Speech/Language  | Issue is resolved    | Been completely achieved |
	And I close the case
	And I logout of SOCS application
	And I close the browser successfully