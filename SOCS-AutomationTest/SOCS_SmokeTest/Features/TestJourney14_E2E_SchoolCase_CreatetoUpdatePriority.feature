﻿Feature: TestJourney14_E2E_SchoolCase_CreatetoUpdatePriority
##############################################################################################################
##
## This feature file covers the following:
## 01_SchoolPrincipal_Create_and_Submit_a_School_Case_Supportfollowingcriticalincident
## 02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
## 03_CaseLeader-SSS_Team_Member_Assigning_a_CaseWorker-SSS_Team_Member_to_a_Service
## 04_SSSTeamLeader_UpdatePriority
##
## Things to do:
## Modify the Student First Name - ensure that no existing submitted/active case for this student
## Update the FileUpload_ConsentForm in SOCS_SmokeTest>Data>Resources.resx
## Build/Rebuild
##
## NOTE(s):
## below scenarios are created for continuity/dependency of each other
## integrated jobs may affect SOCS performance hence may affect automation script as well
##
##############################################################################################################

@SmokeTest
Scenario: Create>Submit>AssignCaseLeader>AssignCaseWorker>UpdatePriority
	##01_SchoolPrincipal_Create_and_Submit_a_School_Case_Supportfollowingcriticalincident
	Given I navigate to SOCS application homepage
	When I login using SchoolPrincipal username and password
	And I press login button
	Then I click School Cases submenu
	And I select student
	| Student First Name |
	| FSN010050167       |
	And I fill in case details
	| Request Type       | Primary Presenting Issue            |
	| Individual student | Support following critical incident |
	And I will get the case number
	Then I click Cases menu
	And I filter and select by newly created case number
	And I attach consent form
	And I submit existing student case
	And I logout of SOCS application
	And I close the browser successfully
	##02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created case number
	And I made some action
	| Priority   | Action               | Recommendations                                 |
	| Priority 1 | Assign a Case Leader | The school agrees on an internal school program |
	And I assign a case leader
	| FirstName           | LastName | UserRole         | Specialty    |
	| SOCS SSSStaffMember | Test     | SSS Staff Member | Psychologist |
	And I logout of SOCS application
	And I close the browser successfully
	##03_CaseLeader-SSS_Team_Member_Assigning_a_CaseWorker-SSS_Team_Member_to_a_Service
	Given I navigate to SOCS application homepage
	When I login using SSSStaffMember username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created case number
	And I select Service
	| Service type         |
	| Learning/Development |
	And I assign a case worker
	| FirstName           | LastName | UserRole         | Specialty    |
	| SOCS SSSStaffMember | Test     | SSS Staff Member | Psychologist |
	And I logout of SOCS application
	And I close the browser successfully
	##04_SSSTeamLeader_UpdatePriority
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button
	Then I click Cases menu
	##And I filter and select by case number
	##| Case number |
	##| 00135088    |
	And I filter and select by newly created case number
	And I update Priority
	| Priority   |
	| Priority 2 |
	And I save changes in School Case
	And I logout of SOCS application
	And I close the browser successfully
	##05_Cleanup_CloseCase_to_make_same_student_reusable
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button
	Then I click Cases menu
	And I filter and select by newly created case number
	And I select Service
	| Service type         |
	| Learning/Development |
	And I close the service
	| InterventionOutcomes | InterventionAims         |
	| Issue is resolved    | Been completely achieved |
	And I close the case
	And I logout of SOCS application
	And I close the browser successfully