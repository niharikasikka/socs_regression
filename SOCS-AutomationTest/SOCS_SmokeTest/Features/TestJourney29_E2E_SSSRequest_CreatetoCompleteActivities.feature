﻿Feature: TestJourney29_E2E_SSSRequest_CreatetoCompleteActivities
##############################################################################################################
##
## This feature file covers the following:
## 01_SchoolPrincipal_Create_and_Submit_a_SSSRequest_MentalHealth
## 02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
## 03_CaseLeader-SSS_Team_Member_Assigning_a_CaseWorker-SSS_Team_Member_to_a_Service
## 04_CaseLeader-SSS_Team_Member_CompleteActivities
##
## Things to do:
## Modify the Student First Name - ensure that no existing submitted/active case for this student
## Update the FileUpload_ConsentForm in SOCS_SmokeTest>Data>Resources.resx
## Build/Rebuild
##
## NOTE(s):
## below scenarios are created for continuity/dependency of each other
## integrated jobs may affect SOCS performance hence may affect automation script as well
##
##############################################################################################################

@SmokeTest
Scenario: Create>Submit>AssignCaseLeader>AssignCaseWorker>CompleteActivities
	##01_SchoolPrincipal_Create_and_Submit_a_SSSRequest_MentalHealth
	Given I navigate to SOCS application homepage
	When I login using SchoolPrincipal username and password
	And I press login button
	Then I select Create Student Support Services Request from the Case menu
	And I select student for SSS Request
	| Student First Name |
	| FSN010050196       |
	And I fill in Student Support Services request details
	| Request Type       | Primary Presenting Issue |
	| Individual student | Mental Health            |
	And I will get the SSS Request case number
	Then I click Cases menu
	And I filter and select by newly created SSS Request case number
	And I attach Consent Form for SSS Request
	And I save and submit existing SSS Request
	And I logout of SOCS application
	And I close the browser successfully
	##02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created SSS Request case number
	And I made some action for SSS Request
	| Priority   | Action               | Recommendations                                 |
	| Priority 1 | Assign a Case Leader | The school agrees on an internal school program |
	And I assign a case leader for SSS Request
	| FirstName           | LastName | UserRole         | Specialty    |
	| SOCS SSSStaffMember | Test     | SSS Staff Member | Psychologist |
	And I logout of SOCS application
	And I close the browser successfully
	##03_CaseLeader-SSS_Team_Member_Assigning_a_CaseWorker-SSS_Team_Member_to_a_Service
	Given I navigate to SOCS application homepage
	When I login using SSSStaffMember username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created SSS Request case number
	And I select Service for SSS Request
	| Service type      |
	| Disability/Health |
	And I assign a case worker for SSS Request
	| FirstName           | LastName | UserRole         | Specialty    |
	| SOCS SSSStaffMember | Test     | SSS Staff Member | Psychologist |
	And I logout of SOCS application
	And I close the browser successfully
	##04_CaseLeader-SSS_Team_Member_CompleteActivities
	Given I navigate to SOCS application homepage
	When I login using SSSStaffMember username and password
	And I press login button
	Then I click Cases menu
	And I filter and select by newly created SSS Request case number
	And I indicate that case activities have been completed for SSS Request
	And I select Service for SSS Request
	| Service type      |
	| Disability/Health |
	And I close the service for SSS Request
	| InterventionOutcomes | InterventionAims         |
	| Issue is resolved    | Been completely achieved |	
	And I save the changes in SSS Request
	And I logout of SOCS application
	And I close the browser successfully
	##05_Cleanup_CloseCase_to_make_same_student_reusable
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button
	Then I click Cases menu
	And I filter and select by newly created SSS Request case number	
	And I close the SSS request case
	And I logout of SOCS application
	And I close the browser successfully