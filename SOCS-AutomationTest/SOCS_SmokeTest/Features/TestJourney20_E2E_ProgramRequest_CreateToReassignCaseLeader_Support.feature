﻿Feature: TestJourney20_E2E_ProgramRequest_CreateToReassignCaseLeader_Support
##############################################################################################################
##
## This feature file covers the following:
## 01_SchoolPrincipal_Create_and_Submit_a_School_Case_Support following critical incident
## 02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
## 03_SSSTeamLeader_ReAssigning_a_CaseLeader_to_SOCS VisitingTeacher
##
## Things to do:
## Modify the Student First Name - ensure that no existing submitted/active case for this student
## Update the FileUpload_ConsentForm in SOCS_SmokeTest>Data>Resources.resx
## Build/Rebuild
##
## NOTE(s):
## below scenarios are created for continuity/dependency of each other
## integrated jobs may affect SOCS performance hence may affect automation script as well
##
##############################################################################################################

@SmokeTest
Scenario: Create>Submit>Assign_Case_Leader>Reassign_Case_Leader>SupportFollowingCriticalIncident
	##01_SchoolPrincipal_Create_and_Submit_a_Program_Request_SupportFollowingCriticalIncident
	Given I navigate to SOCS application homepage
	When I login using SchoolPrincipal username and password
	And I press login button
	Then I click Create Program Request submenu
	And I fill in program request details - Support following critical incident
	| Primary Presenting Issue            |
	| Support following critical incident |  
	And I select students for the program request
	| Student First Name |
	| FSN010050207       |
	And I will get the program case number
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	And I add/save case note for Program Request 
	| DurationHours | DurationMinutes | School Action                      |
	| 1             | 00              | Complete a school based assessment |
	And I save the changes in Program Request
	And I attach consent form for Program Request 
	And I save and submit existing Program Request
	And I logout of SOCS application
	And I close the browser successfully
	##02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	And I made some action for program request
	| Priority   | Action               | Recommendations                                 |
	| Priority 1 | Assign a Case Leader | The school agrees on an internal school program |
	And I assign a case leader
	| FirstName           | LastName | UserRole         | Specialty    |
	| SOCS SSSStaffMember | Test     | SSS Staff Member | Psychologist |
	And I logout of SOCS application
	And I close the browser successfully
	##03_SSSTeamLeader_ReAssigning_a_CaseLeader_to_SOCS VisitingTeacher
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	And I re-assign a case leader
	| FirstName            | LastName | UserRole         | Specialty |
	| SOCS VisitingTeacher | Test     | Visiting Teacher |           |
	And I logout of SOCS application
	And I close the browser successfully
	##04_Cleanup_CloseCase_to_make_same_student_reusable
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	And I close the case for program request
	And I logout of SOCS application
	And I close the browser successfully
######################END OF SCRIPT################################################