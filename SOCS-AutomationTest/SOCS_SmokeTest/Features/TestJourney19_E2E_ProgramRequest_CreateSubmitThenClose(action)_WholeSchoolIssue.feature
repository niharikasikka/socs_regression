﻿Feature: TestJourney19_E2E_ProgramRequest_CreateSubmitThenClose[action]_WholeSchoolIssue
##############################################################################################################
##
## This feature file covers the following:
## 01_SchoolPrincipal_Create_and_Submit_a_ProgramRequest Whole School Issue
## 02_AuthorisedSchoolOfficer_AddCaseNote
##
## Things to do:
##
## Update the FileUpload_ConsentForm in SOCS_SmokeTest>Data>Resources.resx
## Update the FileUpload_Document in SOCS_SmokeTest>Data>Resources.resx
## Build/Rebuild
##
## NOTE(s):
## below scenarios are created for continuity/dependency of each other
## integrated jobs may affect SOCS performance hence may affect automation script as well
##
##############################################################################################################

@SmokeTest
Scenario: Create>Submit>AddCaseNote>Close_Action>NoStudent>WholeSchoolIssue
	##01_SchoolPrincipal_Create_and_Submit_a_ProgramRequest
	Given I navigate to SOCS application homepage
	When I login using SchoolPrincipal username and password
	And I press login button
	Then I click Create Program Request submenu	
	And I fill in program request details - Whole School Issue
	| Primary Presenting Issue |
	| Whole school issues      | 
	And I select no student for the program request
	And I will get the program case number
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	Then I upload supporting documents for Program Request
	And I save the changes in Program Request
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	And I add/save case note for Program Request 
	| DurationHours | DurationMinutes | School Action                      |
	| 1             | 00              | Complete a school based assessment |
	And I save the changes in Program Request
	And I attach consent form for Program Request 
	And I save and submit existing Program Request
	And I logout of SOCS application
	And I close the browser successfully
	##02_SSSTeamLeader_ACTION_Closing_a_Case
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created Program Request case number
	And I close the case for program request by action
	| Priority   | Action     |
	| Priority 1 | Close case |
	And I logout of SOCS application
	And I close the browser successfully
	###########END OF SCRIPT##############################################	