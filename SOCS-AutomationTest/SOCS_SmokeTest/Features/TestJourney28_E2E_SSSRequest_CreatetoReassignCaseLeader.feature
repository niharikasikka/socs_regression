﻿Feature: TestJourney28_E2E_SSSRequest_CreatetoReassignCaseLeader
##############################################################################################################
##
## This feature file covers the following:
## 01_SchoolPrincipal_Create_and_Submit_a_SSSRequest_Curriculum/learning
## 02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
## 03_SSSTeamLeader_ReAssigning_a_CaseLeader_to_SOCS VisitingTeacher
##
## Things to do:
## Modify the Student First Name - ensure that no existing submitted/active case for this student
## Update the FileUpload_ConsentForm in SOCS_SmokeTest>Data>Resources.resx
## Build/Rebuild
##
## NOTE(s):
## below scenarios are created for continuity/dependency of each other
## integrated jobs may affect SOCS performance hence may affect automation script as well
##
##############################################################################################################

@SmokeTest
Scenario: Create>Submit>Assign_Case_Leader>Reassign_Case_Leader
	##01_SchoolPrincipal_Create_and_Submit_a_SSSRequest_Curriculum/learning
	Given I navigate to SOCS application homepage
	When I login using SchoolPrincipal username and password
	And I press login button
	Then I select Create Student Support Services Request from the Case menu
	And I select student for SSS Request
	| Student First Name |
	| FSN010050194       |
	And I fill in Student Support Services request details
	| Request Type       | Primary Presenting Issue |
	| Individual student | Curriculum/learning      |
	And I will get the SSS Request case number
	Then I click Cases menu
	And I filter and select by newly created SSS Request case number
	And I attach Consent Form for SSS Request
	And I save and submit existing SSS Request
	And I logout of SOCS application
	And I close the browser successfully
	##02_SSSTeamLeader_Assigning_a_CaseLeader_to_SSS Team Member
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created SSS Request case number
	And I made some action for SSS Request
	| Priority   | Action               | Recommendations                                 |
	| Priority 1 | Assign a Case Leader | The school agrees on an internal school program |
	And I assign a case leader for SSS Request
	| FirstName           | LastName | UserRole         | Specialty    |
	| SOCS SSSStaffMember | Test     | SSS Staff Member | Psychologist |
	And I logout of SOCS application
	And I close the browser successfully
	##03_SSSTeamLeader_ReAssigning_a_CaseLeader_to_SOCS VisitingTeacher
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button	
	Then I click Cases menu
	And I filter and select by newly created SSS Request case number	
	And I re-assign a case leader for SSS Request
	| FirstName            | LastName | UserRole         | Specialty |
	| SOCS VisitingTeacher | Test     | Visiting Teacher |           |
	And I logout of SOCS application
	And I close the browser successfully
	##04_Cleanup_CloseCase_to_make_same_student_reusable
	Given I navigate to SOCS application homepage
	When I login using SSSTeamLeader username and password
	And I press login button
	Then I click Cases menu
	And I filter and select by newly created SSS Request case number
	And I close the SSS request case
	And I logout of SOCS application
	And I close the browser successfully